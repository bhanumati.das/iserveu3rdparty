package com.iserveu.service;

import com.iserveu.request.pojo.TxnStatusWrapper;
import com.iserveu.response.pojo.TxnStatusResponse;

/**
 * Service interface for Fino API layer.
 * 
 * @author Bhanumati
 *
 */
public interface PayeeVerificationFinoApiService {

	TxnStatusResponse getTransactionStatus(String clientUniqueID);

	TxnStatusWrapper consumeTxnStatus(String clientUniqueID);

}
