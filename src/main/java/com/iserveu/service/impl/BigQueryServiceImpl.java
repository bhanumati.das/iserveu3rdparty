package com.iserveu.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.threeten.bp.Duration;

import com.google.api.gax.retrying.RetrySettings;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryError;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValue;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableResult;
import com.iserveu.service.BigQueryService;


/**
 * 
 * @author Raj
 *
 */
@Service
public class BigQueryServiceImpl implements BigQueryService {

   final String BQDataset = "production";
	//final String BQDataset = "matm_dataset";
	//final String MATMTable = "matm_transaction_params";
	final String MATMTable = "transaction_params";
	final String DBAndTable = BQDataset + "." + MATMTable;
	final String credentialPath = "/home/iserveutech/iServeUProd-1f8372f23af4.json"; // LIVE
	//final String credentialPath= "/home/iserveutech/iserveustaging-aa3ce3e40a0a.json";
	private static final Logger logger = LogManager.getLogger(BigQueryServiceImpl.class);
	
	@Value("${BQ_BASE_URL}")
	private String BQ_BASE_URL; 

	private BigQuery bigQuery = null;

	public BigQueryServiceImpl() {
		// Read the credentials
		GoogleCredentials credentials = null;
		File credentialsPath = new File(credentialPath);
		try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
			credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Connect to BigQuery dataset
		this.setBigQuery(BigQueryOptions.newBuilder().setCredentials(credentials)
				.setRetrySettings(RetrySettings.newBuilder()
						// .setMaxAttempts(10)
						.setRetryDelayMultiplier(1.2).setTotalTimeout(Duration.ofMinutes(10)).build())
				.build().getService());
	}

	
	@Override
	public String saveMatmTransactionParamsToBq(Map<String, Object> row) {
		try {
		TableId tableId = TableId.of(BQDataset, MATMTable);
		//Setting the insertion time in BigQuery
		row.put("created_date", ISTToUTC(new Date()));
		InsertAllResponse response = this.bigQuery
				.insertAll(InsertAllRequest.newBuilder(tableId).addRow(row).build());
		if (response.hasErrors()) {
			logger.info("\terror in insertion!!");
			// If any of the insertions failed, this lets you inspect the errors
			for (Map.Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
				// inspect row error
				entry.toString();
				logger.info(entry.toString());
			}
			return null;
		}else {
			logger.info("\tdata inserted successfully!!!");
			return "transaction data saved successfully";
		}
		}catch(Exception e) {
			logger.info("\terror in insertion!!");
			logger.error("\t"+ExceptionUtils.getStackTrace(e));
			return null;
		}
	}
	
	@Override
	public Integer findExistingTransactionInMATM(String txnId) {
			logger.info("\t----------------finding existing transaction for txn id ----------------------");
			logger.info("\t\ttransactionId="+txnId);
			    String query = "select count(id) as count from `"+DBAndTable+"` where id ="+txnId;
			    QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();
			    Integer count=1;
			    // Print the results.
			    try {
					for (FieldValueList row : bigQuery.query(queryConfig).iterateAll()){
						FieldValue value=row.get(0);
						count=Integer.parseInt(value.getValue().toString());
						logger.info("\t\tgot response of existing transaction search");
						logger.info("\t------------------------------------------------------------------------------");
						return count;
					}
					logger.info("\t\tdidn't hit bq due to some reason");
					logger.info("\t------------------------------------------------------------------------------");
					return null;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.info("\t\t got exception while hitting big query");
					logger.error("\t\t"+ExceptionUtils.getStackTrace(e));
					logger.info("\t------------------------------------------------------------------------------");
					return null;
				}
	}
	
	@Async("bigQueryThreadPoolTaskExecutor")
	public void restCallsaveToBigQuery(Map<String, Object> transactionMap) {

	}

	public BigQuery getBigQuery() {
		return bigQuery;
	}

	public void setBigQuery(BigQuery bigQuery) {
		this.bigQuery = bigQuery;
	}

	/*
	 * QueryJobConfiguration to process the query and responseName to format the
	 * returned json
	 */
	private String queryConfigToJsonString(QueryJobConfiguration queryConfig, String responseName) {

		 //System.out.println(queryConfig.toString());

		TableResult result = null;
		try {
			long start = System.currentTimeMillis();
			// Excecute the query
			result = bigQuery.query(queryConfig);
			System.out.println("The query takes " + (System.currentTimeMillis() - start) + "ms");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(result.getTotalRows() + " rows");

		String finalResponse = "";
		try {
			long start = System.currentTimeMillis();
			// Prepare the result in the Json format "{},{},{}, ... {}"
			Iterable<FieldValueList> iterable = result.iterateAll();
			Stream<FieldValueList> streamIterable = StreamSupport.stream(iterable.spliterator(), true);
			finalResponse = streamIterable.map(value -> value.get(0).getStringValue()).collect(Collectors.joining(","));
			System.out.println(
					"Parallel processing with collectors takes " + (System.currentTimeMillis() - start) + "ms");
		} catch (Exception e) {
			// The query returns 0 row
		}

		finalResponse = "{\"" + responseName + "\": [" + finalResponse + "]}";

		return finalResponse;
		/*
		 * Gson gson = new Gson(); java.lang.reflect.Type type = new
		 * TypeToken<Map<String, Object>>(){}.getType(); Map<String, Object> myMap =
		 * gson.fromJson(resultString, type); return myMap;
		 */
	}

	/*
	 * Convert IST date to UTC
	 */
	private String ISTToUTC(Date date) {
		// need to import the corresponding libraries from java.time
		java.time.LocalDateTime dbTime = date.toInstant().atZone(java.time.ZoneId.of("Asia/Kolkata")).toLocalDateTime();
		dbTime = dbTime.minusHours(5).minusMinutes(30);

		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter
				.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSS");

		return dbTime.format(formatter);
	}

	/*
	 * Main reporting method Used to fetch UWT of different type
	 */
	
	@Override
    @Async("bigQueryThreadPoolTaskExecutor")
    public void restCallsavetocashoutdb(Map<String, Object> walletcashout) {
        try {

 

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

 

            HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(walletcashout, headers);

 

            final RestTemplate restTemplate = new RestTemplate();
            
            String api = BQ_BASE_URL + "/saveWalletCashoutToBQ";

 

            ResponseEntity<String> response = restTemplate.postForEntity(api, requestEntity, String.class);
        } catch (Exception e) {
            logger.info("Could not insert in BigQuery transaction  " + e.getMessage());
            e.printStackTrace();
        }

    }


}
