package com.iserveu.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.entity.FeatureEntity;
import com.iserveu.entity.ServiceProviderDetailsEntity;
import com.iserveu.entity.UserApiMappingEntity;
import com.iserveu.entity.UserFeatureEntity;
import com.iserveu.entity.WalletCashoutTransaction;
import com.iserveu.rbl.query.model.request.Get_Single_Payment_Status_Corp_Req;
import com.iserveu.rbl.query.model.request.RBLQueryRequestPojo;
import com.iserveu.rbl.query.model.response.RBLQueryResponsePojo;
import com.iserveu.repository.FeatureRepository;
import com.iserveu.repository.ServiceProviderJpaRepository;
import com.iserveu.repository.UserApiMappingRepository;
import com.iserveu.repository.UserFeatureRepository;
import com.iserveu.repository.WalletCashoutEntiyRepository;
import com.iserveu.request.pojo.Body;
import com.iserveu.request.pojo.Header;
import com.iserveu.request.pojo.RBLCommitTransactionRequestPojo;
import com.iserveu.request.pojo.RBLIMPSRequestPojo;
import com.iserveu.request.pojo.Signature;
import com.iserveu.request.pojo.Single_Payment_Corp_Req;
import com.iserveu.request.pojo.WalletCashoutrequest;
import com.iserveu.request.pojo.WalletCashoutrequestRefresh;
import com.iserveu.request.pojo.WalletOpreationRequest;
import com.iserveu.response.pojo.RBLIMPSResponsePojo;
import com.iserveu.response.pojo.UserBank;
import com.iserveu.service.CoreService;
import com.iserveu.service.RBLIMPSService;
import com.iserveu.service.WalletCashoutService;
import com.iserveu.service.WalletService;
import com.iserveu.util.Utility;

@Service
public class WalletCashoutServiceImpl implements WalletCashoutService {
	
	
	private static final Logger LOGGER = LogManager.getLogger(WalletCashoutServiceImpl.class);
	
	
	
	@Autowired
	private Environment env;
	

	@Autowired
	CoreService coreservice;
	
	
	@Autowired
	FeatureRepository featureRepository;
	
	
	@Autowired
	UserFeatureRepository userFeatureRepository;
	
	private String getDebitAccName() {
        return env.getProperty("RBLDebitAcctName");
    }

 

    private String getDebitAccNo() {
        return env.getProperty("RBLDebitAcctNo");
    }

 

    private String getDebitIfsc() {
        return env.getProperty("RBLDebitIFSC");
    }

 

    private String getDebitMobNo() {
        return env.getProperty("RBLDebitMobile");
    }
	 
	 
	 private String getCorpID() {
	        return env.getProperty("RBLCorpID");
	    }

	
	
	@Value("${core_app_url}")
	private String coreAppUrl;
	
	@Value("${walletApiurl}")
	private String walletApiurl;

	@Autowired
	RBLIMPSService rblimpsservice;
	
	@Autowired
	WalletService walletservice;
	

	@Autowired
	private UserApiMappingRepository userApiRepo;
	
	@Autowired
	WalletCashoutEntiyRepository walletjparepository;
	
	
	
	@Autowired
	ServiceProviderJpaRepository serviceProviderRepo;
	
	@Override
	public Map<String, Object> getWalletCashoutservicce(WalletCashoutrequest request,String Authorization) {
		
		Map<String, Object> responsemap = new HashMap<String, Object>();
		
		String userName =  Utility.getUserName(Authorization);
		
		LOGGER.info("Wallet 2 cashout requested user :"+userName);
		
		
		if(userName==null) {
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "User not available");
			return responsemap;
		}
		
		Optional<FeatureEntity> featuredata = featureRepository.findById(50);
		
		if(featuredata.isPresent()) {
			
			FeatureEntity feature = featuredata.get();
			
			if(feature==null) {
				responsemap.put("status", -1);
				responsemap.put("statusDesc", "Add feature first");
				return responsemap;
			}
			if(feature.getIsActive().equals(false)) {
				responsemap.put("status", -1);
				responsemap.put("statusDesc", "Service is disabled temporarily,Please do not use until further notification.");
				return responsemap;
			}
			
		}else {
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "Feature not available");
			return responsemap;
			
		}

		

		
		
		
		Optional<UserApiMappingEntity> userdata = this.userApiRepo.findById(userName);
		if(userdata.isPresent()){

			UserApiMappingEntity userApidata  =   userdata.get();
			if(userApidata!=null) {
				
				UserFeatureEntity userfeature = userFeatureRepository.findByUserFeatureKeyUserIdAndUserFeatureKeyFeatureId(userApidata.getUserId(),50);
				
				

				if(userfeature==null) {
				
					responsemap.put("status", -1);
					responsemap.put("statusDesc", "Add feature to this user");
					return responsemap;
				}
				
					if(userfeature.getIsActive().equals(false)) {
						
						responsemap.put("status", -1);
						responsemap.put("statusDesc", "Service is not available to this user");
						return responsemap;
						
					}
				
			}
		}else {
			
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "User not available");
			return responsemap;
			
		}
		
		
		
		
		
		
		if(Double.valueOf(request.getAmount())<100||Double.valueOf(request.getAmount())>100000){
			LOGGER.info("Wallet 2 cashout requested amount :"+Double.valueOf(request.getAmount()));
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "Invalid amount,Amount should be in between 100 to 1 lakhs");
			return responsemap;
		}
		LOGGER.info("Wallet 2 cashout requested user going to bank check :"+userName);
		UserBank userbank = togetUserbankdetails(Authorization);
		if(userbank.getActive().equals(false)) {
			LOGGER.info("if bank is not active for:"+userName+ "with message"+userbank.getMessage());
			responsemap.put("status", -1);
			responsemap.put("statusDesc", userbank.getMessage());
			return responsemap;
		}
		
		if(userbank.getActive().equals(true)) {
			LOGGER.info("Entered in if bank is active:"+userName+ "with message"+userbank.getMessage());
			
			Optional<UserApiMappingEntity> userApiOptional= this.userApiRepo.findById(userName);
			if(userApiOptional.isPresent()){
				
				LOGGER.info("If user is available"+userName);
				
				UserApiMappingEntity userApidata  =   userApiOptional.get();
				
				Double amount = Double.valueOf(request.getAmount());
				
				if(userApidata.getApiWallet2Id()!=null&&userApidata.getWallet2Id()!=null) {
					
					ServiceProviderDetailsEntity providerRetailer=this.serviceProviderRepo.findByAmountRange(amount,"MOVE_TO_BANK");
					
					if(providerRetailer==null) {
						responsemap.put("status", -1);
						responsemap.put("statusDesc", "Service Provideer not mapped");
						return responsemap;
					}
					
					WalletOpreationRequest walletrequest = new WalletOpreationRequest();
					walletrequest.setAmount(amount);
					walletrequest.setApiServiceProviderId(providerRetailer.getId());
					walletrequest.setServiceProviderId(providerRetailer.getId());
					walletrequest.setApiuserid(userApidata.getApiUserId());
					walletrequest.setUserid(userApidata.getUserId());
					walletrequest.setApiwalletid(userApidata.getApiWallet2Id());
					walletrequest.setWalletid(userApidata.getWallet2Id());
					walletrequest.setAccountnumber(userbank.getAccountNumber());
					walletrequest.setType("WALLET2CASHOUT");
					
					
					
					
					Map<String,Object> transactionresponse =  walletservice.initiatedtransaction(walletrequest);
					System.out.println(transactionresponse);
					ResponseEntity<RBLIMPSResponsePojo> apiresponse = null;
					if(transactionresponse==null) {
						responsemap.put("status", -1);
						responsemap.put("statusDesc", "Exception in Initiated transaction");
						return responsemap;
					}
					
					if(!transactionresponse.get("status").equals(0)) {
						
						responsemap.put("status", -1);
						responsemap.put("statusDesc", transactionresponse.get("statusDesc"));
						return responsemap;
					}
					if(transactionresponse.get("status").equals(0)) {
						
						Long id =  (Long) transactionresponse.get("txnId");
						if(id==null) {
							

							responsemap.put("status", -1);
							responsemap.put("statusDesc", "Exception Occured in getting txn id");
						}
						
						// Setting request data to hit RBL imps payment API
						RBLIMPSRequestPojo impsrequest = new RBLIMPSRequestPojo();
						Header header = new Header();
						Body body = new Body();
						Signature signature = new Signature();
						Single_Payment_Corp_Req req = new Single_Payment_Corp_Req();

						header.setApprover_ID("");
						header.setChecker_ID("");
						header.setCorp_ID(getCorpID().trim());
						header.setMaker_ID("");
						header.setTranID(id.toString().trim());
						// Bank name should be max 20 character based on RBL rule.
		                String bankName = userbank.getBankName().length() >= 20 ? userbank.getBankName().substring(0, 19)
		                        : userbank.getBankName();
						body.setAmount(request.getAmount().trim());
						body.setBen_Acct_No(userbank.getAccountNumber().trim());
						body.setBen_BankName(bankName.trim());
						body.setBen_Email("");//requestingUser.getEmail().trim()
						body.setBen_IFSC(userbank.getIfsc().trim());
						body.setBen_Mobile("9090953004");//request.getBeneMobileNo().trim()
						body.setBen_Name("Reeturaj Sahoo");//request.getBeneName().trim()
						body.setDebit_Acct_Name(getDebitAccName().trim());
						body.setDebit_Acct_No(getDebitAccNo().trim());
						body.setDebit_IFSC(getDebitIfsc().trim());
						body.setDebit_Mobile(getDebitMobNo().trim());
						body.setMode_of_Pay("IMPS".trim());
						body.setRemarks("IMPS Request".trim());

						signature.setSignature("Signature");

						req.setHeader(header);
						req.setBody(body);
						req.setSignature(signature);
						impsrequest.setSingle_Payment_Corp_Req(req);

						

						
						apiresponse = rblimpsservice.calltorblimpstransaction(impsrequest);
						
						RBLCommitTransactionRequestPojo commitrequest = new RBLCommitTransactionRequestPojo();

						commitrequest.setPipeNo(id);
						commitrequest.setServiceproviderid(providerRetailer.getId());
						//Check if response from RBL is NULL or not.
						if (apiresponse.getBody() != null) {//BANK response is not NULL
							
							if (apiresponse.getBody().getSingle_Payment_Corp_Resp()!=null && apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()!=null && apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null
									&& apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()
											.equals("91")) {
								
								//Bank response code :91.Its a timeout error at bank,so treated as a pending transaction.
								//LOGGER.info("*******************Timeout error at bank side*********************************");
								
								//printing the bank error message if the bank response code is not a null value
								if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null) {
									LOGGER.info("Bank Response : " + rblImpsResponseCode(
											apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()));
								}
								
								//setting up commit request object to update the transaction based on the bank response
								commitrequest.setStatusCode(1);
								commitrequest.setMessage(Utility.statusPending);
								commitrequest.setTxnID(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getTranID() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
														.getTranID()
												: "");
								commitrequest.setResponseCode(apiresponse.getBody().getSingle_Payment_Corp_Resp()
										.getHeader().getResp_cde() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
														.getResp_cde()
												: "");
								commitrequest.setDisplayMessage("Transaction Pending");

								if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody() != null) {

									commitrequest.setActCode(
											apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRRN() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRRN()
													: "");

									commitrequest.setRequestID(
											apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRefNo() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
															.getRefNo()
													: "");
									commitrequest.setUserTrackId(apiresponse.getBody().getSingle_Payment_Corp_Resp()
											.getBody().getChannelpartnerrefno() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
															.getChannelpartnerrefno()
													: "");
									commitrequest.setAmountRequested(apiresponse.getBody().getSingle_Payment_Corp_Resp()
											.getBody().getAmount() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
															.getAmount()
													: "0.0");
									commitrequest.setBeneName(apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
											.getBenIFSC() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
															.getBenIFSC()
													: "");
									commitrequest.setTransactionDatetime(apiresponse.getBody().getSingle_Payment_Corp_Resp()
											.getBody().getTxn_Time() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
															.getTxn_Time()
													: "");
									commitrequest.setCustomerNumber(apiresponse.getBody().getSingle_Payment_Corp_Resp()
											.getBody().getBen_Acct_No() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
															.getBen_Acct_No()
													: "");
								} else {
									//if in the response Body part is NUll
									commitrequest.setActCode("0");
									commitrequest.setRequestID("");
									commitrequest.setUserTrackId("");
									commitrequest.setAmountRequested(request.getAmount().toString());
									commitrequest.setTransactionDatetime("");
									commitrequest.setCustomerNumber("");
								}

								
								
							}
							else if (apiresponse.getBody().getSingle_Payment_Corp_Resp()!=null && apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()!=null && apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()!=null && apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getStatus()!=null && apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getStatus()
									.equalsIgnoreCase("SUCCESS")
									&& apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null
									&& apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()
											.equals("00")) {
								//if success message from bank
								LOGGER.info("api response success block for wallet cashout : "
										+ apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde());
								
								//printing bank error message if the response code is not null
								if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null) {
									LOGGER.info("Bank Response : " + rblImpsResponseCode(
											apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()));
								}
								
								//setting up commit request object to update the transaction based on the bank response
								commitrequest.setStatusCode(0);
								commitrequest.setActCode(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRRN() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRRN()
												: "");
								
								commitrequest.setMessage(Utility.statusSuccess);
								commitrequest.setTxnID(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getTranID() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getTranID()
												: "");
								commitrequest.setRequestID(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRefNo() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRefNo()
												: "");
								commitrequest.setUserTrackId(apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
										.getChannelpartnerrefno() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
														.getChannelpartnerrefno()
												: "");
								commitrequest.setAmountRequested(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getAmount() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getAmount()
												: "0.0");
								commitrequest.setBeneName(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getBenIFSC() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getBenIFSC()
												: "");
								commitrequest.setResponseCode(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()
												: "");
								commitrequest.setTransactionDatetime(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getTxn_Time() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getTxn_Time()
												: "");
								commitrequest.setCustomerNumber(
										apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getBen_Acct_No() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getBen_Acct_No()
												: "");
								commitrequest.setDisplayMessage("Transaction Successful");

								

							} else {

								try {
		                            //If bank response status is FAILED/FAILURE
									if ((apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getStatus()
											.equalsIgnoreCase("FAILURE"))
											|| (apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getStatus()
													.equalsIgnoreCase("FAILED"))) {

										LOGGER.info("api response failure block for wallet cashout : " + apiresponse.getBody()
												.getSingle_Payment_Corp_Resp().getHeader().getResp_cde());
										LOGGER.info("CASHOUT:enter into Refund block for user" 
												+ ", out TxId: " + id);

										LOGGER.info("Error message from Bank : " + apiresponse.getBody()
												.getSingle_Payment_Corp_Resp().getHeader().getStatus() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getError_Desc()
														: "");
										
										if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null) {
											LOGGER.info("Bank Response : " + rblImpsResponseCode(
													apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()));
										}
										commitrequest.setStatusCode(-1);
										commitrequest.setActCode("0");
										commitrequest.setMessage(Utility.statusRefund);
										commitrequest.setTxnID(apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
												.getTranID() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getTranID()
														: "");
										commitrequest.setRequestID("");
										commitrequest.setUserTrackId("");
										commitrequest.setAmountRequested(amount.toString());
										commitrequest.setResponseCode(apiresponse.getBody().getSingle_Payment_Corp_Resp()
												.getHeader().getResp_cde() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getResp_cde()
														: "");
										commitrequest.setDisplayMessage(apiresponse.getBody().getSingle_Payment_Corp_Resp()
										.getHeader().getStatus() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
														.getError_Desc()
												: "");

										if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody() != null) {

											commitrequest.setUserTrackId(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getChannelpartnerrefno() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getChannelpartnerrefno()
															: "");

										}



									} else {

										//Except SUCCESS/ FAILED /FAILURE /TIMEOUT response, any other response is treated as pending transaction
										LOGGER.info("api response pending block for wallet cashout : " + apiresponse.getBody()
												.getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getResp_cde()
														: "");
										LOGGER.info("Error message from bank : " + apiresponse.getBody()
												.getSingle_Payment_Corp_Resp().getHeader().getStatus() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getError_Desc()
														: "");
										//printing bank response message if the response code is not null
										if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null) {
											LOGGER.info("Bank Response : " + rblImpsResponseCode(
													apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()));
										}
										
										//setting up commit request object to update the transaction based on the bank response
										commitrequest.setStatusCode(1);
										commitrequest.setMessage(Utility.statusPending);
										commitrequest.setTxnID(apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
												.getTranID() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getTranID()
														: "");
										commitrequest.setResponseCode(apiresponse.getBody().getSingle_Payment_Corp_Resp()
												.getHeader().getResp_cde() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
																.getResp_cde()
														: "");
										commitrequest.setDisplayMessage(apiresponse.getBody().getSingle_Payment_Corp_Resp()
										.getHeader().getStatus() != null
												? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
														.getError_Desc()
												: "");

										if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody() != null) {

											commitrequest.setActCode(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getRRN() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getRRN()
															: "");

											commitrequest.setRequestID(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getRefNo() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getRefNo()
															: "");
											commitrequest.setUserTrackId(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getChannelpartnerrefno() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getChannelpartnerrefno()
															: "");
											commitrequest.setAmountRequested(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getAmount() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getAmount()
															: "0.0");
											commitrequest.setBeneName(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getBenIFSC() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getBenIFSC()
															: "");
											commitrequest.setTransactionDatetime(apiresponse.getBody()
													.getSingle_Payment_Corp_Resp().getBody().getTxn_Time() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getTxn_Time()
															: "");
											commitrequest.setCustomerNumber(apiresponse.getBody().getSingle_Payment_Corp_Resp()
													.getBody().getBen_Acct_No() != null
															? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																	.getBen_Acct_No()
															: "");
										} else {
											//if in the response Body part is NUll
											commitrequest.setActCode("0");
											commitrequest.setRequestID("");
											commitrequest.setUserTrackId("");
											commitrequest.setAmountRequested(amount.toString());
											commitrequest.setTransactionDatetime("");
											commitrequest.setCustomerNumber("");
										}

								

									}

								} catch (Exception e) {
									//if any exception occured while hitting bank, treated as a pending transaction
									LOGGER.info("Exception " + e.getMessage());
									LOGGER.info("api response pending block for wallet cashout : " + apiresponse.getBody()
											.getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
															.getResp_cde()
													: "");

									LOGGER.info("Error message from bank : " + apiresponse.getBody()
											.getSingle_Payment_Corp_Resp().getHeader().getStatus() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
															.getError_Desc()
													: "");
									//printing the bank response message if the response code is not null
									if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null) {
										LOGGER.info("Bank Response : " + rblImpsResponseCode(
												apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()));
									}
									
									//setting up commit request object to update the transaction based on the bank response
									commitrequest.setStatusCode(1);
									commitrequest.setMessage(Utility.statusPending);
									commitrequest.setTxnID(
											apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getTranID() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
															.getTranID()
													: "");
									commitrequest.setResponseCode(apiresponse.getBody().getSingle_Payment_Corp_Resp()
											.getHeader().getResp_cde() != null
													? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
															.getResp_cde()
													: "");
									commitrequest.setDisplayMessage(apiresponse.getBody().getSingle_Payment_Corp_Resp()
											.getHeader().getStatus() != null
											? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader()
													.getError_Desc()
											: "");

									if (apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody() != null) {

										commitrequest.setActCode(
												apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRRN() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRRN()
														: "");

										commitrequest.setRequestID(
												apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody().getRefNo() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																.getRefNo()
														: "");
										commitrequest.setUserTrackId(apiresponse.getBody().getSingle_Payment_Corp_Resp()
												.getBody().getChannelpartnerrefno() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																.getChannelpartnerrefno()
														: "");
										commitrequest.setAmountRequested(apiresponse.getBody().getSingle_Payment_Corp_Resp()
												.getBody().getAmount() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																.getAmount()
														: "0.0");
										commitrequest.setBeneName(apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
												.getBenIFSC() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																.getBenIFSC()
														: "");
										commitrequest.setTransactionDatetime(apiresponse.getBody().getSingle_Payment_Corp_Resp()
												.getBody().getTxn_Time() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																.getTxn_Time()
														: "");
										commitrequest.setCustomerNumber(apiresponse.getBody().getSingle_Payment_Corp_Resp()
												.getBody().getBen_Acct_No() != null
														? apiresponse.getBody().getSingle_Payment_Corp_Resp().getBody()
																.getBen_Acct_No()
														: "");
									} else {
										//if response body part is NULL
										commitrequest.setActCode("0");
										commitrequest.setRequestID("");
										commitrequest.setUserTrackId("");
										commitrequest.setAmountRequested(amount.toString());
										commitrequest.setTransactionDatetime("");
										commitrequest.setCustomerNumber("");
									}


								}

							}

						} else {
							//Bank response is null, so treated as a failed transaction
							LOGGER.info("NULL response from bank");
							LOGGER.info("CASHOUT:Enter into Refund block for user" 
									+ ", out TxId: " + id);

							commitrequest.setStatusCode(-1);
							commitrequest.setActCode("0");
							commitrequest.setMessage(Utility.statusRefund);
							commitrequest.setTxnID(
									apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getTranID() != null
											? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getTranID()
											: "");
							commitrequest.setRequestID("");
							commitrequest.setUserTrackId("");
							commitrequest.setAmountRequested(amount.toString());
							commitrequest.setResponseCode(
									apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde() != null
											? apiresponse.getBody().getSingle_Payment_Corp_Resp().getHeader().getResp_cde()
											: "");
							commitrequest.setDisplayMessage("Transaction Failed");


						}

						responsemap =	committransactionstatus(commitrequest);
					
					
					}
					
				}else {
					responsemap.put("status", -1);
					responsemap.put("statusDesc", "Wallet id not mapped");
					return responsemap;
				}
			
			
		}else {
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "User not found");
			return responsemap;
		}
		
		
		
		}else {

			responsemap.put("status", -1);
			responsemap.put("statusDesc", userbank.getMessage());
			return responsemap;
		}
		return responsemap;
	}

	private Map<String, Object> committransactionstatus(RBLCommitTransactionRequestPojo response) {
		
		Map<String, Object> responsemap = new HashMap<String, Object>();
		Long id= null;
		try {
		 id= response.getPipeNo();
		}catch(Exception e) {
			
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "Exception in Commit/Not getting id properly");
			return responsemap;
			
		}
		Optional<WalletCashoutTransaction> transactiondata =  walletjparepository.findById(id);
		
		
		if(transactiondata.isPresent()){
		
			WalletCashoutTransaction transaction = transactiondata.get();
			
			
		if(transaction==null) {
			responsemap.put("status", -1);
			responsemap.put("statusDesc", "Exception in Commit/Not getting id properly");
			return responsemap;
		}
		
		if(transaction.getStatus().equals("INITIATED")) {
			
			 if (response.getStatusCode() == 0) {// SUCCESS
	                LOGGER.info("**************** Commiting success response ************************");
	                
	                try {
	                responsemap = consumeforupdate(response);
	                }catch(Exception e) {
	                	LOGGER.info("Exception in updating transaction"+response.getPipeNo());
	                }
	                
	                transaction.setStatus(Utility.statusSuccess);
	                transaction.setStatusDesc(response.getDisplayMessage());
	                transaction.setBankrefId(response.getActCode() != null ?  response.getActCode() : "");
	                transaction.setApiComment(response.getDisplayMessage());
	                transaction.setUpdatedDate(new Date());

	                
	              //  walletjparepository.save(transaction);

	        		coreservice.savewalletcashouttodb(transaction);

	    			responsemap.put("status", 0);
	    			responsemap.put("statusDesc", "Transaction Success");
                    } else if (response.getStatusCode() == 1) {
                    	

    	                //responsemap = consumeforupdate(response);
                    	
                    	 transaction.setStatus(Utility.statusPending);
                         transaction.setBankrefId(response.getActCode() != null ? response.getActCode() : "");
                         transaction.setApiComment(response.getDisplayMessage());
                         transaction.setUpdatedDate(new Date());
                         transaction.setStatusDesc(response.getDisplayMessage());
                         // transaction details updated here
                      //   walletjparepository.save(transaction);


                 		coreservice.savewalletcashouttodb(transaction);
     	    			responsemap.put("status", 0);
     	    			responsemap.put("statusDesc", "Transaction Pending");
                     }else if(response.getStatusCode() == -1) {
                    	 
                     try {
     	                responsemap = consumeforupdate(response);
                     }catch(Exception e) {
 	                	LOGGER.info("Exception in updating transaction"+response.getPipeNo());
 	                }
                    	 transaction.setStatus(Utility.statusRefund);
                         transaction.setBankrefId(response.getActCode() != null ? response.getActCode() : "");
                         transaction.setApiComment(response.getDisplayMessage());
                         transaction.setUpdatedDate(new Date());
                         transaction.setStatusDesc(response.getDisplayMessage());
                         // transaction details updated here
                        // walletjparepository.save(transaction);

                  		coreservice.savewalletcashouttodb(transaction);

      	    			responsemap.put("status", 0);
      	    			responsemap.put("statusDesc", "Transaction Failed");
                    	 
                     }else {
                    	 
                    	 responsemap.put("status", -1);
             			responsemap.put("statusDesc", "Transaction status not Updated");
             			 
                     }
		
		}
		
		}
		
		return responsemap;
		
	}



	private Map<String, Object> consumeforupdate(RBLCommitTransactionRequestPojo response) {
		
		
		Map<String, Object> responseMap = new HashMap<String, Object>();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		

		JSONObject requestObject = new JSONObject();
		
		
		try {
			requestObject.put("id", response.getPipeNo());
			requestObject.put("statuscode", response.getStatusCode());  //response.getStatusCode()
			requestObject.put("serviceproviderId", response.getServiceproviderid());
			requestObject.put("apiserviceproviderId", response.getServiceproviderid());
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			final RestTemplate restTemplate = new RestTemplate();
			
			HttpEntity<String> entity= new HttpEntity<String>(requestObject.toString(),headers);
			ResponseEntity<String> updatedetails = restTemplate.exchange(walletApiurl+"/api/double/updatetransaction",HttpMethod.POST,entity, String.class);
		         System.out.println("update details:"+updatedetails);
		         
		         responseMap.put("status", 0);
					responseMap.put("statusDesc", "Update successfully");
					
		}catch(Exception e) {
			responseMap.put("status", -1);
			responseMap.put("statusDesc", "Exception in Update");
			
		}
		return responseMap;
	
		
		
		
	
	}



	private UserBank togetUserbankdetails(String authorization) {
		

		
		HttpHeaders headers = new HttpHeaders();
		
		UserBank userbank = new UserBank();
		
		headers.add("Authorization", authorization);
		Gson gson = new Gson();
		try {
			final RestTemplate restTemplate = new RestTemplate();
			
			HttpEntity<String> entity= new HttpEntity<String>(null,headers);
			ResponseEntity<String> userbankdetails = restTemplate.exchange(coreAppUrl+"/getbank/cashout",HttpMethod.GET,entity, String.class);
		System.out.println(userbankdetails);

		LOGGER.info("User bank response :"+userbankdetails);
		if(userbankdetails.getBody()!=null&&userbankdetails.getStatusCode().equals(HttpStatus.OK)) {
			String bankinfo = userbankdetails.getBody();
			userbank = gson.fromJson(bankinfo, UserBank.class);
			return userbank;
		}
		} catch (Exception e) {
		  LOGGER.info("exception in user bank respone :"+e.getMessage());
		  System.out.println(e.getMessage());
		  userbank.setActive(false);
		  userbank.setMessage("Error in fetching bank details,Try again !");
          return userbank;
			
		}
	return userbank;
		
	
	}

	
	
	/**
     * Method to map RBL imps response code with response message
     *
     * @param responseCode - Holds response code
     * @return a string that holds response messages
     */
    private String rblImpsResponseCode(String responseCode) {
        switch (responseCode) {
        case "00":
            return "Approved-Completed successfully";
        case "08":
            return "Technical Error - Beneficiary bank switch respond to NPCI that their CBS is down hence reverse the customer a/c of remitting bank. (or) Bank is not connected to NPCI as beneficiary to receive the transaction (Bank Node Offline)";
        case "91":
            return "Deemed Approved - Bank as beneficiary does not send any response within the NPCI TAT of 30 seconds";
        case "12":
            return "Technical - An invalid transaction initiated by remitting bank ex: Deposit";
        case "20":
            return "Technical - Beneficiary bank has sent an invalid response code to NPCI which is not as per specification";
        case "96":
            return "Technical - Beneficiary bank is not able to process the transaction due to various technical reason";
        case "30":
            return "Technical - Remitting bank / Beneficiary bank has sent the message in wrong format which is not as per NPCI specification";
        case "21":
            return "Technical - Beneficiary bank send response of no action taken due to various technical reason";
        case "68":
            return "Technical - Beneficiary bank send response to NPCI after TAT of 30 seconds which NPCI does not consider";
        case "22":
            return "Technical - Beneficiary banks send suspected malfunction response due to an internal security issues";
        case "13":
            return "Technical - Remitting bank / Beneficiary bank has sends wrong amount in the field specified, which is not as per NPCI specification";
        case "M0":
            return "Business - When beneficiary bank does not respond to NPCI with the status of the transactions (i.e. approved/declined), NPCI will send no response message to remitting bank. Remitting bank sends VR (verification request) to NPCI, NPCI to beneficiary bank. Beneficiary bank will check the status of the original transaction status if the original transaction is not successful then beneficiary bank will send RC-M0, which is an instruction for remitting bank to refund their customer a/c online";
        case "M1":
            return "Business - Beneficiary a/c details sent by remitting bank is incorrect";
        case "M2":
            return "Business - Beneficiary bank set limit for their customer to credit per day as a business requirement";
        case "M3":
            return "Business - Beneficiary a/c is closed or blocked or frozen due to whatsoever reason";
        case "M4":
            return "Business - Beneficiary declines the transaction stating that the a/c Is pertaining to NRI hence cannot credit the a/c";
        case "M5":
            return "Business - Beneficiary bank customer has closed the a/c";
        case "M6":
            return "Business - Remitting bank NDC limit is exceeded, NPCI will decline the transaction and respond back to remitting bank without sending request message to beneficiary bank";
        case "92":
            return "Business - NBIN allotted for the bank as beneficiary is incorrect, hence beneficiary bank declines the transaction and respond back to NPCI/remitting bank to decline the transaction.";
        case "MA":
            return "Business - Any error related to merchant based transactions";
        case "MC":
            return "Business - Remitting bank send P2M or M2P and if the respective beneficiary bank is not live on these transactions, then NPCI will decline the transactions with RC-MC and send it to remitting bank. NPCI does not forward these transactions to the beneficiary banks";
        case "MK":
            return "Business - Merchant related decline transactions";
        case "ML":
            return "Business - Merchant related decline transactions";
        case "MF":
            return "Business - When beneficiary bank receives successful response for P2M or M2P and while forwarding this good response to the merchant beneficiary bank finds merchant is not connected online, hence beneficiary bank sends the response back to NPCI/remitting bank to reverse the customer a/c online";
        case "M9":
            return "Business - Remitting /issuing bank will decline the transaction if the OTP entered by the customer is invalid for merchant based transactions";
        case "MZ":
            return "Business - Remitting /issuing bank will decline the transaction if the OTP entered by the customer is expired for merchant based transactions";
        case "MH":
            return "Business - Remitting /issuing bank will decline the transaction if the OTP entered by the customer is amount varies for merchant based transactions";
        case "MG":
            return "Business - Bank has not yet enabled this type of transaction for the customer";
        case "05":
            return "Business - Beneficiary declines the transactions with do not honour reason due to various business reason";
        case "94":
            return "Business - Beneficiary bank declines the transactions of the NBIN+RRN is same";
        case "34":
            return "Business - Beneficiary banks suspect that the particular transaction is suspected to be fraud";
        case "MP":
            return "Business - Beneficiary bank is live on P2P but not live on P2A, if a remitting bank sends P2A to this particular transaction then NPCI will decline with the RC-MP";
        case "40":
            return "Business - Beneficiary bank declines with the business decline reason invalid debit account";
        case "01":
            return "Business - Applicable to M2P TXN";
        case "M8":
            return "Business - Applicable to M2P TXN";
        case "MV":
            return "Business - Same as above MP (NPCI declines the transaction if a particular bank as beneficiary is not live on P2U";
        case "04":
            return "Business - Beneficiary declines the transactions with do not honour reason due to various business reason";
        case "52":
            return "Business - Beneficiary a/c details are not correct";
        case "MU":
            return "Business - Applicable to P2U Aadhar based TXN";
        default:
            return "unknown error";
        }
    }
    

@Override
public Map<String, Object> getWalletCashoutservicceforrefresh(WalletCashoutrequestRefresh request) {


    Map<String, Object> responsevalue = new HashMap<String, Object>();
    
    //Get the transaction details 
    Optional<WalletCashoutTransaction> cashouttransaction = this.walletjparepository.findById(request.getTxnId());
    
    


    if (cashouttransaction.isPresent()) {
        
        WalletCashoutTransaction transaction = cashouttransaction.get();


        LOGGER.info(transaction.getId()+" transaction status before update "+transaction.getStatus());
        if (transaction.getStatus().equals("INITIATED") || transaction.getStatus().equals("PENDING")) {


            try {
            ResponseEntity<RBLQueryResponsePojo> aepscashoutStatusResponse;
            aepscashoutStatusResponse = rblTransactionQuery(request.getTxnId().toString());
            RBLQueryResponsePojo aepscashOutStatus = (RBLQueryResponsePojo) aepscashoutStatusResponse.getBody();


            responsevalue = statusclearOpreation(aepscashOutStatus, request.getTxnId());
            
            return responsevalue;
            }catch(Exception e) {
                responsevalue.put("status", -1);
                responsevalue.put("statusDesc", "Exception Occured/Check report");
                return responsevalue;
            }


        } else {
            responsevalue.put("status", -1);
            responsevalue.put("statusDesc", "Only Initiated/Pending transaction allowed");
            return responsevalue;
        }


    } else {
        responsevalue.put("status", -1);
        responsevalue.put("statusDesc", "Transaction is not present in DB/Enter valid transaction id");
        return responsevalue;
    }
    
   }


@Override
public ResponseEntity<RBLQueryResponsePojo> rblTransactionQuery(String txId) {
    



    LOGGER.info(txId+" call to bank api ");



    RBLQueryRequestPojo request = new RBLQueryRequestPojo();



    Get_Single_Payment_Status_Corp_Req req = new Get_Single_Payment_Status_Corp_Req();



    com.iserveu.rbl.query.model.request.Body body = new com.iserveu.rbl.query.model.request.Body();
    com.iserveu.rbl.query.model.request.Header header = new com.iserveu.rbl.query.model.request.Header();
    com.iserveu.rbl.query.model.request.Signature signature = new com.iserveu.rbl.query.model.request.Signature();



    body.setOrgTransactionID(txId);
    header.setCorp_ID(getCorpID());
    header.setTranID(txId);
    header.setApprover_ID("");
    header.setChecker_ID("");
    header.setMaker_ID("");
    signature.setSignature("Signature");



    req.setHeader(header);
    req.setBody(body);
    req.setSignature(signature);
    request.setGet_Single_Payment_Status_Corp_Req(req);



    RBLQueryResponsePojo queryresponse = rblimpsservice.consumeRBLTransactionQuery(request).getBody();



    return new ResponseEntity<>(queryresponse, HttpStatus.OK);

}

private Map<String, Object> statusclearOpreation(RBLQueryResponsePojo aepscashOutStatus, Long txnId) {


    Map<String, Object> responsevalue = new HashMap<String, Object>();


    Optional<WalletCashoutTransaction> cashouttransaction = this.walletjparepository.findById(txnId);


    if (cashouttransaction.isPresent()) {
        WalletCashoutTransaction transaction = cashouttransaction.get();


        if (transaction.getStatus().equals("INITIATED") || transaction.getStatus().equals("PENDING")) {
            
            RBLCommitTransactionRequestPojo response = new RBLCommitTransactionRequestPojo();
            
            if (aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader().getStatus().equalsIgnoreCase("FAILED")
                    || aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader().getStatus()
                            .equalsIgnoreCase("FAILURE")) {
                transaction.setStatus(Utility.statusRefund);
                transaction.setApiComment("Reversed");
                transaction.setBankrefId((null != aepscashOutStatus && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn()
                                ? aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn() : ""));
                transaction.setUpdatedDate(new Date());
                try {
                    Map<String, Object> responsemap = new HashMap<String, Object>();
                    response.setPipeNo(transaction.getId());
                    response.setStatusCode(-1);
                    response.setServiceproviderid(transaction.getServiceProviderId());
                    
                    
                    responsemap = consumeforupdate(response);
                } catch (Exception e) {
                    LOGGER.info("Exception in updating transaction" + response.getPipeNo());
                }
                


                coreservice.savewalletcashouttodb(transaction);
                responsevalue.put("status", 0);
                responsevalue.put("statusDesc", "Transaction Converted to Refund");
                return responsevalue;
                


            }


            else if (aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader().getStatus()
                    .equalsIgnoreCase("SUCCESS")
                    && aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getPaymentstatus().equals("8")) {





                transaction.setStatus(Utility.statusRefund);
                transaction.setApiComment("Reversed");


                transaction.setBankrefId((null != aepscashOutStatus && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn()
                                ? aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn() : ""));
                transaction.setUpdatedDate(new Date());
                
                try {
                    Map<String, Object> responsemap = new HashMap<String, Object>();
                    response.setPipeNo(transaction.getId());
                    response.setStatusCode(-1);
                    response.setServiceproviderid(transaction.getServiceProviderId());
                    
                    
                    responsemap = consumeforupdate(response);
                } catch (Exception e) {
                    LOGGER.info("Exception in updating transaction" + response.getPipeNo());
                }


                coreservice.savewalletcashouttodb(transaction);
                responsevalue.put("status", 0);
                responsevalue.put("statusDesc", "Transaction Converted to Refund");
                return responsevalue;
                


            }
            else if (aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader().getStatus()
                    .equalsIgnoreCase("SUCCESS")
                    && aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getPaymentstatus().equals("7")) {



                
                transaction.setStatus(Utility.statusSuccess);
                transaction.setBankrefId((null != aepscashOutStatus && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn()
                                ? aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn() : ""));



                transaction.setApiComment("COMPLETED");



                transaction.setUpdatedDate(new Date());
                
                
                try {
                    Map<String, Object> responsemap = new HashMap<String, Object>();
                    response.setPipeNo(transaction.getId());
                    response.setStatusCode(0);
                    response.setServiceproviderid(transaction.getServiceProviderId());
                    
                    
                    responsemap = consumeforupdate(response);
                } catch (Exception e) {
                    LOGGER.info("Exception in updating transaction" + response.getPipeNo());
                }
                



                coreservice.savewalletcashouttodb(transaction);
                responsevalue.put("status", 0);
                responsevalue.put("statusDesc", "Transaction Converted to Success");
                return responsevalue;




            }



            else {



                transaction.setStatus(Utility.statusPending);
                
                transaction.setApiComment((null != aepscashOutStatus && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader().getStatus()
                                ? aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getHeader().getStatus() : ""));
                transaction.setBankrefId((null != aepscashOutStatus && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody()
                        && null != aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn()
                                ? aepscashOutStatus.getGet_Single_Payment_Status_Corp_Res().getBody().getRrn() : ""));



                transaction.setUpdatedDate(new Date());




                coreservice.savewalletcashouttodb(transaction);
                responsevalue.put("status", 1);
                responsevalue.put("statusDesc", "Transaction is Pending");
                return responsevalue;



            }
            
            
            



        } else {
            responsevalue.put("status", -1);
            responsevalue.put("statusDesc", "Only Initiated/Pending transaction allowed");
            return responsevalue;
        }



    } else {
        responsevalue.put("status", -1);
        responsevalue.put("statusDesc", "Transaction is not present/Enter valid transaction id");
        return responsevalue;
    }
}




























    
    
    
}
