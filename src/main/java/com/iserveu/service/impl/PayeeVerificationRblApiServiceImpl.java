package com.iserveu.service.impl;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.util.Arrays;
import java.util.function.Function;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.request.pojo.BeneficiaryAccValidationReq;
import com.iserveu.request.pojo.PayeeNameValidationRblRequest;
import com.iserveu.request.pojo.RblPayeeRequestBody;
import com.iserveu.request.pojo.RblRequestHeader;
import com.iserveu.request.pojo.RblRequestPojo;
import com.iserveu.request.pojo.RblRequestSignature;
import com.iserveu.response.pojo.BeneficiaryAccValidationRes;
import com.iserveu.response.pojo.PayeenamevalidationRblResponse;
import com.iserveu.response.pojo.RblPayeeResponseBody;
import com.iserveu.service.PayeeVerificationRblApiService;

/**
 * Implementation class for RBL API service layer.
 * 
 * @author Bhanumati
 *
 */
@Service
@PropertySource("classpath:payeeVerification.properties")
public class PayeeVerificationRblApiServiceImpl implements PayeeVerificationRblApiService {

	private static final Logger LOGGER = LogManager.getLogger(PayeeVerificationRblApiServiceImpl.class);

	@Value("file:/home/iserveutech/keys/aepscashout/isuonlinerblnew.pfx")
	private Resource file;

	@Value("${route.rbl.payeeNamevalidation.Url}")
	private String payeeNamevalidateBaseUrl;

	@Value("${route.rbl.ldapId}")
	private String ldapId;

	@Value("${route.rbl.ldapPassword}")
	private String ldapPassword;

	@Override
	public ResponseEntity<PayeenamevalidationRblResponse> consumeForPayeeNameValidation(RblRequestPojo request)
			throws Exception {
		LOGGER.info(
				"************************ Inside consumeForPayeeNameValidation() method of PayeeVerificationRblApiServiceImpl ******************");
		System.out.println(
				"************************ Inside consumeForPayeeNameValidation() method of PayeeVerificationRblApiServiceImpl ******************");
		PayeeNameValidationRblRequest rblRequest = externalPayeeToRblrequest.apply(request);
		return consumePayeenamevalidationApi(rblRequest);
	}
	Function<RblRequestPojo, PayeeNameValidationRblRequest> externalPayeeToRblrequest = p -> {
		PayeeNameValidationRblRequest rblRequest = new PayeeNameValidationRblRequest();
		BeneficiaryAccValidationReq beneficiaryAccValidationReq = new BeneficiaryAccValidationReq();
		RblRequestHeader header = new RblRequestHeader();
		header.setApprover_ID("");
		header.setChecker_ID("");
		header.setCorp_ID(ldapId.trim());
//		header.setCorp_ID("ISERVEUTEC");
		header.setMaker_ID("");
		header.setTranID(p.getTranID().trim());
		beneficiaryAccValidationReq.setHeader(header);

		RblPayeeRequestBody body = new RblPayeeRequestBody();
		body.setAccountNumber(p.getAccountNumber());
		body.setBeneficiaryMobileNumber(p.getBeneficiaryMobileNumber());
		body.setBeneficiaryName(p.getBeneficiaryName());
		body.setIfscCode(p.getIfscCode());
		beneficiaryAccValidationReq.setBody(body);

		RblRequestSignature signature = new RblRequestSignature();
		signature.setSignature("");
		beneficiaryAccValidationReq.setSignature(signature);

		rblRequest.setBeneficiaryAccValidationReq(beneficiaryAccValidationReq);
		return rblRequest;
	};

	/*
	 * This method is to consume RBL payeeName Validation Api for validate payeeName
	 * 
	 */
	public ResponseEntity<PayeenamevalidationRblResponse> consumePayeenamevalidationApi(
			PayeeNameValidationRblRequest request) {

		LOGGER.info(
				"***************** Inside consumePayeenamevalidationApi() method of PayeeVerificationRblApiServiceImpl class **********************");

		Gson gson = new Gson();
		String jsonReqData = "{" + "\"beneficiaryAccValidationReq\"" + ":" + "{" + "\"Header\"" + ":" + "{"
				+ "\"TranID\"" + ":" + "\"" + request.getBeneficiaryAccValidationReq().getHeader().getTranID() + "\""
				+ "," + "\"Corp_ID\"" + ":" + "\"" + request.getBeneficiaryAccValidationReq().getHeader().getCorp_ID()
				+ "\"" + "," + "\"Maker_ID\"" + ":" + "\""
				+ request.getBeneficiaryAccValidationReq().getHeader().getMaker_ID() + "\"" + "," + "\"Checker_ID\""
				+ ":" + "\"" + request.getBeneficiaryAccValidationReq().getHeader().getChecker_ID() + "\"" + ","
				+ "\"Approver_ID\"" + ":" + "\"" + request.getBeneficiaryAccValidationReq().getHeader().getApprover_ID()
				+ "\"" + "}" + "," + "\"Body\"" + ":" + "{" + "\"beneficiaryName\"" + ":" + "\""
				+ request.getBeneficiaryAccValidationReq().getBody().getBeneficiaryName() + "\"" + ","
				+ "\"beneficiaryMobileNumber\"" + ":" + "\""
				+ request.getBeneficiaryAccValidationReq().getBody().getBeneficiaryMobileNumber() + "\"" + ","
				+ "\"accountNumber\"" + ":" + "\""
				+ request.getBeneficiaryAccValidationReq().getBody().getAccountNumber() + "\"" + "," + "\"ifscCode\""
				+ ":" + "\"" + request.getBeneficiaryAccValidationReq().getBody().getIfscCode() + "\""

				+ "}" + "," + "\"" + "Signature" + "\"" + ":" + "{" + "\"" + "Signature" + "\"" + ":" + "\""
				+ request.getBeneficiaryAccValidationReq().getSignature().getSignature() + "\"" + "}" + "}" + "}";

		LOGGER.info("Request JSON for RBL : " + jsonReqData);
		System.out.println("Request JSON for RBL : " + jsonReqData);
		HttpEntity<String> requestEntity = new HttpEntity<>(jsonReqData, getHttpHeader());

		try {

			RestTemplate restTemplate = getRestClient();
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
					Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

			// logger.info("Channel Partner Login requestEntity :" + requestEntity);

			System.out.println(payeeNamevalidateBaseUrl);
			ResponseEntity<String> response = restTemplate.postForEntity(payeeNamevalidateBaseUrl, requestEntity,
					String.class);

			LOGGER.info("Response for RBL API CALL:" + response.getBody());
			LOGGER.info("Response entity for RBL API CALL:" + response);
			System.out.println("Response for RBL API CALL:" + response.getBody());
			System.out.println("Response entity for RBL API CALL:" + response);
			PayeenamevalidationRblResponse finalresponse = gson.fromJson(response.getBody(),
					PayeenamevalidationRblResponse.class);

			LOGGER.info("Response pojo for RBL API CALL :" + finalresponse.toString());
			System.out.println("Response pojo for RBL API CALL :" + finalresponse.toString());
			return new ResponseEntity<PayeenamevalidationRblResponse>(finalresponse, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("exception :" + e.getStackTrace());
			PayeenamevalidationRblResponse response = new PayeenamevalidationRblResponse();
			BeneficiaryAccValidationRes resp = new BeneficiaryAccValidationRes();
			RblPayeeResponseBody body = new RblPayeeResponseBody();
			body.setStatus("Failure");
			body.setRemarks(e.getMessage());
			resp.setBody(body);
			response.setBeneficiaryAccValidationRes(resp);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	}

	/**
	 * Method to set the http headers.
	 * 
	 * @return HttpHeaders
	 */
	private HttpHeaders getHttpHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		String auth = ldapId + ":" + ldapPassword;
		System.out.println("ldapId=" + ldapId);
		System.out.println("ldapPassword=" + ldapPassword);
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		headers.add("Authorization", authHeader);
		LOGGER.info("Header set :" + headers.toString());
		return headers;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	private RestTemplate getRestClient() throws Exception {

		InputStream keyStoreInputStream = file.getInputStream();

		if (keyStoreInputStream == null) {
			// throw new FileNotFoundException("Could not find file ");
			System.out.println("key file not found");
		}

		System.out.println("RBL service:entering to key store");
		final KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
		try {

			trustStore.load(keyStoreInputStream, "iserveu".toCharArray());
		} finally {
			keyStoreInputStream.close();
		}

		SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(trustStore, "iserveu".toCharArray()).build();
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1.2" }, null,
				SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

		HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
				httpClient);
		final RestTemplate restTemplate = new RestTemplate(httpComponentsClientHttpRequestFactory);

		return restTemplate;

	}

}
