package com.iserveu.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.iserveu.request.pojo.InitiateTxnRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.service.DebitGatewayService;
import com.iserveu.util.IsuUtility;

@Service
public class DebitGatewaySeviceImpl implements DebitGatewayService {
	
	@Value("${core_app_url}")
	private String coreAppBaseUrl;

	
	private Logger logger=LogManager.getLogger(DebitGatewaySeviceImpl.class);
	
	@Autowired
	private Environment env;
	
	public CompletableFuture<Map<String,Object>> debitRetailerWallet(InitiateTxnRequestPojo request,String authorization) {
		//System.out.println(">>startAsync threadName="+Thread.currentThread().getName()+" starttime="+starttime);
		Map<String,Object> responseMap=new HashMap<String,Object>();
		try {
		//Thread.sleep(1000);
		HttpHeaders headers= new HttpHeaders();
		headers.add("Content-Type","application/json");
		headers.add("Authorization",authorization);
		Gson gson = new Gson();
		HttpEntity<String> requestEntity = new HttpEntity<>(gson.toJson(request), headers);
		final RestTemplate restTemplate = new RestTemplate();
		//logger.info("simplcash request for getting redirection link= " + requestEntity);
		ResponseEntity<String> debitResponseEntity = restTemplate.postForEntity(coreAppBaseUrl.trim()+"/debit/retailer_wallet", requestEntity,String.class);
		//logger.info("simplcash response for after comsuming api "+simplcashResponse.getBody());
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String,String> debitResponse=objectMapper.readValue(debitResponseEntity.getBody(),Map.class);
		if(debitResponseEntity.getBody()!=null && debitResponse.get(IsuUtility.STATUS).equals("0") && (debitResponseEntity.getStatusCode().equals(HttpStatus.OK))) {
			responseMap.put(IsuUtility.STATUS,"0");
			responseMap.put(IsuUtility.STATUS_DESC,debitResponse.get(IsuUtility.STATUS_DESC));
			responseMap.put(IsuUtility.TRANSACTION_ID,debitResponse.get(IsuUtility.TRANSACTION_ID));
		//	logger.info("successfully got redirection response ="+simplcashResponse.getBody());
			return CompletableFuture.completedFuture(responseMap);
		}
		responseMap.put(IsuUtility.STATUS,"-1");
		responseMap.put(IsuUtility.TRANSACTION_ID,0);
		responseMap.put(IsuUtility.STATUS_DESC,debitResponse.get(IsuUtility.STATUS_DESC));
		return CompletableFuture.completedFuture(responseMap);
		}catch(Exception e) {
			//logger.info(">>comsumeSimplcashApi---->error in server side ");
			logger.error("ERROR : "+e.getMessage());
			responseMap.put(IsuUtility.STATUS,"-1");
			responseMap.put(IsuUtility.TRANSACTION_ID,0);
			responseMap.put(IsuUtility.STATUS_DESC,"unable to debit, try again later");
			return CompletableFuture.completedFuture(responseMap);
		}
	}
	
	@Override
	@Async
	public CompletableFuture<GeneralResponse> startAsycnTxnGateway(InitiateTxnRequestPojo request,String authorization) {
		//Long starttime=System.currentTimeMillis();
		//System.out.println(">>startAsync threadName="+Thread.currentThread().getName());
		System.out.println(">>Thread Name of startAsycnTxnGateway ()="+Thread.currentThread().getName());
		String riskcovryCallbackUrl=env.getProperty("riskcovry_callback_url");
		CompletableFuture<GeneralResponse> future=CompletableFuture.supplyAsync(()->this.debitRetailerWallet(request,authorization)).
        thenComposeAsync(callBackResponse->this.afterGatewayTxnCallback(callBackResponse,riskcovryCallbackUrl));
		System.out.println("<<Thread name of startAsyncTxnGateway="+Thread.currentThread().getName());
		//long endtime=System.currentTimeMillis();
		return CompletableFuture.completedFuture(new GeneralResponse("0","transaction request recieved",null));	
	}
	
	public CompletableFuture<GeneralResponse> afterGatewayTxnCallback(CompletableFuture<Map<String,Object>> response,String callbackUrl) {
		
		GeneralResponse generalResponse=new GeneralResponse();
		Map<String,Object> responseMap= new HashMap<String,Object>();
			try {
				Map<String,Object> txnResponse= response.get();
				Thread.sleep(1000);
				HttpHeaders headers= new HttpHeaders();
				headers.add("Content-Type","application/json");
				Gson gson = new Gson();
				HttpEntity<String> requestEntity = new HttpEntity<>(gson.toJson(txnResponse), headers);
				final RestTemplate restTemplate = new RestTemplate();
				//logger.info("simplcash request for getting redirection link= " + requestEntity);
				ResponseEntity<String> callbackResponseEntity = restTemplate.postForEntity(callbackUrl, requestEntity,String.class);
				//logger.info("simplcash response for after comsuming api "+simplcashResponse.getBody());
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
				objectMapper.setSerializationInclusion(Include.NON_NULL);
				objectMapper.setSerializationInclusion(Include.NON_EMPTY);
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				Map<String,Object> callbackResponse=objectMapper.readValue(callbackResponseEntity.getBody(),Map.class);
				if(callbackResponseEntity.getBody()!=null && callbackResponse.get(IsuUtility.STATUS).equals("0") && (callbackResponseEntity.getStatusCode().equals(HttpStatus.OK))) {
					generalResponse.setStatus("0");
					generalResponse.setStatusDesc((String)callbackResponse.get(IsuUtility.STATUS_DESC));
					//logger.info("successfully got redirection response ="+simplcashResponse.getBody());
					System.out.println("callback response=");
					System.out.print(callbackResponse.get("statusDesc"));
					return CompletableFuture.completedFuture(generalResponse);
				}
				generalResponse.setStatus("-1");
				generalResponse.setStatusDesc((String)callbackResponse.get(IsuUtility.STATUS_DESC));
				System.out.println("<<startAsync ThreadName="+Thread.currentThread().getName()+" response="+txnResponse);
				System.out.println("callback response=");
				System.out.print(callbackResponse.get("statusDesc"));
				return CompletableFuture.completedFuture(generalResponse);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			generalResponse = new GeneralResponse("-1","error in transaction","");
			long endtime=System.currentTimeMillis();
			System.out.println("<<startAsync ThreadName="+Thread.currentThread().getName()+" endtTime="+endtime);
			System.out.print(" error="+e1.getMessage()+" "+e1.getLocalizedMessage());
			return CompletableFuture.completedFuture(generalResponse);
		}
	}
}
