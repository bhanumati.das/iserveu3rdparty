package com.iserveu.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Arrays;

import javax.net.ssl.SSLContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.iserveu.rbl.query.model.request.RBLQueryRequestPojo;
import com.iserveu.rbl.query.model.response.Get_Single_Payment_Status_Corp_Res;
import com.iserveu.rbl.query.model.response.RBLQueryResponsePojo;
import com.iserveu.request.pojo.RBLIMPSRequestPojo;
import com.iserveu.response.pojo.Body;
import com.iserveu.response.pojo.Header;
import com.iserveu.response.pojo.RBLIMPSResponsePojo;
import com.iserveu.response.pojo.Single_Payment_Corp_Resp;
import com.iserveu.service.RBLIMPSService;

@Service
public class RBLIMPSServieImpl implements RBLIMPSService {
	
	
	private static final Logger LOGGER = LogManager.getLogger(RBLIMPSServieImpl.class);
	
	
	@Value("file:/home/iserveutech/keys/aepscashout/isuonlinerblnew.pfx")
	private Resource file;

	//Production LDAP:
	private static String plainCreds = "ISERVEU:Welcome@123";
	
	//Production URL:
	private static String baseUrl = "https://gateway.rblbank.com/prod/sb/rbl/v1/payments/corp/payment?client_id=fd580f3c-62ae-4cf9-acbc-755b0e8ce9f3&client_secret=dO8rJ3oT5qS6kQ3fU2gF1qX1xT3fS1cP8nT7mS5fQ1iV1sX2fG";
	
	//Production URL for Transaction Query:
    private static String baseUrlQuery = "https://gateway.rblbank.com/prod/sb/rbl/v1/payments/corp/payment/query?client_id=fd580f3c-62ae-4cf9-acbc-755b0e8ce9f3&client_secret=dO8rJ3oT5qS6kQ3fU2gF1qX1xT3fS1cP8nT7mS5fQ1iV1sX2fG";
	

	@Override
	public ResponseEntity<RBLIMPSResponsePojo> calltorblimpstransaction(RBLIMPSRequestPojo request) {

		LOGGER.info("Insert in bank consume");
		HttpHeaders headers = new HttpHeaders();
		// String plainCreds = "ISERVEUTEC:Iserveu@123";

		// String plainCreds = "ISERVEUTEC:Welcome@123";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		// HttpHeaders headers = new HttpHeaders();
		// headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.APPLICATION_JSON);
		// headers.add("Authorization", "Basic
		// SVNFUlZFVVRFQzpXZWxjb21lQDEyMw==");//staging
		headers.add("Authorization", "Basic SVNFUlZFVTpXZWxjb21lQDEyMw=="); // production

		Gson gson = new Gson();

		String jsonReqData = "{" + "\"Single_Payment_Corp_Req\"" + ":" + "{" + "\"Header\"" + ":" + "{" + "\"TranID\""
				+ ":" + "\"" + request.getSingle_Payment_Corp_Req().getHeader().getTranID() + "\"" + "," + "\"Corp_ID\""
				+ ":" + "\"" + request.getSingle_Payment_Corp_Req().getHeader().getCorp_ID() + "\"" + ","
				+ "\"Maker_ID\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getHeader().getMaker_ID() + "\""
				+ "," + "\"Checker_ID\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getHeader().getChecker_ID()
				+ "\"" + "," + "\"Approver_ID\"" + ":" + "\""
				+ request.getSingle_Payment_Corp_Req().getHeader().getApprover_ID() + "\"" + "}" + "," + "\"Body\""
				+ ":" + "{" + "\"Amount\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getAmount()
				+ "\"" + "," + "\"Debit_Acct_No\"" + ":" + "\""
				+ request.getSingle_Payment_Corp_Req().getBody().getDebit_Acct_No() + "\"" + "," + "\"Debit_Acct_Name\""
				+ ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getDebit_Acct_Name() + "\"" + ","
				+ "\"Debit_IFSC\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getDebit_IFSC() + "\""
				+ "," + "\"Debit_Mobile\"" + ":" + "\""
				+ request.getSingle_Payment_Corp_Req().getBody().getDebit_Mobile() + "\"" + "," + "\"Ben_IFSC\"" + ":"
				+ "\"" + request.getSingle_Payment_Corp_Req().getBody().getBen_IFSC() + "\"" + "," + "\"Ben_Acct_No\""
				+ ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getBen_Acct_No() + "\"" + ","
				+ "\"Ben_Name\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getBen_Name() + "\""
				+ "," + "\"Ben_BankName\"" + ":" + "\""
				+ request.getSingle_Payment_Corp_Req().getBody().getBen_BankName() + "\"" + "," + "\"Ben_Email\"" + ":"
				+ "\"" + request.getSingle_Payment_Corp_Req().getBody().getBen_Email() + "\"" + "," + "\"Ben_Mobile\""
				+ ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getBen_Mobile() + "\"" + ","
				+ "\"Mode_of_Pay\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getMode_of_Pay()
				+ "\"" + "," + "\"Remarks\"" + ":" + "\"" + request.getSingle_Payment_Corp_Req().getBody().getRemarks()
				+ "\"" + "}" + "," + "\"" + "Signature" + "\"" + ":" + "{" + "\"" + "Signature" + "\"" + ":" + "\""
				+ request.getSingle_Payment_Corp_Req().getSignature().getSignature() + "\"" + "}" + "}" + "}";

		//logger.info("Request JSON for RBL IMPS : " + jsonReqData);
		HttpEntity<String> requestEntity = new HttpEntity<>(jsonReqData, headers);

		//logger.info("Request entity for RBL IMPS : " + requestEntity);
		System.out.println("request entity :"+requestEntity);

		LOGGER.info("request entity"+requestEntity);

		// HttpEntity<String> requestEntity = new HttpEntity<>(xmlString, headers);

		try {

			System.out.println("try block 1---entry");

			LOGGER.info("try block 1---entry");
			
			InputStream keyStoreInputStream = file.getInputStream();

			if (keyStoreInputStream == null) {
				// throw new FileNotFoundException("Could not find file ");
				System.out.println("key file not found");
			}

			System.out.println("rbl service:entering to key store");
			final KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			try {

				trustStore.load(keyStoreInputStream, "iserveu".toCharArray());
			} finally {
				keyStoreInputStream.close();
			}


			System.out.println("rbl going to bank");
			
			SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(trustStore, "iserveu".toCharArray()).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1.2" },
					null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			final RestTemplate restTemplate = new RestTemplate(httpComponentsClientHttpRequestFactory);

			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
					Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);


			ResponseEntity<String> response = restTemplate.postForEntity(baseUrl, requestEntity, String.class);

			System.out.println("Bank response before parse:"+response);
			LOGGER.info("Bank response before parse:"+response);

			

			RBLIMPSResponsePojo finalresponse = gson.fromJson(response.getBody(), RBLIMPSResponsePojo.class);
			System.out.println("Bank response:"+finalresponse);
			LOGGER.info("Bank response:"+finalresponse);

			return new ResponseEntity<>(finalresponse, HttpStatus.OK);

		} catch (Exception e) {

			System.out.print("Exception in calling bank"+e.getMessage());
			LOGGER.info("Exception in calling bank"+e.getMessage());
			
			
			//logger.info("exception :" + e);
			RBLIMPSResponsePojo response = new RBLIMPSResponsePojo();
			Single_Payment_Corp_Resp resp = new Single_Payment_Corp_Resp();
			Body body = new Body();
			Header header = new Header();
			header.setStatus("Pending");
			header.setError_Desc("Exception");
			resp.setHeader(header);
			response.setSingle_Payment_Corp_Resp(resp);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

	}
	
	  @Override
	    public ResponseEntity<RBLQueryResponsePojo> consumeRBLTransactionQuery(RBLQueryRequestPojo request) {

	 

		  LOGGER.info("<<====Bank api call=====>>");
	        
	        HttpHeaders headers = new HttpHeaders();

	 

	        byte[] plainCredsBytes = plainCreds.getBytes();
	        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
	        String base64Creds = new String(base64CredsBytes);

	 


	        headers.setContentType(MediaType.APPLICATION_JSON);
	        headers.add("Authorization", "Basic SVNFUlZFVTpXZWxjb21lQDEyMw==");
	        
	        Gson gson = new Gson();

	 

	        
	        String jsonReqData = "{" + "\"get_Single_Payment_Status_Corp_Req\"" + ":" + "{" + "\"Header\"" + ":" + "{"
	                + "\"TranID\"" + ":" + "\"" + request.getGet_Single_Payment_Status_Corp_Req().getHeader().getTranID()
	                + "\"" + "," + "\"Corp_ID\"" + ":" + "\""
	                + request.getGet_Single_Payment_Status_Corp_Req().getHeader().getCorp_ID() + "\"" + "," + "\"Maker_ID\""
	                + ":" + "\"" + request.getGet_Single_Payment_Status_Corp_Req().getHeader().getMaker_ID() + "\"" + ","
	                + "\"Checker_ID\"" + ":" + "\""
	                + request.getGet_Single_Payment_Status_Corp_Req().getHeader().getChecker_ID() + "\"" + ","
	                + "\"Approver_ID\"" + ":" + "\""
	                + request.getGet_Single_Payment_Status_Corp_Req().getHeader().getApprover_ID() + "\"" + "}" + ","
	                + "\"Body\"" + ":" + "{" + "\"OrgTransactionID\"" + ":" + "\""
	                + request.getGet_Single_Payment_Status_Corp_Req().getBody().getOrgTransactionID() + "\""

	 

	                + "}" + "," + "\"" + "Signature" + "\"" + ":" + "{" + "\"" + "Signature" + "\"" + ":" + "\""
	                + request.getGet_Single_Payment_Status_Corp_Req().getSignature().getSignature() + "\"" + "}" + "}"
	                + "}";

	 

	        LOGGER.info("Request JSON for RBL Query : " + jsonReqData);
	        
	        HttpEntity<String> requestEntity = new HttpEntity<>(jsonReqData, headers);

	 

	        LOGGER.info("Request entity for RBL Query : " + requestEntity);

	 


	        try {

	 

	            System.out.println("try block 1---entry");
	            LOGGER.info("try block 1---entry");

	 

	            InputStream keyStoreInputStream = file.getInputStream();

	 

	            if (keyStoreInputStream == null) {
	            
	                System.out.println("key file not found");
	                LOGGER.info("key file not found");
	            }

	 

	            System.out.println("rbl service:entering to key store");
	            final KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	            try {

	 

	                trustStore.load(keyStoreInputStream, "iserveu".toCharArray());
	            } finally {
	                keyStoreInputStream.close();
	            }

	 

	            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(trustStore, "iserveu".toCharArray()).build();
	            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1.2" },
	                    null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
	            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

	 

	            HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
	                    httpClient);
	            final RestTemplate restTemplate = new RestTemplate(httpComponentsClientHttpRequestFactory);

	 

	            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
	            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
	                    Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
	            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

	 

	            // logger.info("Channel Partner Login requestEntity :" + requestEntity);

	 

	            ResponseEntity<String> response = restTemplate.postForEntity(baseUrlQuery, requestEntity, String.class);

	 

	            // String response= "{ \"Single_Payment_Corp_Resp\":{ \"Header\":{
	            // \"TranID\":\"1511944295627\", \"Corp_ID\":\"MC009\", \"Maker_ID\":\"M003\",
	            // \"Checker_ID\":\"C003\", \"Approver_ID\":\"A003\", \"Status\":\"Success\",
	            // \"Resp_cde\":\"91\", \"Error_Desc\":\" \" }, \"Body\":{
	            // \"RefNo\":\"SPMC0091511944295627\",
	            // \"channelpartnerrefno\":\"IMPSMC0091511944295627\", \"RRN\":\"733314573138\",
	            // \"Ben_Acct_No\":\"33017400905\", \"Amount\":\"1.01\",
	            // \"BenIFSC\":\"SBIN0012345\", \"Txn_Time\":\"2017-11-29 14:03:23.607778\" },
	            // \"Signature\":{ \"Signature\":\"Signature\" } } }";

	 

	            // ResponseEntity<String> response = new ResponseEntity<String>
	            // ("{\"get_Single_Payment_Status_Corp_Res\": { \"Header\": { \"TranID\":
	            // \"5774\", \"Corp_ID\": \"ISERVEU\", \"Maker_ID\": \"\", \"Checker_ID\": \"\",
	            // \"Approver_ID\": \"\", \"Status\": \"SUCCESS\", \"Error_Cde\": \"\",
	            // \"Error_Desc\": \"TEST ITPL\" }, \"Body\": { \"ORGTRANSACTIONID\": \"5774\",
	            // \"REFNO\": \"SPISERVEU5774\", \"RRN\": \"817115002872\", \"AMOUNT\": \"1.0\",
	            // \"PAYMENTSTATUS\": \"7\", \"REMITTERNAME\": \"ISERVEU\", \"REMITTERMBLNO\":
	            // \"9861067399\", \"BENEFICIARYNAME\": \"mano mano\", \"BANK\": \"HDFC BANK\",
	            // \"IFSCCODE\": \"HDFC0002058\", \"BEN_ACCT_NO\": \"50100109092763\",
	            // \"TXNTIME\": \"2018-06-20 15:14:48.12\" }, \"Signature\": {\"Signature\":
	            // \"Signature\"} }}", HttpStatus.OK);

	 

	            ObjectMapper objectMapper = new ObjectMapper();
	            RBLQueryResponsePojo finalresponse = new RBLQueryResponsePojo();

	 

	            try {
	                finalresponse = objectMapper.readValue(response.getBody(), RBLQueryResponsePojo.class);
	                LOGGER.info("RBL Transaction Query API response " + response.toString());
	            } catch (JsonParseException e) {
	                LOGGER.info("RBL Transaction Query jsonparse exception = " + e);
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            } catch (JsonMappingException e) {
	                LOGGER.info("RBL Transaction Query JsonMappingException = " + e);
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            } catch (IOException e) {
	                LOGGER.info("RBL Transaction Query IOException= " + e);
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }

	 

	            if (response.getStatusCode().equals(HttpStatus.OK)) {

	 

	                LOGGER.info("Response pojo for RBL Query :" + finalresponse.toString());

	 

	                return new ResponseEntity<>(finalresponse, HttpStatus.OK);

	 

	            } else {
	                RBLQueryResponsePojo finresponse = new RBLQueryResponsePojo();
	                Get_Single_Payment_Status_Corp_Res res = new Get_Single_Payment_Status_Corp_Res();
	                com.iserveu.rbl.query.model.response.Header head = new com.iserveu.rbl.query.model.response.Header();
	                head.setStatus("Bad request retreiving transaction query");
	                res.setHeader(head);
	                finresponse.setGet_Single_Payment_Status_Corp_Res(res);

	 

	                return new ResponseEntity<>(finresponse, HttpStatus.BAD_REQUEST);

	 

	            }

	 

	        } catch (Exception e) {

	 

	            LOGGER.info("exception :" + e);
	            RBLQueryResponsePojo response = new RBLQueryResponsePojo();
	            Get_Single_Payment_Status_Corp_Res res = new Get_Single_Payment_Status_Corp_Res();
	            com.iserveu.rbl.query.model.response.Header head = new com.iserveu.rbl.query.model.response.Header();
	            head.setStatus("Error while retreiving transaction query");
	            res.setHeader(head);
	            response.setGet_Single_Payment_Status_Corp_Res(res);

	 

	            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	        }

	 

	    }

}
