package com.iserveu.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.entity.SimplcashWebhook;
import com.iserveu.entity.UserTranslateEntity;
import com.iserveu.repository.SimplcashWebhookRepository;
import com.iserveu.repository.UserTranslateRespository;
import com.iserveu.request.pojo.SimplcashRedirectRequestPojo;
import com.iserveu.request.pojo.SimplcashWebhookInitiateRequestPojo;
import com.iserveu.request.pojo.SimplcashWebhookUpdateRequestPojo;
import com.iserveu.response.pojo.GeneralApiResponse;
import com.iserveu.service.SimplcashService;
import com.iserveu.util.IsuUtility;

/**
 * service impl consist of service implementations necessary for simplcash feature
 * 
 * @author Raj Kishore
 *
 */

@Service
@PropertySource("classpath:simplcash_configurations.properties")
public class SimplcashServiceImpl implements SimplcashService {

	//Instance to read values stores in properties file
	@Autowired
	private Environment env;
	
	@Autowired
	private UserTranslateRespository userTranslateRepository;
	
	@Autowired
	private SimplcashWebhookRepository webhookResponseRepository;
	
	private static SimpleDateFormat dbTimeStamp= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private Logger logger=LogManager.getLogger(SimplcashServiceImpl.class);
	
	/**
	 * service to get simplcash redirection url by passing necessary request Body params
	 * 
	 */
	@Override
	public Map<String, Object> getSimplcashRedirectionUrl(SimplcashRedirectRequestPojo requestBody) {
		logger.info(">>getting redirection process is started");
		Map<String,Object> responseMap=new HashMap<String,Object>();
		try {
		Date timeStamp=new Date(Long.parseLong(requestBody.getDate_time()));
		requestBody.setDate_time(dbTimeStamp.format(timeStamp));
		}
		catch(Exception e) {
			logger.error("error in parsing date !!!");
			responseMap.put(IsuUtility.STATUS,"-1");
			responseMap.put("response", null);
			responseMap.put(IsuUtility.STATUS_DESC,"please send proper date_time");
			return responseMap;
		}
		try {
		HttpHeaders headers= new HttpHeaders();
		String xApiKey=env.getProperty("Simplcash-X-Api-Key");
		String xAuthKey=env.getProperty("Simplcash-X-Auth-Token");
		headers.add("Content-Type","application/json");
		headers.add("X-Api-Key",xApiKey);
		headers.add("X-Auth-Token",xAuthKey);
		String baseUrl=env.getProperty("simpl_cash_baseUrl");
		Gson gson = new Gson();
		HttpEntity<String> requestEntity = new HttpEntity<>(gson.toJson(requestBody), headers);
		final RestTemplate restTemplate = new RestTemplate();
		logger.info("simplcash request for getting redirection link= " + requestEntity);
		ResponseEntity<Map> simplcashResponse = restTemplate.postForEntity(baseUrl, requestEntity,Map.class);
		logger.info("simplcash response for after comsuming api "+simplcashResponse.getBody());
		if(simplcashResponse.getBody()!=null && (boolean)simplcashResponse.getBody().get("success")==true && (simplcashResponse.getStatusCode().equals(HttpStatus.ACCEPTED)||simplcashResponse.getStatusCode().equals(HttpStatus.OK))) {
			responseMap.put(IsuUtility.STATUS,"0");
			responseMap.put(IsuUtility.STATUS_DESC,"success in getting redirection url");
			responseMap.put("response",simplcashResponse.getBody());
			logger.info("successfully got redirection response ="+simplcashResponse.getBody());
			return responseMap;
		}
		responseMap.put(IsuUtility.STATUS,"-1");
		responseMap.put("response", simplcashResponse.getBody());
		responseMap.put(IsuUtility.STATUS_DESC,"server error, try after sometime");
		return responseMap;
		}catch(Exception e) {
			logger.info(">>comsumeSimplcashApi---->error in server side ");
			logger.error("ERROR : "+e);
			responseMap.put(IsuUtility.STATUS,"-1");
			responseMap.put("response",null);
			responseMap.put(IsuUtility.STATUS_DESC,"server is not reachable, please contact iserveu support to resolve this issue");
			return responseMap;
		}
	}
	
	/**
	 *service to initiate webhook for initiated transaction Id before debitting retailer wallet for simplcash  
	 */
	@Override
	public void savewebhookResponse(SimplcashWebhookInitiateRequestPojo request) {
			SimplcashWebhook webhookResponse=new SimplcashWebhook();
			Optional<SimplcashWebhook> webhookResponseOptional= webhookResponseRepository.findById(request.getTransactionId().toString());
			if(!webhookResponseOptional.isPresent()) {
			logger.info("valid request : saving request into webhook, request="+request);
			try {
				webhookResponse.setTimeStamp(dbTimeStamp.parse(request.getTimeStamp()));
				webhookResponse.setAmount(request.getAmount());
				webhookResponse.setClientId(request.getClientId());
				webhookResponse.setId(request.getTransactionId().toString());
				webhookResponse.setType(request.getType());
				webhookResponse.setStatus(request.getStatus());
				webhookResponse.setRetailerId(request.getRetailerId());
				webhookResponse.setOriginUrl(request.getOrigin_url());
				webhookResponse.setPartnerId(request.getPartnerId());
				webhookResponseRepository.save(webhookResponse);
			}catch(Exception e) {
				logger.info("error in saving webhook response");
				logger.error("ERROR : "+e);
				e.printStackTrace();
			}
			}
			else {
			logger.info("invalid request: tried to override existing webhook");
			}
	}
	/**
	 * after debit retailer wallet webhook should be updated with final simplcash transaction status
	 */
	@Override
	public void updateTransactionReferenceForSimplcash(SimplcashWebhookUpdateRequestPojo request) {
		Optional<SimplcashWebhook> webhookResponseOptional= webhookResponseRepository.findById(request.getTransactionId());
		if(webhookResponseOptional.isPresent()) {
		logger.info("valid update request: valid update request for existing transaction, request="+request);
		SimplcashWebhook webhookResponse=webhookResponseOptional.get();
		try {
			webhookResponse.setTimeStamp(dbTimeStamp.parse(request.getTimeStamp()));		
			webhookResponse.setStatus(request.getStatus());
			webhookResponse=webhookResponseRepository.save(webhookResponse);
			logger.info("webhook updated successfully"+webhookResponse);
			}catch(Exception e) {
				logger.info("error in updating webhook response exception="+e);
				logger.error("ERROR : "+e);
				e.printStackTrace();
			}
		}
		logger.info("invalid update request: transaction not present for update, request="+request);
	}
	
	@Override
	public Boolean findExistingParams(String param1,String param2) {
		List<UserTranslateEntity> userTransalates=userTranslateRepository.findByParam1AndParam2(param1,param2);
		if(userTransalates.size()!=0) {
			return true;
		}
		else return false;
		
	}

	/**
	 * service to validate wallet debit operation for simplcash transaction
	 */
	@Override
	public GeneralApiResponse validateSimplcashDebitOperation(SimplcashWebhookUpdateRequestPojo validateRequest) {
		logger.info(">>validateSimplcashDebitOperation");
		Optional<SimplcashWebhook> webhookResponseOptional= webhookResponseRepository.findById(validateRequest.getTransactionId());
		GeneralApiResponse responseMap= new GeneralApiResponse();
		if(webhookResponseOptional.isPresent()) {
			SimplcashWebhook simplcashWebhook=webhookResponseOptional.get();
			logger.info("found transaction in webhook , webhook transaction="+simplcashWebhook);
			if(simplcashWebhook.getStatus().equals("INITIATE")) {//:TODO after deciding the allowed webhook response for retailer debit
//				Date lastWebhookUpdateTime=simplcashWebhook.getTimeStamp();
//				try {
//					Date currentValidationTime=dbTimeStamp.parse(validateRequest.getTimeStamp());
//					if(((currentValidationTime.getTime()-lastWebhookUpdateTime.getTime())/1000)<180) {
						responseMap.setStatus("0");
						responseMap.setStatusDesc("VALID REQUEST");
						Map<String,Object> webhookData= new HashMap<String,Object>();
						webhookData.put("client",simplcashWebhook.getClientId());
						responseMap.setResponse(webhookData);
						return responseMap;
//					}
//					responseMap.setStatus("-1");
//					responseMap.setStatusDesc("REQUEST EXPIRED");
//					return responseMap;
	
			}
			logger.info("no transacstion found in the webhook with status='INITIATE' for transactionId="+validateRequest.getTransactionId());
			responseMap.setStatus("-1");
			responseMap.setStatusDesc("Invalid request");
			return responseMap;
		}
		logger.info("no transaction found in webhook with transactionId="+validateRequest.getTransactionId());
		responseMap.setStatus("-1");
		responseMap.setStatusDesc("Invalid Request");
		return responseMap;
	}

}
