package com.iserveu.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.entity.WalletCashoutTransaction;
import com.iserveu.repository.WalletCashoutEntiyRepository;
import com.iserveu.request.pojo.WalletOpreationRequest;
import com.iserveu.service.BigQueryService;
import com.iserveu.service.CoreService;




@Service
public class CoreServiceImpl implements CoreService{
	
	

	private static final Logger LOGGER = LogManager.getLogger(CoreServiceImpl.class);
	
	@Autowired
	WalletCashoutEntiyRepository walletcashoutjparepository;
	
	@Value("${walletApiurl}")
	private String walletApiurl;
	
	@Autowired
	BigQueryService bigQueryService;

	@Override
	public Map<String, Object> makewalletopreation(WalletOpreationRequest walletrequest) {
		

		HttpHeaders headers = new HttpHeaders();
		
		
		Map<String, Object> response = new HashMap<String, Object>();
		
		headers.add("Content-Type", "application/json");
		
		Gson gson = new Gson();
		try {
			final RestTemplate restTemplate = new RestTemplate();
			
			HttpEntity<String> entity= new HttpEntity<String>(gson.toJson(walletrequest),headers);
			ResponseEntity<String> walletresponse = restTemplate.exchange(walletApiurl+"/api/double/debitwallet",HttpMethod.POST,entity, String.class);
		
		if(walletresponse.getBody()!=null&&walletresponse.getStatusCode().equals(HttpStatus.OK)) {
			JSONObject object = new JSONObject(walletresponse.getBody());
			boolean status = object.getBoolean("Status");
			String statusDesc = object.getString("StatusDesc");
			
			response.put("status", status);
			response.put("statusDesc", statusDesc);
			
			
		}
		
		
		
		} catch (Exception e) {
		
			response.put("status", false);
			response.put("statusDesc", "Exception in wallet opreation");
			
			
		}
		return response;
		
	
	}

	@Override
	public void savewalletcashouttodb(WalletCashoutTransaction transaction) {
		
		try {
		
		transaction.setUpdatedDate(new Date());
		walletcashoutjparepository.save(transaction);
		
		}catch(Exception e) {
			LOGGER.info("Exception in saving casendra"+e.getMessage());
		}
		
		
		try {
			Map<String, Object> walletcashout = transaction.toHashMap();

			//if (transaction != null && transaction.getStatus() != null) {
				bigQueryService.restCallsavetocashoutdb(walletcashout);
			//}
		} catch (Exception e) {
			//logger.info(e.getMessage());
			e.printStackTrace();
		}

		
		
		
	}

}
