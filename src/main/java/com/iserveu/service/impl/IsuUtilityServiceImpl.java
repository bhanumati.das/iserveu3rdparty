package com.iserveu.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.cassandra.CassandraConnectionFailureException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.entity.BankIINDetailsEntity;
import com.iserveu.entity.FeatureEntity;
import com.iserveu.entity.UserApiMappingEntity;
import com.iserveu.entity.UserFeatureEntity;
import com.iserveu.entity.UserFeatureEntityKey;
import com.iserveu.entity.UserPropertiesEntity;
import com.iserveu.repository.BankIINDetailsRepository;
import com.iserveu.repository.FeatureRepository;
import com.iserveu.repository.UserApiMappingRepository;
import com.iserveu.repository.UserFeatureRepository;
import com.iserveu.repository.UserPropertiesRepository;
import com.iserveu.request.pojo.BankNameToBankDetailsRequestPojo;
import com.iserveu.request.pojo.IINToBankRequestPojo;
import com.iserveu.request.pojo.IdToBankRequestPojo;
import com.iserveu.request.pojo.UserDataRequest;
import com.iserveu.request.pojo.UserPropertiesRequest;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.response.pojo.IINToBankResponsePojo;
import com.iserveu.service.IsuUtilityService;
import com.iserveu.util.IsuUtility;

@Service
public class IsuUtilityServiceImpl implements IsuUtilityService {
	
	@Value("${dmt_app_url}")
	private String dmt_app_url;
	@Autowired
	private BankIINDetailsRepository bankIinDetailsRepository;
	
	@Autowired
	private UserApiMappingRepository userApiRepo;
	
	@Autowired
	private UserPropertiesRepository userpropertyRepo;
	
	@Autowired 
	private FeatureRepository featureRepo;
	
	@Autowired
	private UserFeatureRepository userFeatureRepo;
	
	private Logger logger=LogManager.getLogger(IsuUtilityServiceImpl.class);
	/**
	 * service to get bank details for the given iin number
	 */
	@Override
	public IINToBankResponsePojo getBankDetailsForGivenIIN(IINToBankRequestPojo request) {
		BankIINDetailsEntity bankIinDetailsOptional=this.bankIinDetailsRepository.findByIINNumber(Long.parseLong(request.getIinNumber()));
		if(bankIinDetailsOptional!=null) {
			IINToBankResponsePojo iinToBankResponse= new IINToBankResponsePojo();
			//BankIINDetailsEntity bankIinDetails=bankIinDetailsOptional.get();
			iinToBankResponse.setBankName(bankIinDetailsOptional.getBankName());
			iinToBankResponse.setId(bankIinDetailsOptional.getId());
			iinToBankResponse.setIinNumber(bankIinDetailsOptional.getIINNumber().toString());
			return iinToBankResponse;
		}
		return null;
	}
	/**
	 * service to get bank details for the given id
	 */
	@Override
	public IINToBankResponsePojo getBankDetailsForGivenID(IdToBankRequestPojo request) {
		Optional<BankIINDetailsEntity> bankIinDetailsOptional=this.bankIinDetailsRepository.findById(request.getId());
		if(bankIinDetailsOptional.isPresent()) {
			IINToBankResponsePojo iinToBankResponse= new IINToBankResponsePojo();
			BankIINDetailsEntity bankIinDetails=bankIinDetailsOptional.get();
			iinToBankResponse.setBankName(bankIinDetails.getBankName());
			iinToBankResponse.setId(bankIinDetails.getId());
			iinToBankResponse.setIinNumber(bankIinDetails.getIINNumber().toString());
			return iinToBankResponse;
		}
		return null;
	}
	/**
	 * service to get bank details from the dmt apps for given bank name
	 */
	@Override
	public Map<Boolean,Object> getBankDetailsForGivenBankName(BankNameToBankDetailsRequestPojo request) {
		Map<Boolean,Object> responseMap=new HashMap<Boolean,Object>();
		logger.info(">>getBankDetailsForGivenBankName :-->bank details seach started");
		HttpHeaders headers= new HttpHeaders();
		headers.add("Content-Type","application/json");
		//headers.add("Authorization", authorization);
		String baseUrl=dmt_app_url.trim()+"/api/getbankbranches/"+request.getBankName();//TODO: to be changed before deployement in staging and product
		try {
		HttpEntity<String> requestEntity = new HttpEntity<>(headers);
		Gson gson = new Gson();
		final RestTemplate restTemplate = new RestTemplate();
		logger.info("baseUrl= "+baseUrl+" ,RequestEntity="+requestEntity);
		ResponseEntity<List> existingBankDetails=restTemplate.getForEntity(baseUrl, List.class);
		//ResponseEntity<List> existingBankDetails=restTemplate.exchange(baseUrl, HttpMethod.GET, requestEntity,List.class);
		//logger.info("response="+existingBankDetails);
		if(existingBankDetails!=null && (existingBankDetails.getBody()!=null ) && existingBankDetails.getStatusCode().equals(HttpStatus.OK)) {
			//System.out.println(existingBankDetails.getBody());
			List existingBank=existingBankDetails.getBody();
			if(existingBank!=null &&existingBank.size()!=0){
				logger.info("found bank details");
				responseMap.put(Boolean.TRUE,existingBank);
				return responseMap;
			}
			logger.info("now bank details found for given bank name="+request.getBankName());
			responseMap.put(Boolean.FALSE,"No bank details found for the given bankName ="+request.getBankName());
			return responseMap;
		}
		responseMap.put(Boolean.FALSE,"error response from server,try again");
		return responseMap;
		}catch(Exception e) {
			logger.info("either bank detail doesn't exist, try again later");
			logger.error(" ERROR : "+e);
			responseMap.put(Boolean.FALSE,"either no bank details found, or server error");
			return responseMap;
		}
	}
	
	
		@Override
	    public void saveuserdatatocasendra(UserDataRequest request) {  
	        UserApiMappingEntity userapimap = new UserApiMappingEntity();
	        userapimap.setApiUserId(request.getApiuserid());
	        userapimap.setApiWallet2Id(request.getApiwallet2id());
	        userapimap.setApiWalletId(request.getApiwalletid());
	        userapimap.setUserName(request.getUsername());
	        userapimap.setWallet2Id(request.getWallet2id());
	        userapimap.setWalletId(request.getWalletid());
	        userapimap.setUserId(request.getUserid());
	        userapimap.setCity(request.getCity());
	        userapimap.setState(request.getState());
	        userapimap.setPincode(request.getPincode());
	        try {
	        userApiRepo.save(userapimap);
	        }catch(Exception e) {
	            System.out.println("Exception in user data save"+e.getMessage());
	           
	        }
	       
	    }
	 
	 @Override
	    public void saveuserdatatoproperties(UserPropertiesRequest request) {
	       
	        try {
	        UserPropertiesEntity userproperties = new UserPropertiesEntity();
	        userproperties.setUserId(request.getUserid());
	        userproperties.setAdmin(request.getAdmin());
	        userproperties.setAdminWalletId(request.getAdminwalletid());
	        userproperties.setMaster(request.getMaster());
	        userproperties.setMasterWalletId(request.getMasterwalletid());
	        userproperties.setDistributor(request.getDistributor());
	        userproperties.setDistributerWalletId(request.getDistributorwalletid());
	        userproperties.setCity(request.getCity());
	        userproperties.setState(request.getState());
	       
	        userproperties.setPincode(request.getPincode());
	        userproperties.setUserRole(request.getRole());
	       
	        userpropertyRepo.save(userproperties);
	        }
	        catch (Exception e) {
	            logger.info("Exception in saving user properties data save"+e.getMessage());
	        }
	       
	       
	    }
	 
	 @Override
		public GeneralResponse featureCheck(UserApiMappingEntity userApi,Integer featureId) {
			logger.info("\t------------fetching user api address-----------------");
			try {
				Optional<FeatureEntity> featureOpt =this.featureRepo.findById(featureId);
				if(!featureOpt.isPresent()) {
					logger.info("\t feature doesn't exist");
					logger.info("\t------------------------------------------------------");
					return new GeneralResponse("-1","feature doesn't exist",null);
				}
				FeatureEntity feature=featureOpt.get();
				if(feature.getIsActive()==null || feature.isActive()==false) {
					return new GeneralResponse("-1","feature  is not active",null);
				}
				logger.info("\t feature is active");
			if(userApi!=null) {
				UserFeatureEntityKey key= new UserFeatureEntityKey();
				key.setUserId(userApi.getUserId());
				key.setFeatureId(feature.getFeatureId());
				Optional<UserFeatureEntity> userFeatureOpt=this.userFeatureRepo.findById(key);
				if(userFeatureOpt.isPresent()) {
					if(userFeatureOpt.get().getIsActive()!=null && userFeatureOpt.get().getIsActive()==true) {
						logger.info("\t user feature is Active");
						logger.info("\t------------------------------------------------------");
						return new GeneralResponse("0","feature and user feature is active",null);
					}else {
						logger.info("\t user feature is Not Active");
						logger.info("\t------------------------------------------------------");
						return new GeneralResponse("-1","user feature is Not Active",null);
					}
				}else {
					logger.info("\t user doesn't have this feature");
					logger.info("\t------------------------------------------------------");
					return new GeneralResponse("-1","user doesn't have this feature",null);
				}
			}
			logger.info("\tunable to identify user");
			logger.info("\t------------------------------------------------------");
			return new GeneralResponse("-1","unable to identify user",null);
		}catch(CassandraConnectionFailureException e) {
			return new GeneralResponse("-1","failed to establish connection, try again after few minutes",null);
		}
		}

}
