package com.iserveu.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.ssl.OpenSSL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iserveu.request.pojo.TxnStatusWrapper;
import com.iserveu.response.pojo.TrimResponseData;
import com.iserveu.response.pojo.TxnStatusResponse;
import com.iserveu.service.PayeeVerificationFinoApiService;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSource;

/**
 * Implementation class for Fino API service layer.
 * 
 * @author Bhanumati
 *
 */
@Service
@PropertySource("classpath:payeeVerification.properties")
public class PayeeVerificationFinoApiServiceImpl implements PayeeVerificationFinoApiService {

	private static final Logger LOGGER = LogManager.getLogger(PayeeVerificationFinoApiServiceImpl.class);

	@Autowired
	Environment env;

	public static String convert(InputStream inputStream, Charset charset) throws IOException {
		return IOUtils.toString(inputStream, charset);
	}

	private String BASE_URL() {
		return env.getProperty("finoBaseUrl");
	}

	private String header_Value() {
		return env.getProperty("finoRequestHeader");
	}

	private String header_Key() {
		return env.getProperty("finoRequestHeaderkey");
	}

	private String body_Key() {
		return env.getProperty("finoRequestBodykey");
	}

	/**
	 * Method to get the transaction status.
	 */
	@Override
	public TxnStatusResponse getTransactionStatus(String clientUniqueID) {
		LOGGER.info(
				"********************* Inside getTransactionStatus( ) method of PayeeVerificationFinoApiServiceImpl class ************************");
		TxnStatusResponse status;
		try {
			TxnStatusWrapper statusResponse = consumeTxnStatus(clientUniqueID);

			LOGGER.info("TxnStatus APIResponse set : " + statusResponse.toString());

			LOGGER.info("TxnStatus Response : " + statusResponse.toString());

			LOGGER.info("TxnStatus ResponseCode : " + statusResponse.getResponseCode());
			LOGGER.info("TxnStatus ClientUniqueID : " + statusResponse.getClientUniqueID());
			LOGGER.info("TxnStatus ResponseData : " + statusResponse.getResponseData());
			LOGGER.info("TxnStatus DisplayMessage : " + statusResponse.getDisplayMessage());
			LOGGER.info("TxnStatus RequestID : " + statusResponse.getRequestID());

			if (statusResponse.getResponseCode().equals("0")) {
				status = new TxnStatusResponse();

				status.setResponseCode(
						statusResponse.getResponseCode() == null ? "" : statusResponse.getResponseCode());
				status.setMessageString(
						statusResponse.getMessageString() == null ? "" : statusResponse.getMessageString());
				status.setDisplayMessage(
						statusResponse.getDisplayMessage() == null ? "" : statusResponse.getDisplayMessage());
				status.setClientUniqueID(
						statusResponse.getClientUniqueID() == null ? "" : statusResponse.getClientUniqueID());
				status.setRequestID(statusResponse.getRequestID() == null ? "" : statusResponse.getRequestID());

				status.setResponseData(
						statusResponse.getResponseData() == null ? "" : statusResponse.getResponseData());

				Type collectiontype = new TypeToken<TrimResponseData>() {
				}.getType();
				TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(statusResponse.getResponseData(),
						collectiontype);

				status.setActCode(trimmedData.getActCode() == null ? "" : trimmedData.getActCode());
				status.setTxnID(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
				status.setAmountRequested(
						trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
				status.setChargesDeducted(
						trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
				status.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getChargesDeducted());
				status.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
				status.setRfu1(trimmedData.getRfu1() == null ? "" : trimmedData.getRfu1());
				status.setRfu2(trimmedData.getRfu2() == null ? "" : trimmedData.getRfu2());
				status.setRfu3(trimmedData.getRfu3() == null ? "" : trimmedData.getRfu3());
				status.setTransactionDatetime(
						trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());
				status.setTxnDescription(
						trimmedData.getTxnDescription() == null ? "" : trimmedData.getTxnDescription());

				if ((trimmedData.getActCode().equals("26")) || (trimmedData.getActCode().equals("11"))
						|| trimmedData.getActCode().equalsIgnoreCase("S")) {
					status.setStatusCode(0);
					status.setStatus("Success");
					status.setBeneName(trimmedData.getBeneName());
					status.setMessage(
							impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));
				} else if ((trimmedData.getActCode().equals("100")) || (trimmedData.getActCode().equals("23"))
						|| (trimmedData.getActCode().equals("21")) || (trimmedData.getActCode().equals("12"))
						|| (trimmedData.getActCode().equals("R"))) {
					status.setStatusCode(2);
					status.setStatus("Failure");
					status.setMessage(
							impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));
				} else {
					status.setStatusCode(1);
					status.setStatus("Pending");
					status.setMessage(
							impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));

				}

				LOGGER.info("TxnStatus Response set : " + status.toString());
				return status;
			}

			else {

				status = new TxnStatusResponse();

				status.setResponseCode(
						statusResponse.getResponseCode() == null ? "" : statusResponse.getResponseCode());
				status.setMessageString(
						statusResponse.getMessageString() == null ? "" : statusResponse.getMessageString());
				status.setDisplayMessage(
						statusResponse.getDisplayMessage() == null ? "" : statusResponse.getDisplayMessage());
				status.setClientUniqueID(
						statusResponse.getClientUniqueID() == null ? "" : statusResponse.getClientUniqueID());
				status.setRequestID(statusResponse.getRequestID() == null ? "" : statusResponse.getRequestID());

				status.setResponseData(
						statusResponse.getResponseData() == null ? "" : statusResponse.getResponseData());

				Type collectiontype = new TypeToken<TrimResponseData>() {
				}.getType();
				TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(statusResponse.getResponseData(),
						collectiontype);

				status.setActCode(trimmedData.getActCode() == null ? "" : trimmedData.getActCode());
				status.setTxnID(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
				status.setAmountRequested(
						trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
				status.setChargesDeducted(
						trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
				status.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getChargesDeducted());
				status.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
				status.setRfu1(trimmedData.getRfu1() == null ? "" : trimmedData.getRfu1());
				status.setRfu2(trimmedData.getRfu2() == null ? "" : trimmedData.getRfu2());
				status.setRfu3(trimmedData.getRfu3() == null ? "" : trimmedData.getRfu3());
				status.setTransactionDatetime(
						trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());
				status.setTxnDescription(
						trimmedData.getTxnDescription() == null ? "" : trimmedData.getTxnDescription());

				status.setStatusCode(-6);
				status.setStatus("Transaction Enquiry Failed.");
				status.setMessage(
						impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));

			}

			LOGGER.info("TxnStatus failed Response set : " + status.toString());
			return status;

		} catch (Exception e) {

			status = new TxnStatusResponse();
			status.setStatusCode(-1);
			status.setStatus("Exception occured");
			LOGGER.info("TxnStatus Response exception : " + e.toString());
			return status;
		}

	}

	/**
	 * Method to consume the API to get the status of transaction.
	 */
	@Override
	public TxnStatusWrapper consumeTxnStatus(String clientUniqueID) {
		LOGGER.info(
				"********************* Inside consumeTxnStatus( ) method of PayeeVerificationFinoApiServiceImpl class ************************");

		try {

			ObjectMapper mapperObj = new ObjectMapper();

			InputStream headerstream = new ByteArrayInputStream(header_Value().getBytes());
			InputStream headerencrypted = OpenSSL.encrypt("AES256", header_Key().getBytes("UTF-8"), headerstream, true);

			MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
			formVars.add("ClientUniqueID", clientUniqueID);

			String txnRequestPacket = mapperObj.writeValueAsString(formVars).replaceAll("\\[", "").replaceAll("\\]",
					"");

			InputStream txnStatusStream = new ByteArrayInputStream(txnRequestPacket.getBytes(Charset.forName("UTF-8")));
			InputStream statusencrypted = OpenSSL.encrypt("AES256", body_Key().getBytes(), txnStatusStream, true);

			String paramValue = (convert(statusencrypted, Charset.forName("UTF-8")).replaceAll("\\s", ""));

			// Staging
			// String uri = BASE_URL() + "/txnstatusrequest";

			// Production
			String uri = BASE_URL() + "/TxnStatusRequest";

			String headerValue = convert(headerencrypted, Charset.forName("UTF-8")).replaceAll("\\s", "");
			String txnparam = "\"" + paramValue + "\"";

			Builder b = new Builder();
			b.readTimeout(90000, TimeUnit.MILLISECONDS);
			b.connectTimeout(90000, TimeUnit.MILLISECONDS);
			OkHttpClient client = b.build();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, txnparam);
			Request request = new Request.Builder().url(uri).post(body).addHeader("authentication", headerValue)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache").build();

			Response response = client.newCall(request).execute();

			BufferedSource source = response.body().source();
			source.request(Long.MAX_VALUE); // Buffer the entire body.
			Buffer buffer = source.buffer();
			String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));

			Type collectiontype = new TypeToken<TxnStatusWrapper>() {
			}.getType();
			TxnStatusWrapper responseJson = (TxnStatusWrapper) new Gson().fromJson(responseBodyString, collectiontype);

			InputStream responseStream = new ByteArrayInputStream(responseJson.getResponseData().getBytes());
			InputStream responsedecrypted = OpenSSL.decrypt("AES256", body_Key().getBytes("UTF-8"), responseStream);

			responseJson.setResponseData(convert(responsedecrypted, Charset.forName("UTF-8")).replaceAll("\\s", ""));

			LOGGER.info("TxnStatus API response :" + responseJson);
			return responseJson;

		} catch (Exception e) {

			TxnStatusWrapper responseJson = new TxnStatusWrapper();
			responseJson.setDisplayMessage("Exception occured!!!");
			LOGGER.info("TxnStatus API response:" + responseJson);
			LOGGER.info("TxnStatus API response stacktrace:" + e.toString());
			LOGGER.info("TxnStatus API response stacktrace:" + e.getMessage());
			return responseJson;

		}

	}

	/**
	 * Method to get the description of status code for IMPS transaction.
	 * 
	 * @param actcode - Holds the response code given by bank
	 * 
	 * @return String
	 */
	private String impsTxnStatusCodeDesc(String actcode) {
		switch (actcode) {
		case "1":
			return "Transaction Pending";
		case "5001":
			return "Unknown";
		case "2":
			return "Transaction is Pending";
		case "7":
			return "Transaction is Pending";
		case "9":
			return "Transaction is Pending";
		case "11":
			return "Transaction Successful";
		case "12":
			return "Transaction Failed";
		case "20":
			return "Transaction is Pending";
		case "21":
			return "Transaction Failed";
		case "23":
			return "Transaction Failed";
		case "26":
			return "Transaction Successful";
		case "200":
			return "Exception";
		case "401":
			return "Full authentication is required to access this resource.";
		case "100":
			return "Record Not found";
		case "R":
			return "Transaction Failed";
		case "S":
			return "Transaction Successful";
		case "P":
			return "Transaction is Pending";
		case "1010":
			return "InvalidRequest";
		case "1002":
			return "AuthenticationFailed";
		case "1004":
			return "TokenGenerationFailed";
		case "999":
			return "TokenExpired";
		case "1009":
			return "RequestIsNull";
		case "1011":
			return "URLNotFound";
		case "1012":
			return "InvalidResponse";
		case "9999":
			return "Txn Posting Failed";
		case "503":
			return "The remote server returned an error";
		case "504":
			return "The remote server returned an error";
		default:
			return "unknown error";
		}
	}

}
