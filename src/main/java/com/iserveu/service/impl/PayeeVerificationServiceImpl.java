package com.iserveu.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.iserveu.entity.PayeeVerification;
import com.iserveu.repository.PayeeVerificationRepo;
import com.iserveu.request.pojo.PayeeVerificationRequest;
import com.iserveu.request.pojo.RblRequestPojo;
import com.iserveu.response.pojo.AbstractResponse;
import com.iserveu.response.pojo.PayeeVerificationResponse;
import com.iserveu.response.pojo.PayeenamevalidationRblResponse;
import com.iserveu.service.PayeeVerificationFinoApiService;
import com.iserveu.service.PayeeVerificationRblApiService;
import com.iserveu.service.PayeeVerificationService;

/**
 * Implementation class for payee verification service.
 * 
 * @author Bhanumati 
 *
 */
@Service
@PropertySource("classpath:payeeVerification.properties")
public class PayeeVerificationServiceImpl implements PayeeVerificationService {

	private static final Logger LOGGER = LogManager.getLogger(PayeeVerificationServiceImpl.class);

	@Autowired
	Environment env;

	@Autowired
	PayeeVerificationFinoApiService payeeVerificationApiService;

	@Autowired
	PayeeVerificationRblApiService payeeVerificationRblApiService;
	
	@Autowired
	PayeeVerificationRepo payeeVerificationRepo;

	private String body_Key() {
		return env.getProperty("finoRequestBodykey");
	}

	private String BASE_URL() {
		return env.getProperty("finoBaseUrl");
	}

	public static String convert(InputStream inputStream, Charset charset) throws IOException {
		return IOUtils.toString(inputStream, charset);
	}

	private String header_Value() {
		return env.getProperty("finoRequestHeader");
	}

	private String header_Key() {
		return env.getProperty("finoRequestHeaderkey");
	}

//	/**
//	 * Method to verify a payee by FINO bank.
//	 */
//	@Override
//	public ResponseEntity<? extends AbstractResponse> verifyPayeeByFino(PayeeVerificationRequest bene) {
//		LOGGER.info("*********************** Inside verifyPayeeByFino() method of PayeeVerificationServiceImpl class ************************"); 
//
//		NeftResponseWrapper responseJson;
////		String clientUniqueId = IsuUtility.getNumbericRandom(6);
//		bene.setGateWayNo(2);
//		PayeeVerificationResponse payeeResponse = new PayeeVerificationResponse();
//
//		try {
//			ObjectMapper mapperObj = new ObjectMapper();
//
//			MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
//			formVars.add("ClientUniqueID", bene.getClientUniqueId());
//			formVars.add("CustomerMobileNo", bene.getCustomerMobileNo());
//			formVars.add("BeneIFSCCode", bene.getBeneIFSCCode());
//			formVars.add("BeneAccountNo", bene.getBeneAccountNo());
//			formVars.add("BeneName", bene.getBeneName());
//			formVars.add("Amount", "1");
//			formVars.add("CustomerName", bene.getCustomerName());
//
//			String neftRequestPacket = mapperObj.writeValueAsString(formVars).replaceAll("\\[", "").replaceAll("\\]",
//					"");
//
//			LOGGER.info("API neftRequestPacket :" + neftRequestPacket.toString());
//			InputStream neftstream = new ByteArrayInputStream(neftRequestPacket.getBytes(Charset.forName("UTF-8")));
//
//			LOGGER.info("API neftRequestPacket neftstream:" + neftstream.toString());
//			InputStream bodyencrypted = OpenSSL.encrypt("AES256", body_Key().getBytes(), neftstream, true);
//
//			LOGGER.info("API neftRequestPacket bodyencrypted:" + bodyencrypted.toString());
//
//			// Staging
//			// String uri = BASE_URL() + "/impsrequest";
//
//			// Production
//			String uri = BASE_URL() + "/IMPSRequest";
//
//			LOGGER.info("API uri: " + uri);
//			String paramValue = (convert(bodyencrypted, Charset.forName("UTF-8")).replaceAll("\\s", ""));
//
//			InputStream headerstream = new ByteArrayInputStream(header_Value().getBytes());
//			InputStream headerencrypted = OpenSSL.encrypt("AES256", header_Key().getBytes("UTF-8"), headerstream, true);
//			String headerValue = convert(headerencrypted, Charset.forName("UTF-8")).replaceAll("\\s", "");
//
//			String neftparam = "\"" + paramValue + "\"";
//
//			LOGGER.info("API neftparam value:" + neftparam);
//			Builder b = new Builder();
//			b.readTimeout(90000, TimeUnit.MILLISECONDS);
//			b.connectTimeout(90000, TimeUnit.MILLISECONDS);
//			OkHttpClient client = b.build();
//
//			MediaType mediaType = MediaType.parse("application/json");
//			RequestBody body = RequestBody.create(mediaType, neftparam);
//
//			Request request = new Request.Builder().url(uri).post(body).addHeader("authentication", headerValue)
//					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache").build();
//
//			Response response = client.newCall(request).execute();
//			LOGGER.info("API okhttp response value:" + response);
//			BufferedSource source = response.body().source();
//			LOGGER.info("API BufferedSource response value:" + source.toString());
//			source.request(Long.MAX_VALUE); // Buffer the entire body.
//			Buffer buffer = source.buffer();
//			String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));
//			LOGGER.info("API responseBodyString  value:" + responseBodyString.toString());
//			Type collectiontype = new TypeToken<NeftResponseWrapper>() {
//			}.getType();
//			responseJson = (NeftResponseWrapper) new Gson().fromJson(responseBodyString, collectiontype);
//
//			LOGGER.info("API responseJson  value:" + responseJson.toString());
//			InputStream responseStream = new ByteArrayInputStream(responseJson.getResponseData().getBytes());
//			InputStream responsedecrypted = OpenSSL.decrypt("AES256", body_Key().getBytes("UTF-8"), responseStream);
//
//			responseJson.setResponseData(
//					convert(responsedecrypted, Charset.forName("UTF-8")).replaceAll("\\s", "").toString());
//
//			responseJson.setClientUniqueID(bene.getClientUniqueId());
//
//			LOGGER.info("API final response :" + responseJson);
//			
////			"{\"ActCode\":\"0\",\"TxnID\":\"002018060109\",\"AmountRequested\":1.0,\"ChargesDeducted\":0.0,\"TotalAmount\":1.0,\"BeneName\":\"BHANUMATIDAS\",\"Rfu1\":null,\"Rfu2\":null,\"Rfu3\":null,\"TransactionDatetime\":\"2020-01-2006:26:43\",\"TxnDescription\":\"IMPS\"}",
////		    "messageString": "Transaction Successful"
//			
//			String responseData = responseJson.getResponseData();
//			
//			responseData = responseData.replace("\'", "");
//			System.out.println(responseData);
//			JSONObject jobj = new JSONObject(responseData);
//			String actCode = jobj.getString("ActCode");
//			String txnId = jobj.getString("TxnID");
//			String beneName = jobj.getString("BeneName");
//			System.out.println("Act code : " + actCode);
//			
//			payeeResponse.setStatus(actCode);
//			payeeResponse.setStatusDesc(responseJson.getDisplayMessage());
//			payeeResponse.setClientUniqueId(responseJson.getClientUniqueID());
//			payeeResponse.setBeneName(beneName);
//			payeeResponse.setMessage(responseJson.getMessageString());
//			payeeResponse.setTxnId(txnId);
//			
//			PayeeVerification pvObj = new PayeeVerification();
//			pvObj.setClientUniqueId(bene.getClientUniqueId());
//			pvObj.setVendorId(bene.getClientUniqueId().substring(0, 2));
//			pvObj.setGateWayNo(bene.getGateWayNo());
//			pvObj.setStatus(((Integer.parseInt(actCode) == 0)?"SUCCESS" : "FAILED"));
//			pvObj.setCreatedDate(new Date());
//			pvObj.setUpdatedDate(new Date());
//			
//			payeeVerificationRepo.save(pvObj);
//
//			return new ResponseEntity<>(payeeResponse, HttpStatus.OK);
//
//		} catch (Exception e) {
//
//			responseJson = new NeftResponseWrapper();
//			LOGGER.info("API response stacktrace:" + e.toString());
//
//			try {
//				LOGGER.info("(nested catch-try)API response exception stacktrace for clientuniqueId:" + bene.getClientUniqueId());
//
//				TxnStatusResponse statusResponse = payeeVerificationApiService.getTransactionStatus(bene.getClientUniqueId());
//				responseJson.setDisplayMessage(
//						statusResponse.getDisplayMessage() == null ? "" : statusResponse.getDisplayMessage());
//				responseJson.setClientUniqueID(bene.getClientUniqueId() == null ? "" : bene.getClientUniqueId());
//				responseJson.setMessageString(
//						statusResponse.getMessageString() == null ? "" : statusResponse.getMessageString());
//				responseJson.setRequestID(statusResponse.getRequestID() == null ? "" : statusResponse.getRequestID());
//				responseJson.setResponseCode(
//						statusResponse.getResponseCode() == null ? "" : statusResponse.getResponseCode());
//				responseJson.setResponseData(
//						statusResponse.getResponseData() == null ? "" : statusResponse.getResponseData());
//				if (statusResponse.getStatusCode() == -6) {
//					responseJson.setResponseCode("-6");
//				}
//				
//				if(!(statusResponse.getResponseData() == null)) {
//					
//					String responseData = statusResponse.getResponseData();
//					
//					responseData = responseData.replace("\'", "");
//					System.out.println(responseData);
//					JSONObject jobj = new JSONObject(responseData);
//					String actCode = jobj.getString("ActCode");
//					String txnId = jobj.getString("TxnID");
//					String beneName = jobj.getString("BeneName");
//					System.out.println("Act code : " + actCode);
//					payeeResponse.setStatus(actCode == null ? "-1" : actCode);
//					payeeResponse.setBeneName(beneName == null ? "" : beneName);
//					payeeResponse.setTxnId(txnId == null ? "0" : txnId);
//					
//				}
//				
//				if (statusResponse.getStatusCode() == -6) {
//					payeeResponse.setStatus("-6");
//				}
//				payeeResponse.setStatusDesc(statusResponse.getDisplayMessage());
//				payeeResponse.setClientUniqueId(statusResponse.getClientUniqueID());
//				payeeResponse.setMessage(statusResponse.getMessageString());
//				
//
//				LOGGER.info("exception for txnRecheck inside catch: " + bene.getClientUniqueId());
//				
//				PayeeVerification pvObj = new PayeeVerification();
//				pvObj.setClientUniqueId(bene.getClientUniqueId());
//				pvObj.setVendorId(bene.getClientUniqueId().substring(0, 2));
//				pvObj.setGateWayNo(bene.getGateWayNo());
//				pvObj.setStatus(((Integer.parseInt(payeeResponse.getStatus()) == 0)?"SUCCESS" : "FAILED"));
//				pvObj.setCreatedDate(new Date());
//				pvObj.setUpdatedDate(new Date());
//				
//				payeeVerificationRepo.save(pvObj);
//
//			} catch (Exception e2) {
//				
//				PayeeVerification pvObj = new PayeeVerification();
//				pvObj.setClientUniqueId(bene.getClientUniqueId());
//				pvObj.setVendorId(bene.getClientUniqueId().substring(0, 2));
//				pvObj.setGateWayNo(bene.getGateWayNo());
//				pvObj.setStatus(((Integer.parseInt(payeeResponse.getStatus()) == 0)?"SUCCESS" : "FAILED"));
//				pvObj.setCreatedDate(new Date());
//				pvObj.setUpdatedDate(new Date());
//				payeeVerificationRepo.save(pvObj);
//				
//				payeeResponse.setStatus("-1");
//				payeeResponse.setStatusDesc("Transaction status check failed");
//				payeeResponse.setClientUniqueId(responseJson.getClientUniqueID());
//				payeeResponse.setBeneName("");
//				payeeResponse.setMessage("Transaction status check failed");
//				payeeResponse.setTxnId("0");
//				
//				LOGGER.info("API response reset with txnstatus response for clientuniqueId :" + bene.getClientUniqueId());
//				return new ResponseEntity<>(payeeResponse, HttpStatus.BAD_REQUEST);
//			}
//			// TransactionStatusRequestPojo requestpojo = new
//			// TransactionStatusRequestPojo();
//			// requestpojo.setBeneAccount(beneAccountNo);
//			// requestpojo.setBeneMobileNo(customerMobileNo);
//			// requestpojo.setClientUniqueId(clientUniqueID.toString());
//			// customerService.updateTransactionStatus(requestpojo, token);
//			// LOGGER.info("core txn status updated after resetting txnstatus
//			// response for clientuniqueId: "+clientUniqueID.toString());
//			return new ResponseEntity<>(payeeResponse, HttpStatus.OK);
//
//		}
//
//	}

	

	/**
	 * Method to verify a payee by RBL bank.
	 */
	@Override
	public ResponseEntity<? extends AbstractResponse> verifyPayeeByRbl(PayeeVerificationRequest request) {
		LOGGER.info(
				"\t----------------Inside verifyPayeeByRbl() method of PayeeVerificationServiceImpl class----------------");
		String[] nameArr = request.getBeneName().split(" ");
		String name = "";
		for (int i = 0; i < nameArr.length; i++) {
			name += nameArr[i];
		}
		request.setBeneName(name);
		// String clientUniqueId = IsuUtility.getNumbericRandom(6);
		request.setGateWayNo(1);
		RblRequestPojo rblRequest = new RblRequestPojo();
		rblRequest.setTranID(request.getParamA().trim());
		rblRequest.setAccountNumber(request.getBeneAccountNo().trim());
		rblRequest.setBeneficiaryMobileNumber(request.getCustomerMobileNo().trim());
		rblRequest.setBeneficiaryName(request.getBeneName().trim());
		rblRequest.setIfscCode(request.getBeneIFSCCode().trim());
		PayeeVerificationResponse response = new PayeeVerificationResponse();
		ResponseEntity<PayeenamevalidationRblResponse> rblResponse = null;
		try {
			rblResponse = payeeVerificationRblApiService.consumeForPayeeNameValidation(rblRequest);
			LOGGER.info("\tRBL Response :  " + rblResponse);
			// System.out.println("RBL Response : " + rblResponse);
			PayeeVerification pvObj = new PayeeVerification();
			// System.out.println(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getNpciResponsecode());
			if (rblResponse.getBody() != null && rblResponse.getBody().getBeneficiaryAccValidationRes() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getHeader() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getNpciResponsecode() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getNpciResponsecode()
							.equals("00")) {
				// System.out.println("if statment");
				response.setStatus("0");
				response.setBeneName(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getBeneName());
				response.setStatusDesc(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getRemarks());
				response.setMessage(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getStatus());
				response.setClientUniqueId(
						rblResponse.getBody().getBeneficiaryAccValidationRes().getHeader().getTranID());
				response.setTxnId(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getBankRefno());
				LOGGER.info(
						"\t(432)Verification for account no " + request.getBeneAccountNo() + "->Response :" + response);
				pvObj.setClientUniqueId(request.getParamA());
				pvObj.setVendorId(request.getParamA().substring(0, 2));
				pvObj.setGateWayNo(request.getGateWayNo());
				pvObj.setStatus((response.getStatus().equalsIgnoreCase("0")) ? "SUCCESS" : "FAILED");
				pvObj.setCreatedDate(new Date());
				pvObj.setUpdatedDate(new Date());
				payeeVerificationRepo.save(pvObj);
				LOGGER.info("\t rbl verification is success, bank status="
						+ (rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getRemarks() == null ? ""
								: rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getRemarks()));
				LOGGER.info(
						"\t------------------------------------------------------------------------------------------------------");
				return new ResponseEntity<>(response, HttpStatus.OK);

			} else if (rblResponse.getBody() != null && rblResponse.getBody().getBeneficiaryAccValidationRes() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getHeader() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getNpciResponsecode() != null
					&& (rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getNpciResponsecode()
							.equals("91")
							||Pattern.matches("[0-9]{12}",rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getNpciResponsecode()))) {
				// System.out.println("if statment");
				response.setStatus("1");
				response.setBeneName(request.getBeneName());
				response.setStatusDesc(
						rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc());
				response.setMessage("Transaction Pending");
				response.setClientUniqueId(
						rblResponse.getBody().getBeneficiaryAccValidationRes().getHeader().getTranID());
				response.setTxnId(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getBankRefno());
				// LOGGER.info(
				// "\t(432)Verification for account no " + request.getBeneAccountNo() +
				// "->Response :" + response);
				pvObj.setClientUniqueId(request.getParamA());
				pvObj.setVendorId(request.getParamA().substring(0, 2));
				pvObj.setGateWayNo(request.getGateWayNo());
				pvObj.setStatus("PENDING");
				pvObj.setCreatedDate(new Date());
				pvObj.setUpdatedDate(new Date());
				payeeVerificationRepo.save(pvObj);
				LOGGER.info("\t rbl revification pnding , reason="
						+ rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc() == null ? " "
								: rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc());
				LOGGER.info(
						"\t------------------------------------------------------------------------------------------------------");
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
			// preparing failed response
			response.setStatus("-1");
			response.setBeneName(request.getBeneName());
			response.setStatusDesc(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc());
			response.setMessage("Transaction Failed");
			response.setTxnId("0");
			// comiting failed response
			pvObj.setClientUniqueId(request.getParamA());
			pvObj.setVendorId(request.getParamA().substring(0, 2));
			pvObj.setGateWayNo(request.getGateWayNo());
			pvObj.setStatus((response.getStatus().equalsIgnoreCase("0")) ? "SUCCESS" : "FAILED");
			pvObj.setCreatedDate(new Date());
			pvObj.setUpdatedDate(new Date());
			payeeVerificationRepo.save(pvObj);
			LOGGER.info("\t rbl revification failed , reason="
					+ rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc() == null ? " "
							: rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc());
			LOGGER.info(
					"\t------------------------------------------------------------------------------------------------------");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {

			// System.out.println(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("0"));
			if ((rblResponse.getBody() != null && rblResponse.getBody().getBeneficiaryAccValidationRes() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody() != null
					&& rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("0"))
					|| rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("ER001")
					|| rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("ER002")
					|| rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("ER003")
					|| rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("ER004")
					|| rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("ER006")
					|| rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde()
							.equals("ER013")) {
				if (rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde().equals("0")) {
					response.setStatus("-1");
				} else {
					response.setStatus(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde());
				}
				// response.setStatus(rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Cde());
				response.setBeneName(request.getBeneName());
				response.setStatusDesc(
						rblResponse.getBody().getBeneficiaryAccValidationRes().getBody().getError_Desc());
				response.setMessage("Transaction Failed");
				response.setTxnId("0");
				PayeeVerification pvObj = new PayeeVerification();
				pvObj.setClientUniqueId(request.getParamA());
				pvObj.setVendorId(request.getParamA().substring(0, 2));
				pvObj.setGateWayNo(request.getGateWayNo());
				pvObj.setStatus((response.getStatus().equalsIgnoreCase("0")) ? "SUCCESS" : "FAILED");
				pvObj.setCreatedDate(new Date());
				pvObj.setUpdatedDate(new Date());
				payeeVerificationRepo.save(pvObj);

			} else {
				response.setStatus("-1");
				response.setStatusDesc("error occured from bank");
				response.setTxnId("0");
				response.setClientUniqueId(request.getParamA());
			}
			LOGGER.info("\tError occured from bank - " + e.toString());
			LOGGER.info(
					"\t------------------------------------------------------------------------------------------------------");
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

//	/**
//	 * Method to verify a payee by Indusind bank.
//	 */
//	@Override
//	public ResponseEntity<? extends AbstractResponse> verifyPayeeByIbl(PayeeVerificationRequest request) {
//
//		String clientUniqueId = IsuUtility.getNumbericRandom(6);
//		IblRequestPojo iblRequest = new IblRequestPojo();
//		iblRequest.setClientUniqueId(clientUniqueId);
//		iblRequest.setCustomerMobileNo(request.getCustomerMobileNo());
//		iblRequest.setBeneIfscCode(request.getBeneIFSCCode());
//		iblRequest.setBeneAccountNo(request.getBeneAccountNo());
//		iblRequest.setBeneName(request.getBeneName());
//		iblRequest.setAmount("1");
//		iblRequest.setCustomerName(request.getCustomerName());
//		iblRequest.setTransactionMode("IMPS");
//
////		PayeeVerificationResponse response = new PayeeVerificationResponse();
//
//		/*
//		 * FinoNeftResponse apiresponse =
//		 * payeeVerificationIblApiService.sendNeftRequest(iblRequest);
//		 * 
//		 * response.setStatus(apiresponse.getStatusCode()); response.setBeneName(
//		 * apiresponse.getBeneName() != null &&
//		 * !(apiresponse.getBeneName().trim().equalsIgnoreCase("") ||
//		 * apiresponse.getBeneName().equalsIgnoreCase("unregistered") ||
//		 * apiresponse.getBeneName().equalsIgnoreCase("beneficiaryname") ||
//		 * apiresponse.getBeneName().equalsIgnoreCase("NA")) ? apiresponse.getBeneName()
//		 * : iblRequest.getBeneName()); response.setStatusDesc(apiresponse.getMessage()
//		 * != null ? apiresponse.getMessage() : "");
//		 * 
//		 * LOGGER.info("(432)Verification for account no" + request.getBeneAccountNo() +
//		 * "->frontendResponse response :" + response);
//		 * 
//		 * return new ResponseEntity<>(response, HttpStatus.OK);
//		 */
//
//		NeftResponseWrapper response;
//		try {
//			// amount = "";
//			// amount = "1";
//			String responseString = "";
//			String outputString = "";
//
//			// Step1:WSDl end point link
//			// String wsURL =
//			// "https://ibluatapig.indusind.com/app/uat/IdirectPayService?client_id=8180ac85-e249-4d84-8c58-4cc5e1b35bba&client_secret=aX4vI0sC3jH6nW2nT4qD2wR5gN7nB0qO1nR7oQ1bJ6pP7yW3jV";
//			// Live ======
//			String wsURL = "https://apig.indusind.com/ibl/prod/IdirectPayService?client_id=200364a1-63d6-4ecd-ac00-3b49057f8b99&client_secret=J1xU6lF3pN3dD3dM3pT4aC5pI0lU1xR8cG8gR1xW1dH1xB2wK1";
////			 String wsURL = "https://www.google.com";
//			URL url = new URL(wsURL);
//			URLConnection connection = url.openConnection();
//			HttpURLConnection httpConn = (HttpURLConnection) connection;
//			ByteArrayOutputStream bout = new ByteArrayOutputStream();
//			// Step2: SOAP request are formed to send the request.
//			/*
//			 * String xmlInput =
//			 * "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
//			 * + "   <soapenv:Header/>" + "   <soapenv:Body>" +
//			 * "      <tem:ProcessTxnInXml>" + "         <!--Optional:-->" +
//			 * "         <tem:strCustId>10580734</tem:strCustId>" +
//			 * "         <!--Optional:-->" +
//			 * "         <tem:strInputTxn><![CDATA[<PaymentRequest><Transaction>" +
//			 * "<TranType>"+transactiontype+"</TranType><CustomerRefNum>"+Long.toString(
//			 * strCustRefNo)+"</CustomerRefNum>" +
//			 * "<DebitAccount>100002076734</DebitAccount><Amount>"+amount+
//			 * "</Amount><ValueDate>"+Utility.getDateWithoutTime()+"</ValueDate>" +
//			 * "<BenName>"+beneName+"</BenName><BENE_ACNO>"+beneAccountNo+"</BENE_ACNO>" +
//			 * "<BENE_IFSC_CODE>"+beneIFSC+
//			 * "</BENE_IFSC_CODE><BENE_BRANCH></BENE_BRANCH><BENE_BANK></BENE_BANK>" +
//			 * "<Bene_MobileNo></Bene_MobileNo><Bene_EmailId></Bene_EmailId><Bene_MMId></Bene_MMId>"
//			 * + "<Rem_Name>AshishMore</Rem_Name><Rem_MobileNo>9348838053</Rem_MobileNo>" +
//			 * "<Rem_Address1>Andheri</Rem_Address1><Rem_Address2>Kothari</Rem_Address2>" +
//			 * "<Rem_PinNo>400059</Rem_PinNo><Rem_KYC_Status>0</Rem_KYC_Status><CustomerId>10580734</CustomerId>"
//			 * +
//			 * "<CustomerName></CustomerName><MakerId>vijaytel2</MakerId><CheckerId>vijayisup2</CheckerId>"
//			 * +
//			 * "<AgentID>1234</AgentID><Reserve1></Reserve1><Reserve2></Reserve2><Reserve3></Reserve3>"
//			 * +
//			 * "<Reserve4></Reserve4><Reserve5></Reserve5></Transaction></PaymentRequest>]]></tem:strInputTxn>"
//			 * + "      </tem:ProcessTxnInXml>" + "   </soapenv:Body>" +
//			 * "</soapenv:Envelope>";
//			 */
//
//			// Live Request
//
//			String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
//					+ "   <soapenv:Header/>" + "   <soapenv:Body>" + "      <tem:ProcessTxnInXml>"
//					+ "         <!--Optional:-->" + "         <tem:strCustId>38739475</tem:strCustId>"
//					+ "         <!--Optional:-->" + "         <tem:strInputTxn><![CDATA[<PaymentRequest><Transaction>"
//					+ "<TranType>" + iblRequest.getTransactionMode() + "</TranType><CustomerRefNum>"
//					+ iblRequest.getClientUniqueId() + "</CustomerRefNum>"
//					+ "<DebitAccount>201003405575</DebitAccount><Amount>" + iblRequest.getAmount()
//					+ "</Amount><ValueDate>" + getDateWithoutTime() + "</ValueDate>" + "<BenName>"
//					+ iblRequest.getBeneName() + "</BenName><BENE_ACNO>" + iblRequest.getBeneAccountNo()
//					+ "</BENE_ACNO>" + "<BENE_IFSC_CODE>" + iblRequest.getBeneIfscCode()
//					+ "</BENE_IFSC_CODE><BENE_BRANCH></BENE_BRANCH><BENE_BANK></BENE_BANK>"
//					+ "<Bene_MobileNo></Bene_MobileNo><Bene_EmailId></Bene_EmailId><Bene_MMId></Bene_MMId>"
//					+ "<Rem_Name>" + iblRequest.getCustomerName() + "</Rem_Name><Rem_MobileNo>"
//					+ iblRequest.getCustomerMobileNo() + "</Rem_MobileNo>"
//					+ "<Rem_Address1></Rem_Address1><Rem_Address2></Rem_Address2>" + "<Rem_PinNo>" + "754006"
//					+ "</Rem_PinNo><Rem_KYC_Status>0</Rem_KYC_Status><CustomerId>38739475</CustomerId>"
//					+ "<CustomerName></CustomerName><MakerId>DEBASHIS</MakerId><CheckerId>DEBIPRASAD</CheckerId>"
//					+ "<AgentID>" + "itpl" + "</AgentID><Reserve1></Reserve1><Reserve2></Reserve2><Reserve3></Reserve3>"
//					+ "<Reserve4></Reserve4><Reserve5></Reserve5></Transaction></PaymentRequest>]]></tem:strInputTxn>"
//					+ "      </tem:ProcessTxnInXml>" + "   </soapenv:Body>" + "</soapenv:Envelope>";
//
//			// dummy data agentID
//			LOGGER.info("Request Data for processIBlTransaction : " + xmlInput);
////			String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
////					+ "   <soapenv:Header/>" + "   <soapenv:Body>" + "      <tem:ProcessTxnInXml>"
////					+ "         <!--Optional:-->" + "         <tem:strCustId>38739475</tem:strCustId>"
////					+ "         <!--Optional:-->" + "         <tem:strInputTxn><![CDATA[<PaymentRequest><Transaction>"
////					+ "<TranType>" + transactiontype + "</TranType><CustomerRefNum>" + Long.toString(strCustRefNo)
////					+ "</CustomerRefNum>" + "<DebitAccount>201003405575</DebitAccount><Amount>" + amount
////					+ "</Amount><ValueDate>" + Utility.getDateWithoutTime() + "</ValueDate>" + "<BenName>" + beneName
////					+ "</BenName><BENE_ACNO>" + beneAccountNo + "</BENE_ACNO>" + "<BENE_IFSC_CODE>" + beneIFSC
////					+ "</BENE_IFSC_CODE><BENE_BRANCH></BENE_BRANCH><BENE_BANK></BENE_BANK>"
////					+ "<Bene_MobileNo></Bene_MobileNo><Bene_EmailId></Bene_EmailId><Bene_MMId></Bene_MMId>"
////					+ "<Rem_Name>BISWAJIT SASMAL</Rem_Name><Rem_MobileNo>8249341738</Rem_MobileNo>"
////					+ "<Rem_Address1>AT-RATAGARH NUASAHI PO-GODISAHI</Rem_Address1><Rem_Address2>CUTTACK ODISHA</Rem_Address2>"
////					+ "<Rem_PinNo>754006</Rem_PinNo><Rem_KYC_Status>0</Rem_KYC_Status><CustomerId>38739475</CustomerId>"
////					+ "<CustomerName></CustomerName><MakerId>DEBASHIS</MakerId><CheckerId>DEBIPRASAD</CheckerId>"
////					+ "<AgentID>itpl</AgentID><Reserve1></Reserve1><Reserve2></Reserve2><Reserve3></Reserve3>"
////					+ "<Reserve4></Reserve4><Reserve5></Reserve5></Transaction></PaymentRequest>]]></tem:strInputTxn>"
////					+ "      </tem:ProcessTxnInXml>" + "   </soapenv:Body>" + "</soapenv:Envelope>";
//
//			/*
//			 * System.out.println("xmlInput--" + xmlInput); logger.info("xmlInput--" +
//			 * xmlInput);
//			 */
//			byte[] buffer = new byte[xmlInput.length()];
//			buffer = xmlInput.getBytes();
//			bout.write(buffer);
//			byte[] b = bout.toByteArray();
//			// Step3: SOAP action change as per the request in the wsdl
//			String SOAPAction = "http://tempuri.org/IPayService/ProcessTxnInXml";
//			// Set the appropriate HTTP parameters.
//			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
//			httpConn.setRequestProperty("SOAPAction", SOAPAction);
//			httpConn.setRequestMethod("POST");
//			httpConn.setDoOutput(true);
//			httpConn.setDoInput(true);
//			OutputStream out = httpConn.getOutputStream();
//
//			// Writing the content of the request to the outputstream of the HTTP
//			// Connection.
//			LOGGER.info("Request for processIBlTransaction : " + xmlInput);
//			out.write(b);
//
//			out.close();
//			InputStreamReader isr = null;
//
//			if (httpConn.getResponseCode() == 200) {
//				isr = new InputStreamReader(httpConn.getInputStream());
//			} else {
//				isr = new InputStreamReader(httpConn.getErrorStream());
//			}
//
//			BufferedReader in = new BufferedReader(isr);
//
//			System.out.println("HTTP RESPONSE CODE" + httpConn.getResponseCode());
//			System.out.println("HTTP RESPONSE MESSAGE" + httpConn.getResponseMessage());
//
//			LOGGER.info("HTTP RESPONSE CODE : " + httpConn.getResponseCode());
//			LOGGER.info("HTTP RESPONSE MESSAGE : " + httpConn.getResponseMessage());
//
//			// Write the SOAP message response to a String.
//			while ((responseString = in.readLine()) != null) {
//				outputString = outputString + responseString;
//			}
//			while ((responseString = in.readLine()) != null) {
//				outputString = outputString + responseString;
//			}
//			LOGGER.info("Response for processIBlTransaction : " + outputString.toString());
////			outputString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><ProcessTxnInXmlResponse xmlns=\"http://tempuri.org/\"><ProcessTxnInXmlResult>&lt;PaymentResponse>&lt;Transaction>&lt;IBLRefNo>W0QJ000022766665&lt;/IBLRefNo>&lt;CustomerRefNo>89753&lt;/CustomerRefNo>&lt;Amount>1&lt;/Amount>&lt;StatusCode>R000&lt;/StatusCode>&lt;StatusDesc>Success&lt;/StatusDesc>&lt;/Transaction>&lt;/PaymentResponse></ProcessTxnInXmlResult></ProcessTxnInXmlResponse></s:Body></s:Envelope>";
//			System.out.println(outputString);
//			// parsing xml string to json
//			JSONObject xmlJSONObj = XML.toJSONObject(outputString);
//			String xml_data = xmlJSONObj.toString();
//
//			// Dommy Response
//			// String
////			String xml_data="{\"s:Envelope\":{\"xmlns:s\":\"http://schemas.xmlsoap.org/soap/envelope/\",\"s:Body\":{\"ProcessTxnInXmlResponse\":{\"xmlns\":\"http://tempuri.org/\",\"ProcessTxnInXmlResult\":\"<PaymentResponse><Transaction><IBLRefNo>W0QJ000022206419<\\/IBLRefNo><CustomerRefNo>89008<\\/CustomerRefNo><Amount>1<\\/Amount><StatusCode>R000<\\/StatusCode><StatusDesc>Success<\\/StatusDesc><\\/Transaction><\\/PaymentResponse>\"}}}}";
//
//			JSONObject data = new JSONObject(xml_data);
//
//			JSONObject obj2 = data.getJSONObject("s:Envelope");
//			JSONObject obj3 = obj2.getJSONObject("s:Body");
//			JSONObject obj4 = obj3.getJSONObject("ProcessTxnInXmlResponse");
//			JSONObject obj5 = XML.toJSONObject(obj4.getString("ProcessTxnInXmlResult").toString());
//			JSONObject obj6 = obj5.getJSONObject("PaymentResponse");
//			JSONObject finalRes = obj6.getJSONObject("Transaction");
//
//			response = new NeftResponseWrapper();
//			response.setStatusCode(finalRes.getString("StatusCode"));
//
//			/*
//			 * response.setStatusDesc(finalRes.getString("StatusDesc"));
//			 * response.setiBLRefNo(finalRes.getString("IBLRefNo"));
//			 * response.setAmount(finalRes.getInt("Amount"));
//			 * response.setCustomerRefNo(finalRes.getLong("CustomerRefNo"));
//			 * response.setStatusCode(finalRes.getString("StatusCode"));
//			 * response.setBeneName(beneName); response.setTransactionType(transactiontype);
//			 * response.setResponsedate(Utility.getDateWithTime());
//			 */
//			LOGGER.info("Response Status Code for processIBlTransaction Status : " + response.getStatusCode());
//			try {
//				if (response.getStatusCode().equalsIgnoreCase("R000")) {
//					response.setStatusDesc(finalRes.getString("StatusDesc"));
//					response.setiBLRefNo(finalRes.getString("IBLRefNo"));
//					response.setAmount(finalRes.getInt("Amount"));
//					response.setCustomerRefNo(finalRes.getLong("CustomerRefNo"));
//					response.setStatusCode(finalRes.getString("StatusCode"));
//					response.setBeneName(iblRequest.getBeneName());
//					response.setTransactionType(iblRequest.getTransactionMode());
//					response.setResponsedate(getDateWithTime());
//					response.setClientUniqueID(finalRes.get("CustomerRefNo").toString());
//					return new ResponseEntity<>(response, HttpStatus.OK);
//					// Thread.sleep(10000);
//					// return
//					// iblAPIService.getIBLTransactionResponse(Long.toString(response.getCustomerRefNo()));
//					// return iblAPIService.getIBLTransactionResponse(strCustRefNo.toString()); //
//					// strCustRefNo
//				} else if (response.getStatusCode().equalsIgnoreCase("R005")) {
//					NeftResponseWrapper errresponse = new NeftResponseWrapper();
//					errresponse.setClientUniqueID(response.getClientUniqueID());
//					errresponse.setiBLRefNo(response.getiBLRefNo());
//					errresponse.setStatusCode("R005");
//					errresponse.setStatusDesc(response.getStatusDesc());
//					LOGGER.info("Internal Server Error - Please try Later." + errresponse.getStatusCode());
//					return new ResponseEntity<>(errresponse, HttpStatus.INTERNAL_SERVER_ERROR);
//
//				}
//			} catch (Exception e) {
//				System.out.println("exception" + e);
//				e.printStackTrace();
//				LOGGER.error("Exception :" + e.getMessage());
//			}
//		} catch (Exception e) {
//			System.out.println("exception" + e);
//			e.printStackTrace();
//			response = new NeftResponseWrapper();
//			response.setDisplayMessage("Exception Occured");
//			LOGGER.error("Exception :" + e.getMessage());
//			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//		}
//		return null;
//
//	}

	public String getDateWithoutTime() {

		return LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
	}

	public String getDateWithTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);// 2016/11/16 12:08:43
	}
}
