package com.iserveu.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.entity.WalletCashoutTransaction;
import com.iserveu.repository.WalletCashoutEntiyRepository;
import com.iserveu.request.pojo.WalletOpreationRequest;
import com.iserveu.service.BigQueryService;
import com.iserveu.service.CoreService;
import com.iserveu.service.WalletService;
import com.iserveu.util.Utility;


@Service
public class WalletServiceImpl implements WalletService {
	
	
	@Autowired
	WalletCashoutEntiyRepository walletjparepository;
	
	
	@Autowired
	CoreService coreservice;
	
	
	@Autowired
	BigQueryService bigqueryservice;
	
	

	@Override
	public Map<String, Object> initiatedtransaction(WalletOpreationRequest initiatedrequest) {
		
		Map<String, Object> initiateresponse = new HashMap<String, Object>();
		
		WalletCashoutTransaction transaction = new WalletCashoutTransaction();
		
		
		Long id = generateuniqueid();
		if(id == null) {
			initiateresponse.put("status", -1);
			initiateresponse.put("statusDesc", "Exception in generating Unique id");
			return initiateresponse;
		}
		transaction.setId(id);
		transaction.setAmount(initiatedrequest.getAmount());
		transaction.setApiUserId(initiatedrequest.getApiuserid());
		transaction.setUserId(initiatedrequest.getUserid());
		transaction.setApiWalletId(initiatedrequest.getApiwalletid());
		transaction.setWalletId(initiatedrequest.getWalletid());
		transaction.setApiServiceProviderId(initiatedrequest.getApiServiceProviderId());
		transaction.setServiceProviderId(initiatedrequest.getServiceProviderId());
		transaction.setAccountNumber(initiatedrequest.getAccountnumber());
		transaction.setOperationPerformed("WALLET2CASHOUT");
		transaction.setCreatedDate(new Date());
		transaction.setUpdatedDate(new Date());
		transaction.setStatus("INITIATED");
		
		
		
		//walletjparepository.save(transaction);
		
		coreservice.savewalletcashouttodb(transaction);
		
		WalletOpreationRequest walletrequest = new WalletOpreationRequest();
		walletrequest.setId(id);
		walletrequest.setAmount(initiatedrequest.getAmount());
		walletrequest.setApiServiceProviderId(initiatedrequest.getApiServiceProviderId());
		walletrequest.setServiceProviderId(initiatedrequest.getServiceProviderId());
		walletrequest.setApiuserid(initiatedrequest.getApiuserid());
		walletrequest.setUserid(initiatedrequest.getUserid());
		walletrequest.setApiwalletid(initiatedrequest.getApiwalletid());
		walletrequest.setWalletid(initiatedrequest.getWalletid());
		walletrequest.setType("WALLET2CASHOUT");
		
		
		
		Map<String, Object> walletresponse = coreservice.makewalletopreation(walletrequest);
		
		if(walletresponse==null) {
			initiateresponse.put("status", -1);
			initiateresponse.put("txnId", id);
			initiateresponse.put("statusDesc", "Wallet Opration Failed");
			
		}
		
		if(!walletresponse.get("status").equals(true)) {
			initiateresponse.put("status", -1);
			initiateresponse.put("txnId", id);
			initiateresponse.put("statusDesc", "Wallet Opration Failed");
			transaction.setApiComment("Wallet Opration Failed");
			transaction.setStatus("REFUNDED");
			coreservice.savewalletcashouttodb(transaction);
			
		}
		
		if(walletresponse.get("status").equals(true)) {
		

			
			initiateresponse.put("status", 0);
			initiateresponse.put("txnId", id);
			initiateresponse.put("statusDesc", "Wallet Opration Success");
		}
		
		return initiateresponse;
	}

	
	Long generateuniqueid(){
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			Gson gson = new Gson();
			
			String uniqueIdUrl = "https://us-central1-iserveustaging.cloudfunctions.net/uniqueid";
			try {
				final RestTemplate restTemplate = new RestTemplate();
				ResponseEntity<String> idGeneratorResponse = restTemplate.getForEntity(uniqueIdUrl, String.class);
				/*logger.info("IDGenerator response="+idGeneratorResponse);*/
				if (idGeneratorResponse != null && idGeneratorResponse.getBody() != null
						&& idGeneratorResponse.getStatusCode().equals(HttpStatus.OK)) {
					/*logger.info("\tgenerated uniqueId successfully");
					logger.info("----------------------------------------------");*/
					return Long.parseLong(gson.fromJson(idGeneratorResponse.getBody(),Map.class).get(Utility.UNIQUE_ID).toString());
				}
				return null;
			} catch (Exception e) {
				/*logger.error(ExceptionUtils.getStackTrace(e));
				logger.info("\tfailed to generate unique id");
				logger.info("----------------------------------------------");*/
				return null;
			}
	}
	
	
}
