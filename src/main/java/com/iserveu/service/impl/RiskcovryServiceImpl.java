package com.iserveu.service.impl;
/**
 * service implementation for riskcovery specific services
 * @author Raj
 *
 */

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.iserveu.request.pojo.RiskcovryEncryptRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.service.RiskcovryService;
import com.iserveu.util.IsuUtility;


@Service
@PropertySource("classpath:riskcovry.properties")
public class RiskcovryServiceImpl implements RiskcovryService {
	@Value("${riskcovry_shared_key}")
	private String sharedKey;
	
	@Value("${riskcovry_redirection_url}")
	private String riskcovryRedirectionUrl;
	
	@Value("${riskcovry_code}")
	private String riskcovryCode;
	
	@Value("${riskcovry_api_key")
	private String riskcoveryApiKey;
	
	@Value("${riskcovry_shared_IV}")
	private String sharedIV;
	
	Logger logger= LogManager.getLogger(RiskcovryServiceImpl.class);
	
	@Override
	public GeneralResponse getEncryptedDataForRiskcovry(RiskcovryEncryptRequestPojo request){
		GeneralResponse generalResponse= new GeneralResponse();
		Map<String,Object> responseMap= new HashMap<String,Object>();
		try {
		Gson gson= new Gson();
		logger.info("request before encryption = "+gson.toJson(request));
		String encryptedCode=IsuUtility.encryptAES256(gson.toJson(request), sharedKey.trim(),sharedIV.trim().getBytes());
		logger.info("request after decrypting encrypted data="+IsuUtility.decryptAES256(encryptedCode, sharedKey.trim(),sharedIV.trim().getBytes()));
		String redirectionUrl=(this.riskcovryRedirectionUrl.replace(IsuUtility.RISKCOVRY_CONST_CODE,this.riskcovryCode)).replace(IsuUtility.ENCRYPTED_CONST_RISKCOVRY,encryptedCode);
		generalResponse.setStatus("0");
		generalResponse.setStatusDesc("redirectionUrl created");
		responseMap.put(IsuUtility.REDIRECT_LINK,redirectionUrl);
		generalResponse.setResponse(responseMap);
		return generalResponse;
		}catch(Exception e) {
			logger.info("error in generating redirection link");
			logger.error("ERROR :"+e.getMessage());
			generalResponse.setStatus("-1");
			generalResponse.setStatusDesc("some error occured");
			return generalResponse;
		}
	}

}
