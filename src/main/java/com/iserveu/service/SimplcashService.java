package com.iserveu.service;

import java.util.Map;

import com.iserveu.entity.SimplcashWebhook;
import com.iserveu.request.pojo.SimplcashRedirectRequestPojo;
import com.iserveu.request.pojo.SimplcashWebhookInitiateRequestPojo;
import com.iserveu.request.pojo.SimplcashWebhookUpdateRequestPojo;
import com.iserveu.response.pojo.GeneralApiResponse;

/**
 * service requirement specification for SimplcashService
 * @author Raj kishore
 *
 */


public interface SimplcashService {
	//-------------------------service to get redirection url for sampoorna bima---------------//
	Map<String,Object> getSimplcashRedirectionUrl(SimplcashRedirectRequestPojo request);
	//-----------------------------------------------------------------------------------------//
	
	//--------------------------save webhook response------------------------------------------//
	void savewebhookResponse(SimplcashWebhookInitiateRequestPojo request);
	//-----------------------------------------------------------------------------------------//
	//-------------------------saving transaction referene for simplcash-----------------------//
	void updateTransactionReferenceForSimplcash(SimplcashWebhookUpdateRequestPojo request);
	//-----------------------------------------------------------------------------------------//
	//--------------------for testing purpose--------------------------------------------------//
	Boolean findExistingParams(String param1, String param2);
	//-----------------------------------------------------------------------------------------//

	//--------------validate simplcash debit retailer wallet operation-------------------------//
	GeneralApiResponse validateSimplcashDebitOperation(SimplcashWebhookUpdateRequestPojo validateRequest);
	//-----------------------------------------------------------------------------------------//
	
	

	
}
