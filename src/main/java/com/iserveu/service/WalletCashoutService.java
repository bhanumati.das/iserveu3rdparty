package com.iserveu.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.iserveu.rbl.query.model.response.RBLQueryResponsePojo;
import com.iserveu.request.pojo.WalletCashoutrequest;
import com.iserveu.request.pojo.WalletCashoutrequestRefresh;

public interface WalletCashoutService {


	Map<String, Object> getWalletCashoutservicce(WalletCashoutrequest request, String authorization);

	Map<String, Object> getWalletCashoutservicceforrefresh(WalletCashoutrequestRefresh request);

	ResponseEntity<RBLQueryResponsePojo> rblTransactionQuery(String txId);

}
