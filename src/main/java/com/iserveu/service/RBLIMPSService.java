package com.iserveu.service;



import org.springframework.http.ResponseEntity;

import com.iserveu.rbl.query.model.request.RBLQueryRequestPojo;
import com.iserveu.rbl.query.model.response.RBLQueryResponsePojo;
import com.iserveu.request.pojo.RBLIMPSRequestPojo;
import com.iserveu.response.pojo.RBLIMPSResponsePojo;

public interface RBLIMPSService {


	ResponseEntity<RBLIMPSResponsePojo> calltorblimpstransaction(RBLIMPSRequestPojo impsrequest);

	ResponseEntity<RBLQueryResponsePojo> consumeRBLTransactionQuery(RBLQueryRequestPojo request);

}
