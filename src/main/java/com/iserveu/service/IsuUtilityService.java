package com.iserveu.service;

import java.util.Map;

import com.iserveu.entity.UserApiMappingEntity;
import com.iserveu.request.pojo.BankNameToBankDetailsRequestPojo;
import com.iserveu.request.pojo.IINToBankRequestPojo;
import com.iserveu.request.pojo.IdToBankRequestPojo;
import com.iserveu.request.pojo.UserDataRequest;
import com.iserveu.request.pojo.UserPropertiesRequest;
import com.iserveu.response.pojo.BankNameToBankDetailsResponsePojo;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.response.pojo.IINToBankResponsePojo;

public interface IsuUtilityService {

	//---------------service to get bank details for the given IIN Number---------------//
	IINToBankResponsePojo getBankDetailsForGivenIIN(IINToBankRequestPojo request);
	//----------------------------------------------------------------------------------//

	IINToBankResponsePojo getBankDetailsForGivenID(IdToBankRequestPojo request);

	Map<Boolean, Object> getBankDetailsForGivenBankName(BankNameToBankDetailsRequestPojo request);

	void saveuserdatatocasendra(UserDataRequest request);

	void saveuserdatatoproperties(UserPropertiesRequest request);

	GeneralResponse featureCheck(UserApiMappingEntity userApi, Integer featureId);

}
