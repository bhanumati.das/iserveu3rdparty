package com.iserveu.service;

import java.util.Map;

import com.iserveu.entity.WalletCashoutTransaction;
import com.iserveu.request.pojo.WalletOpreationRequest;

public interface CoreService {

	Map<String, Object> makewalletopreation(WalletOpreationRequest walletrequest);

	void savewalletcashouttodb(WalletCashoutTransaction transaction);

}
