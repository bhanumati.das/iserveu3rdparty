package com.iserveu.service;

import org.springframework.http.ResponseEntity;

import com.iserveu.request.pojo.RblRequestPojo;
import com.iserveu.response.pojo.PayeenamevalidationRblResponse;

/**
 * Service interface for RBL API layer.
 * 
 * @author Bhanumati
 *
 */
public interface PayeeVerificationRblApiService {

	ResponseEntity<PayeenamevalidationRblResponse> consumeForPayeeNameValidation(RblRequestPojo request)
			throws Exception;

}
