package com.iserveu.service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.iserveu.request.pojo.InitiateTxnRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;

public interface DebitGatewayService {


	CompletableFuture<GeneralResponse> startAsycnTxnGateway(InitiateTxnRequestPojo request,String authorization);


}
