package com.iserveu.service;

import com.iserveu.request.pojo.RiskcovryEncryptRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;

public interface RiskcovryService {
	//-------------------for getting encripted data for riscovery---------------------------//
		GeneralResponse getEncryptedDataForRiskcovry(RiskcovryEncryptRequestPojo request);
		//--------------------------------------------------------------------------------------//

}
