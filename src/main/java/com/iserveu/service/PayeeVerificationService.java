package com.iserveu.service;

import org.springframework.http.ResponseEntity;

import com.iserveu.request.pojo.PayeeVerificationRequest;
import com.iserveu.response.pojo.AbstractResponse;

/**
 * Service layer for payee verification.
 * 
 * @author Bhanumati
 *
 */
public interface PayeeVerificationService {

	//ResponseEntity<? extends AbstractResponse> verifyPayeeByFino(PayeeVerificationRequest payeeRequest);

	ResponseEntity<? extends AbstractResponse> verifyPayeeByRbl(PayeeVerificationRequest payeeRequest);

//	ResponseEntity<? extends AbstractResponse> verifyPayeeByIbl(PayeeVerificationRequest payeeRequest);

}
