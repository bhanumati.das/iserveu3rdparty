package com.iserveu.service;

import java.util.Map;

/**
 * 
 * @author Thierry
 *
 */
public interface BigQueryService {

	Integer findExistingTransactionInMATM(String txnId);

	String saveMatmTransactionParamsToBq(Map<String, Object> row);

	void restCallsavetocashoutdb(Map<String, Object> walletcashout);

}
