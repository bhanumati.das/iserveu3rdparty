package com.iserveu.controller;

 

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.request.pojo.WalletCashoutrequest;
import com.iserveu.request.pojo.WalletCashoutrequestRefresh;
import com.iserveu.service.WalletCashoutService;

 

@RestController
public class WalletCashoutController {

 

    
    private static final Logger logger = LogManager.getLogger(WalletCashoutController.class);
    
    
    
    @Autowired
    WalletCashoutService cashoutservice;
    
    
    
    @CrossOrigin
    @RequestMapping(path="wallet2/cashout",method=RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> dowallet2cashout(@RequestBody WalletCashoutrequest request,@RequestHeader("Authorization") String Authorization){
        
        logger.info("Wallet 2 Cashout request amount :"+request.getAmount());
        
        Map<String,Object> responseMap = new HashMap<String, Object>();
        
        try {
         responseMap=cashoutservice.getWalletCashoutservicce(request,Authorization);
         logger.info("wallet cashout response="+responseMap);
        }
        catch(Exception e) {
            
            logger.info("Exception in wallet2 Cashout request:"+request.getAmount());

 

            logger.info("Exception in wallet2 Cashout"+e.getMessage());
            responseMap.put("status", -1);
            responseMap.put("statusDesc", "Exception Occured");
            
        }
        
        return new ResponseEntity<Map<String,Object>>(responseMap,HttpStatus.OK);
    }   
    
    
  //Api for Wallet2 Cashout refresh
    @CrossOrigin
    @RequestMapping(path="wallet2/cashout/refresh",method=RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> dowallet2cashoutrefreshs(@RequestBody WalletCashoutrequestRefresh request){
       
        logger.info("Wallet 2 Cashout refresh txnid:"+request.getTxnId());
       
        Map<String,Object> responseMap = new HashMap<String, Object>();
       
        try {
         responseMap=cashoutservice.getWalletCashoutservicceforrefresh(request);
        }
        catch(Exception e) {
           
            logger.info("Exception in wallet2 Cashout request:"+request.getTxnId());
            logger.info("Exception in wallet2 Cashout"+e.getMessage());
            responseMap.put("status", -1);
            responseMap.put("statusDesc", "Exception Occured");
           
        }
       
        return new ResponseEntity<Map<String,Object>>(responseMap,HttpStatus.OK);
    }
}