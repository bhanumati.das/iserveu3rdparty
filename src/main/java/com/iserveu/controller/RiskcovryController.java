package com.iserveu.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.request.pojo.RiskcovryEncryptRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.service.RiskcovryService;

@RestController
public class RiskcovryController {
	
	@Autowired
	private RiskcovryService riskcovryService;
	
	Logger logger=LogManager.getLogger(RiskcovryController.class);
	
	@CrossOrigin
	@RequestMapping(value="/riscovry/enc_data",method=RequestMethod.POST)
	public ResponseEntity<GeneralResponse> getEncryptedDataForRiskcovry(@RequestBody RiskcovryEncryptRequestPojo request){
		GeneralResponse response=this.riskcovryService.getEncryptedDataForRiskcovry(request);
		if(response.getStatus().equals("0")){
			return new ResponseEntity<GeneralResponse>(response,HttpStatus.OK);
		}
		return new ResponseEntity<GeneralResponse>(response,HttpStatus.BAD_REQUEST);
	}

}
