package com.iserveu.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.request.pojo.SimplcashRedirectRequestPojo;
import com.iserveu.request.pojo.SimplcashWebhookInitiateRequestPojo;
import com.iserveu.request.pojo.SimplcashWebhookUpdateRequestPojo;
import com.iserveu.response.pojo.GeneralApiResponse;
import com.iserveu.service.SimplcashService;
import com.iserveu.util.IsuUtility;

@RestController
public class SimplcashController {
	
	@Autowired
	private SimplcashService simplcashService;
	
	private static final Logger logger = LogManager.getLogger(SimplcashController.class);
	
	@CrossOrigin
	@RequestMapping(path="/",method=RequestMethod.GET)
	public String getSimplcashRedirect(){
		logger.error("Third Party app is active");
		return "home";
	}
	
	@CrossOrigin
	@RequestMapping(path="get/simplcash/redirect",method=RequestMethod.POST)
	public ResponseEntity<Map<String,Object>> getSimplcashRedirect(@RequestBody SimplcashRedirectRequestPojo requestForRedirection){
		Map<String,Object> responseMap=simplcashService.getSimplcashRedirectionUrl(requestForRedirection);
		logger.info(">>getSimplcashRedirect");
		if(responseMap.get("status").equals("0")) {
			logger.info("<<getSimplcashRedirect: success response");
	        return new ResponseEntity<Map<String,Object>>(responseMap,HttpStatus.OK);
		}
		logger.info("<<getSimplcashRedirect: success response");
		return new ResponseEntity<Map<String,Object>>(responseMap,HttpStatus.BAD_REQUEST);
	}
	
	@CrossOrigin
	@RequestMapping(path="initiate/webhook",method=RequestMethod.POST)
	public void setWebhook(@RequestBody SimplcashWebhookInitiateRequestPojo request){
		logger.info("initiate/webhook : requestBody="+request);
		this.simplcashService.savewebhookResponse(request);
	}
	
	@CrossOrigin
	@RequestMapping(path="update/webhook",method=RequestMethod.POST)
	public void updateTransactionReferenceForSimplcash(@RequestBody SimplcashWebhookUpdateRequestPojo updateRequest){
	
	this.simplcashService.updateTransactionReferenceForSimplcash(updateRequest);	
	}
	
	@CrossOrigin
	@RequestMapping(path="validate_debit/webhook",method=RequestMethod.POST)
	public ResponseEntity<GeneralApiResponse> validateSimplcashTransaction(@RequestBody SimplcashWebhookUpdateRequestPojo validateRequest){
		logger.info("validate_debit operation");
		GeneralApiResponse responseMap=this.simplcashService.validateSimplcashDebitOperation(validateRequest);
		return new ResponseEntity<GeneralApiResponse>(responseMap,HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(path="get/result/{param1}/{param2}",method=RequestMethod.GET)
	public ResponseEntity<Map<String,Object>> findResult(@PathVariable String param1,@PathVariable String param2){
		Map<String,Object> responseMap= new HashMap<String,Object>();
		if(param1!=null && param2!=null && param1.length()!=0 && param2.length()!=0) {
		Boolean isPresent=this.simplcashService.findExistingParams(param1, param2);
		if(isPresent==Boolean.TRUE) {
			responseMap.put(IsuUtility.STATUS,"0");
			responseMap.put(IsuUtility.STATUS_DESC,"valid user");
			return new ResponseEntity<Map<String,Object>>(responseMap,HttpStatus.OK);
		}
		responseMap.put(IsuUtility.STATUS,"-1");
		responseMap.put(IsuUtility.STATUS_DESC,"invalid user");
		return new ResponseEntity<Map<String,Object>>( responseMap,HttpStatus.BAD_REQUEST); 
	}
		responseMap.put(IsuUtility.STATUS,"-1");
		responseMap.put(IsuUtility.STATUS_DESC,"invalid user details");
		return new ResponseEntity<Map<String,Object>>( responseMap,HttpStatus.BAD_REQUEST); 
	}
	
}
