package com.iserveu.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.request.pojo.MatmParamRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.service.BigQueryService;
import com.iserveu.service.impl.BigQueryServiceImpl;

/**
 * 
 * @author Raj Kishore
 *
 */
@RestController
public class BigQueryController {
	
	@Autowired
	private BigQueryService bigQueryService;
	
	private final Logger logger =LogManager.getLogger(BigQueryServiceImpl.class);

	@CrossOrigin
	@PostMapping("/saveParams")
	public ResponseEntity<Object> saveParamsForTransactions(@Valid @RequestBody MatmParamRequestPojo request){
		logger.info("------------------insert matm txn params into big query--------------------------------");
		logger.info("\trequest="+request);
		Map<String,Object> row= new HashMap<String,Object>();
		Integer count=this.bigQueryService.findExistingTransactionInMATM(request.getTxnId());
		if(count==null || count>0) {
			logger.info("\tfound existing record for the given transaction id, So data not inserted into BQ!!!");
			logger.info("---------------------------------------------------------------------------------------");
			if(count>0) {
				return new ResponseEntity<Object>(new GeneralResponse("-1","transaction already existing",null),HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<Object>(new GeneralResponse("-1","data not inserted",null),HttpStatus.BAD_REQUEST);
		}
		row.put("id",request.getTxnId());
		row.put("param_a",request.getParamA());
		row.put("param_b",request.getParamB());
		row.put("param_c",request.getParamC());
		String response=this.bigQueryService.saveMatmTransactionParamsToBq(row);
		if(response==null) {
			logger.info("\tdata not inserted into bigquery!!!!");
			logger.info("---------------------------------------------------------------------------------------");
			return new ResponseEntity<Object>(new GeneralResponse("-1","data not inserted in bigquery",null),HttpStatus.BAD_REQUEST);
		}
		logger.info("---------------------------------------------------------------------------------------");
		return new ResponseEntity<Object>(new GeneralResponse("0",response,null), HttpStatus.OK);
	}
	
}
