package com.iserveu.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.request.pojo.PayeeVerificationRequest;
import com.iserveu.response.pojo.PayeeVerificationResponse;
import com.iserveu.service.PayeeVerificationService;

@RestController
public class PayeeVerificationController {

	static final Logger LOGGER = LogManager.getLogger(PayeeVerificationController.class);

	@Autowired
	PayeeVerificationService payeeService;

	/**
	 * API to verify a payee
	 * 
	 * @param request - Holds PayeeVerificationRequest object
	 * @return
	 */
	
	@CrossOrigin
	@RequestMapping(value = "/api/verifyPayee", method = RequestMethod.POST)
	public ResponseEntity<PayeeVerificationResponse> payeeVerification(@Valid @RequestBody PayeeVerificationRequest request) {
		//LOGGER.info("Gate Way No : " + request.getGateWayNo());
		LOGGER.info("\t***************************************verifying bene from bank started*****************************************************");
		PayeeVerificationResponse response = new PayeeVerificationResponse();
		/*if (request.getGateWayNo() == 1) {  // FINO BANK
			return payeeService.verifyPayeeByFino(request);
		} else if (request.getGateWayNo() == 2) { // RBL
			return payeeService.verifyPayeeByRbl(request);
		} else if (request.getGateWayNo() == 3) { // Indusind
			return payeeService.verifyPayeeByIbl(request);
		} else {
			response.setStatus("-1");
			response.setStatusDesc("Wrong gateway selected");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}*/
		
		ResponseEntity<PayeeVerificationResponse> payeeResponse = (ResponseEntity<PayeeVerificationResponse>) payeeService.verifyPayeeByRbl(request);
//		if(payeeResponse.getBody().getStatus().equals("-1") || payeeResponse.getBody().getStatus().equals("ER001")
//				|| payeeResponse.getBody().getStatus().equals("ER002")
//				|| payeeResponse.getBody().getStatus().equals("ER003")
//				|| payeeResponse.getBody().getStatus().equals("ER004")
//				|| payeeResponse.getBody().getStatus().equals("ER006")
//				|| payeeResponse.getBody().getStatus().equals("ER013")) {
//			ResponseEntity<PayeeVerificationResponse> finResp=(ResponseEntity<PayeeVerificationResponse>) payeeService.verifyPayeeByFino(request);
//			LOGGER.info("\t***********************************end of bene verification from bank***************************************************");
//			return finResp;
//		}else {
//			LOGGER.info("\t***********************************end of bene verification from bank***************************************************");
//			return new ResponseEntity<>(payeeResponse.getBody(), HttpStatus.OK);
//		}	
		return payeeResponse;
	}
}
