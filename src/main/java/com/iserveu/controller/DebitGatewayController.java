package com.iserveu.controller;

import java.util.concurrent.CompletableFuture;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.request.pojo.InitiateTxnRequestPojo;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.service.DebitGatewayService;

@RestController
public class DebitGatewayController {
	
	@Autowired
	private DebitGatewayService debitGatewayService;
	
	@PostMapping("gateway/debit_wallet")
	@Async
	public CompletableFuture<GeneralResponse> debitRetailerWallet(@Valid @RequestBody InitiateTxnRequestPojo request,@RequestHeader("Authorization") String authorization) {
		System.out.println("in Thread Name="+Thread.currentThread().getName());
		CompletableFuture<GeneralResponse>response = this.debitGatewayService.startAsycnTxnGateway(request,authorization);
		System.out.println(" out Thread Name="+Thread.currentThread().getName());
		return response;
	}

}
