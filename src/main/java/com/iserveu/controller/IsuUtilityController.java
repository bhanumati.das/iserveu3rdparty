package com.iserveu.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.entity.UserApiMappingEntity;
import com.iserveu.entity.UserFeatureEntity;
import com.iserveu.entity.UserFeatureEntityKey;
import com.iserveu.rbl.query.model.request.UpdateUserFeaturePojo;
import com.iserveu.repository.FeatureRepository;
import com.iserveu.repository.UserApiMappingRepository;
import com.iserveu.repository.UserFeatureRepository;
import com.iserveu.request.pojo.BankNameToBankDetailsRequestPojo;
import com.iserveu.request.pojo.IINToBankRequestPojo;
import com.iserveu.request.pojo.IdToBankRequestPojo;
import com.iserveu.request.pojo.UserDataRequest;
import com.iserveu.request.pojo.UserPropertiesRequest;
import com.iserveu.response.pojo.GeneralResponse;
import com.iserveu.response.pojo.IINToBankResponsePojo;
import com.iserveu.service.IsuUtilityService;
import com.iserveu.service.impl.BigQueryServiceImpl;

@RestController
public class IsuUtilityController {
	
	@Autowired
	private IsuUtilityService isuUtilityService;
	
	private final Logger logger =LogManager.getLogger(BigQueryServiceImpl.class);
	
	@Autowired
	private UserApiMappingRepository userApiRepo;	
	
	@Autowired
	private UserFeatureRepository userFeatureRepo;
	
	@Autowired
	private FeatureRepository featureRepo;

	/**
	 * api to get bank details for the given iin number
	 * @param request
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value="/api/banknameByIin", method=RequestMethod.POST)
	public ResponseEntity<GeneralResponse> getBankDetailsForGivenIin(@RequestBody IINToBankRequestPojo request){
		IINToBankResponsePojo iinToBankResponse=this.isuUtilityService.getBankDetailsForGivenIIN(request);
		if(iinToBankResponse!=null){
			return new ResponseEntity<GeneralResponse>(new GeneralResponse("0","valid IIN",iinToBankResponse),HttpStatus.OK);
		}
		return new ResponseEntity<GeneralResponse>(new GeneralResponse("-1","invalid IIN",iinToBankResponse),HttpStatus.BAD_REQUEST);
	}
	/**
	 * To get bank details for the 
	 * @param request
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value="/api/banknameById", method=RequestMethod.POST)
	public ResponseEntity<GeneralResponse> getBankDetailsForGivenId(@RequestBody IdToBankRequestPojo request){
		IINToBankResponsePojo iinToBankResponse=this.isuUtilityService.getBankDetailsForGivenID(request);
		if(iinToBankResponse!=null){
			return new ResponseEntity<GeneralResponse>(new GeneralResponse("0","valid Id",iinToBankResponse),HttpStatus.OK);
		}
		return new ResponseEntity<GeneralResponse>(new GeneralResponse("-1","invalid Id",iinToBankResponse),HttpStatus.BAD_REQUEST);
	}
	/**
	 * To get bank details for the given bank name from dmt app
	 * @param request contains bank name
	 * @return List of bank details
	 */
	
	@CrossOrigin
	@RequestMapping(value="/api/bankDetailsByBankName", method=RequestMethod.POST)
	public ResponseEntity<GeneralResponse> getBankDetailsForGivenBankName(@RequestBody BankNameToBankDetailsRequestPojo request){
		Map<Boolean,Object> responseMap=this.isuUtilityService.getBankDetailsForGivenBankName(request);
		if(responseMap.containsKey(Boolean.TRUE)){
			return new ResponseEntity<GeneralResponse>(new GeneralResponse("0","valid bank name",responseMap.get(Boolean.TRUE)),HttpStatus.OK);
		}
		return new ResponseEntity<GeneralResponse>(new GeneralResponse("-1","invalid bank Name",responseMap.get(Boolean.FALSE)),HttpStatus.BAD_REQUEST);
	}
	
	@CrossOrigin
    @RequestMapping(path ="casendra/savetouserapi",method=RequestMethod.POST)
    public void saveuserdata(@RequestBody UserDataRequest request) {
        try {
        isuUtilityService.saveuserdatatocasendra(request);
       
        }
        catch(Exception e){
            System.out.println("Save in casendra  user api table"+e.getMessage());
           
        }
       
    }
	
	@CrossOrigin
    @RequestMapping(path ="casendra/savetouserproperties",method=RequestMethod.POST)
    public void saveuserdatatoproperties(@RequestBody UserPropertiesRequest request) {
        try {
        isuUtilityService.saveuserdatatoproperties(request);
       
        }
        catch(Exception e){
            System.out.println("Save in casendra  user api table"+e.getMessage());
           
        }
       
    }
		
	@CrossOrigin
	@GetMapping("/featureCheck/{userName}/{featureId}")
	public ResponseEntity<GeneralResponse> checkFeatureByUserName(@PathVariable String userName,@PathVariable Integer featureId){ 
		if(userName==null) {
			logger.info("\tinvalid user name");
			logger.info("\t------------------------------------------------------");
			return new ResponseEntity<GeneralResponse>(new GeneralResponse("-1","invalid user name",null),HttpStatus.OK);
		}
		Optional<UserApiMappingEntity> userApiOptional =this.userApiRepo.findById(userName);
		if(userApiOptional.isPresent()) {
		return new ResponseEntity<GeneralResponse>( this.isuUtilityService.featureCheck(userApiOptional.get(),featureId),HttpStatus.OK);
		}
		else {
			logger.info("\tunable to identify user");
			logger.info("\t------------------------------------------------------");
			return new ResponseEntity<GeneralResponse>(new GeneralResponse("-1","unable to identify user",null),HttpStatus.OK);
		}
	}
	
	@CrossOrigin
	@PostMapping("/updateAllFeature")
	public ResponseEntity<GeneralResponse> updateUserFeature(@RequestBody @Valid UpdateUserFeaturePojo featureUpdate){
		try {
			Optional<UserApiMappingEntity> userApiOpt= this.userApiRepo.findById(featureUpdate.getUserName());
			if(!userApiOpt.isPresent()) {
				return new ResponseEntity(new GeneralResponse("-1","unable to identify user",null),HttpStatus.OK);
			}
			List<Integer> featureIds=this.featureRepo.findIdByFeatureIdIn(featureUpdate.getFeatureIds());
			if(featureIds.size()==0) {
				return new ResponseEntity(new GeneralResponse("-1","no features found with the given ids="+featureIds,null),HttpStatus.OK);
			}
			List<UserFeatureEntity> updatableFeatures=new ArrayList<UserFeatureEntity>();	
			for(Integer featureId:featureIds) {
				UserFeatureEntity userFeature=new UserFeatureEntity();
				userFeature.setIsActive(featureUpdate.getActiveDeactive());
				UserFeatureEntityKey featureKey= new UserFeatureEntityKey();
				featureKey.setFeatureId(featureId);
				featureKey.setUserId(userApiOpt.get().getUserId());
				userFeature.setUserFeatureKey(featureKey);
				updatableFeatures.add(userFeature);
			}
			this.userFeatureRepo.saveAll(updatableFeatures);
			return new ResponseEntity<GeneralResponse>(new GeneralResponse("0","user feature updated successfully",null),HttpStatus.OK);
			}catch(Exception e) {
				System.out.println("error in updating user feature");
				return new ResponseEntity<GeneralResponse>(new GeneralResponse("-1","error in updating feature",null),HttpStatus.OK);
			}
	}
	
}
