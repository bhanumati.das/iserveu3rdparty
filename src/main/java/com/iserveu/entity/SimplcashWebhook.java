package com.iserveu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * 
 * @author Entity to hold webhook_response
 *
 */
@Entity
@Table(name="simplcash_webhook")
public class SimplcashWebhook {
	
	@Id
	private String id;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeStamp;
	
	private String clientId;
	private String retailerId;
	private String type;
	private Double amount;
	private String status;
	private String partnerId;
	private String originUrl;
	
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOriginUrl() {
		return originUrl;
	}
	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "SimplcashWebhook [id=" + id + ", timeStamp=" + timeStamp + ", clientId=" + clientId + ", retailerId="
				+ retailerId + ", type=" + type + ", amount=" + amount + ", status=" + status + ", partnerId="
				+ partnerId + ", originUrl=" + originUrl + "]";
	}
}
