package com.iserveu.entity;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("user_properties")
public class UserPropertiesEntity {
	@PrimaryKey("user_id")
	private Long userId;
	
	@Column("master")
	private String master;

	@Column("master_wallet_id")
	private Long masterWalletId;

	@Column("distributer")
	private String distributor;

	@Column("distributor_wallet_id")
	private Long distributerWalletId;
	
	@Column("admin")
	private String admin;
	
	@Column("admin_wallet_id")
	private  Long adminWalletId;

	@Column("user_role")
	private String userRole;

	private String city;

	private String state;
	
	@Column("provider_base_package_id")
	private Long providerBasePackageId;
	
	@Column("lat_long")
	private String latLong;
	
	@Column("is_bioauth")
	private Boolean isBioauth;
	
	@Column("pincode")
	private Integer pincode;
	
	@Column("aadhaar")
	private String aadhaar;
	
	
	public String getAadhaar() {
		return aadhaar;
	}

	public void setAadhaar(String aadhaar) {
		this.aadhaar = aadhaar;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getLatLong() {
		return latLong;
	}

	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}

	public Boolean getIsBioauth() {
		return isBioauth;
	}

	public void setIsBioauth(Boolean isBioauth) {
		this.isBioauth = isBioauth;
	}

	public Long getProviderBasePackageId() {
		return providerBasePackageId;
	}

	public void setProviderBasePackageId(Long providerBasePackageId) {
		this.providerBasePackageId = providerBasePackageId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public Long getMasterWalletId() {
		return masterWalletId;
	}

	public void setMasterWalletId(Long masterWalletId) {
		this.masterWalletId = masterWalletId;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public Long getDistributerWalletId() {
		return distributerWalletId;
	}

	public void setDistributerWalletId(Long distributerWalletId) {
		this.distributerWalletId = distributerWalletId;
	}
	
	

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public Long getAdminWalletId() {
		return adminWalletId;
	}

	public void setAdminWalletId(Long adminWalletId) {
		this.adminWalletId = adminWalletId;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "UserPropertiesEntity [userId=" + userId + ", master=" + master + ", masterWalletId=" + masterWalletId
				+ ", distributor=" + distributor + ", distributerWalletId=" + distributerWalletId + ", admin=" + admin
				+ ", adminWalletId=" + adminWalletId + ", userRole=" + userRole + ", city=" + city + ", state=" + state
				+ ", providerBasePackageId=" + providerBasePackageId + ", latLong=" + latLong + ", isBioauth="
				+ isBioauth + "]";
	}
}
