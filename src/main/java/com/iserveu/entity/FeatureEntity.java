package com.iserveu.entity;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("feature")
public class FeatureEntity {
    
    @PrimaryKey("feature_id")
    private Integer featureId;
    
    @Column("feature_name")
    private String featureName;
    @Column("is_active")
    private Boolean isActive;
    @Column("feature_description")
    private String featureDescription;
    @Column("created_date")
    private Date createdDate;
    @Column("created_by")
    private String createdBy;
    @Column("updated_date")
    private Date updatedDate;
    @Column("updated_by")
    private String updatedBy;
    
    

 


    public Integer getFeatureId() {
        return featureId;
    }

 

    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

 

    public Boolean getIsActive() {
        return isActive;
    }

 

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

 

    public String getFeatureName() {
        return featureName;
    }

 

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

 

    public boolean isActive() {
        return isActive;
    }

 

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

 

    public String getFeatureDescription() {
        return featureDescription;
    }

 

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

 

    public Date getCreatedDate() {
        return createdDate;
    }

 

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

 

    public String getCreatedBy() {
        return createdBy;
    }

 

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

 

    public Date getUpdatedDate() {
        return updatedDate;
    }

 

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

 

    public String getUpdatedBy() {
        return updatedBy;
    }

 

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

 

}