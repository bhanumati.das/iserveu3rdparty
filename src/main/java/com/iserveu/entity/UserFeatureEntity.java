package com.iserveu.entity;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("user_feature")
public class UserFeatureEntity {

 

    @PrimaryKey
    private UserFeatureEntityKey userFeatureKey;
    
    @Column("is_active")
    private Boolean isActive;

 

    public UserFeatureEntityKey getUserFeatureKey() {
        return userFeatureKey;
    }

 

    public void setUserFeatureKey(UserFeatureEntityKey userFeatureKey) {
        this.userFeatureKey = userFeatureKey;
    }

 

    public Boolean getIsActive() {
        return isActive;
    }

 

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

 

    
    
    
}