package com.iserveu.entity;


import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("user_api_mapping")
public class UserApiMappingEntity {
	@PrimaryKey("user_name")
	private String userName;
	@Column("api_user_id")
	private Long apiUserId;
	@Column("wallet_id")
	private Long walletId;
	@Column("api_wallet_id")
	private Long apiWalletId;
	private String city;
	private String state;
	@Column("wallet2_id")
	private Long wallet2Id;
	@Column("api_wallet2_id")
	private Long apiWallet2Id;
	
	@Column("user_id")
	private Long userId;
    @Column("pincode")
    private String pincode;

	
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public Long getWallet2Id() {
		return wallet2Id;
	}
	public void setWallet2Id(Long wallet2Id) {
		this.wallet2Id = wallet2Id;
	}
	public Long getApiWallet2Id() {
		return apiWallet2Id;
	}
	public void setApiWallet2Id(Long apiWallet2Id) {
		this.apiWallet2Id = apiWallet2Id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getApiUserId() {
		return apiUserId;
	}
	public void setApiUserId(Long apiUserId) {
		this.apiUserId = apiUserId;
	}
	public Long getWalletId() {
		return walletId;
	}
	public void setWalletId(Long walletId) {
		this.walletId = walletId;
	}
	public Long getApiWalletId() {
		return apiWalletId;
	}
	public void setApiWalletId(Long apiWalletId) {
		this.apiWalletId = apiWalletId;
	}
	

}
