package com.iserveu.entity;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class UserFeatureEntityKey {
    
    @PrimaryKeyColumn(
              name = "user_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private Long userId;
    
    @PrimaryKeyColumn(
              name = "feature_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private Integer featureId;

 

    public Long getUserId() {
        return userId;
    }

 

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Integer getFeatureId() {
        return featureId;
    }

 

    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

 

    @Override
    public String toString() {
        return "UserFeatureEntityKey [userId=" + userId + ", featureId=" + featureId + "]";
    }

 

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((featureId == null) ? 0 : featureId.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

 

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserFeatureEntityKey other = (UserFeatureEntityKey) obj;
        if (featureId == null) {
            if (other.featureId != null)
                return false;
        } else if (!featureId.equals(other.featureId))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }
    
    
    
}
 