package com.iserveu.entity;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("service_provider_details")
public class ServiceProviderDetailsEntity {

    @PrimaryKey
    private long id;
    
    private Double min;
    
    private Double max;
    
    @Column("service_provider_name")
    private String serviceProviderName;
    
    @Column("service_provider_type")
    private String serviceProviderType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public String getServiceProviderName() {
		return serviceProviderName;
	}

	public void setServiceProviderName(String serviceProviderName) {
		this.serviceProviderName = serviceProviderName;
	}

	public String getServiceProviderType() {
		return serviceProviderType;
	}

	public void setServiceProviderType(String serviceProviderType) {
		this.serviceProviderType = serviceProviderType;
	}

	@Override
	public String toString() {
		return "ServiceProviderDetailsEntity [id=" + id + ", min=" + min + ", max=" + max + ", serviceProviderName="
				+ serviceProviderName + ", serviceProviderType=" + serviceProviderType + "]";
	} 
}
