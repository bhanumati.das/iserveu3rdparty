package com.iserveu.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class WalletCashoutTransaction {
	
	
	@PrimaryKey("id")
	private Long id;
	@Column("api_user_id")
	private Long apiUserId;
	@Column("api_wallet_id")
	private Long apiWalletId;
    @Column("user_id")
	private Long userId;
	@Column("wallet_id")
	private Long walletId;
	@Column("amount")
	private Double amount;
	@Column("operation_performed")
	private String operationPerformed;
	@Column("service_provider_id")
	private Long serviceProviderId;
	@Column("api_service_provider_id")
	private Long apiServiceProviderId;
	@Column("bank_ref_id")
	private String bankrefId;
	@Column("credit_debit")
	private Boolean creditDebit;
	@Column("created_date")
	private Date createdDate;
	@Column("updated_date")
	private Date updatedDate;
	@Column("status")
	private String status;
	@Column("status_desc")
	private String statusDesc;
	@Column("account_number")
	private String accountNumber;
	@Column("api_comment")
	private String apiComment;
	
	
	
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getApiUserId() {
		return apiUserId;
	}
	public void setApiUserId(Long apiUserId) {
		this.apiUserId = apiUserId;
	}
	public Long getApiWalletId() {
		return apiWalletId;
	}
	public void setApiWalletId(Long apiWalletId) {
		this.apiWalletId = apiWalletId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getWalletId() {
		return walletId;
	}
	public void setWalletId(Long walletId) {
		this.walletId = walletId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getOperationPerformed() {
		return operationPerformed;
	}
	public void setOperationPerformed(String operationPerformed) {
		this.operationPerformed = operationPerformed;
	}
	public Long getServiceProviderId() {
		return serviceProviderId;
	}
	public void setServiceProviderId(Long serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	public Long getApiServiceProviderId() {
		return apiServiceProviderId;
	}
	public void setApiServiceProviderId(Long apiServiceProviderId) {
		this.apiServiceProviderId = apiServiceProviderId;
	}
	public String getBankrefId() {
		return bankrefId;
	}
	public void setBankrefId(String bankrefId) {
		this.bankrefId = bankrefId;
	}
	public Boolean getCreditDebit() {
		return creditDebit;
	}
	public void setCreditDebit(Boolean creditDebit) {
		this.creditDebit = creditDebit;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String string) {
		this.accountNumber = string;
	}
	public String getApiComment() {
		return apiComment;
	}
	public void setApiComment(String apiComment) {
		this.apiComment = apiComment;
	}
	
	
	
     public Map<String, Object> toHashMap(){
		
		Map<String, Object> walletmap = new HashMap<>();
		walletmap.put("id", this.id);
		walletmap.put("api_user_id", this.apiUserId);
		walletmap.put("api_wallet_id", this.apiWalletId);
		walletmap.put("user_id", this.userId);
		walletmap.put("wallet_id", this.walletId);
		walletmap.put("amount", this.amount);
		walletmap.put("operation_performed", this.operationPerformed);
		walletmap.put("service_provider_id", this.serviceProviderId);
		walletmap.put("api_service_provider_id", this.apiServiceProviderId);
		walletmap.put("bank_ref_id", this.bankrefId);
		walletmap.put("credit_debit", this.creditDebit);
		walletmap.put("created_date", ISTToUTC(this.createdDate==null?new Date():this.createdDate));
		walletmap.put("updated_date", ISTToUTC(this.updatedDate==null?new Date():this.updatedDate));
		walletmap.put("status", this.status);
		walletmap.put("status_desc", this.statusDesc);
		walletmap.put("account_number", this.accountNumber);
		walletmap.put("api_comment", this.apiComment);
		
		
		return walletmap;


}
	
     
 	public String ISTToUTC(Date date) {
 		//need to import the corresponding libraries from java.time
 		java.time.LocalDateTime dbTime = date.toInstant()
 			      .atZone(java.time.ZoneId.of("Asia/Kolkata"))
 			      .toLocalDateTime();
 		dbTime = dbTime.minusHours(5).minusMinutes(30);

 		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter
 	            .ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
 		
 		return dbTime.format(formatter);
 	}
 		

}
