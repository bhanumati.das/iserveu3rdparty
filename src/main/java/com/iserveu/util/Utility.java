package com.iserveu.util;

import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;

public class Utility {

	public static final String UNIQUE_ID="uniqueid";
	public static final String statusSuccess = "SUCCESS";
	public static final String statusPending = "PENDING";
	public static final String statusFailure = "FAILED";

	public static final String statusRefund = "REFUNDED";
	
	public static String getUserName(String jwtToken) {
		try {
		Gson gson= new Gson();
		String[] split_string = jwtToken.split("\\.");
        String base64EncodedHeader = split_string[0];
        String base64EncodedBody = split_string[1];
        String base64EncodedSignature = split_string[2];
        Base64 base64Url = new Base64(true);
        System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
        String body = new String(base64Url.decode(base64EncodedBody));
        Map<String,String> map= gson.fromJson(body,Map.class);
        return map.get("sub");
		}catch(Exception e) {
			return null;
		}
	}

}
