package com.iserveu.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * utility class to hold constants and some utility methods
 * 
 * @author Raj Kishore
 *
 */
public class IsuUtility {
		//----------------------------response attributes--------------------------------//;
		public static final String STATUS="status";
		public static final String STATUS_DESC="statusDesc";
		public static final String REDIRECT_LINK="redirectLink";
		public static final String TRANSACTION_ID="transactionId";
		//-------------------------------------------------------------------------------//;
		//------------------------------------------------riscovery constants------------//
		public static String RISKCOVRY_CONST_CODE="<CODE>";
		public static String ENCRYPTED_CONST_RISKCOVRY="<Encryped_User_Info>";
		//------------------------------------------//	
		//------------------------date time formatters-----------------------------------//
		private static SimpleDateFormat dbTimeStamp= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		//-------------------------------------------------------------------------------//
		
		
		public static Date parseToDbTimeStamp(String timeStamp) throws ParseException {
			return dbTimeStamp.parse(timeStamp);
		}
		

		/**
		 * This Method is to generate a random number of any length.
		 * 
		 * @param length - Length of random number
		 * @return String
		 */
		public static String getNumbericRandom(int length) {
			String chars = "123456789";

			int numberOfCodes = 0;// controls the length of alpha numberic string
			String code = "";
			while (numberOfCodes < length) {
				char c = chars.charAt((int) (Math.random() * chars.length()));
				code += Character.toString(c);
				numberOfCodes++;
			}
			return code;
		}
		
	    public static String encryptAES256(String textToEncrypt,String secretkey,byte[] IV) throws Exception
	    {
	    	byte[] decodedKey = secretkey.trim().getBytes();

	    	  /* Constructs a secret key from the given byte array */
	    	  SecretKey key = new SecretKeySpec(decodedKey, 0,
	    	    decodedKey.length, "AES");
	    	byte[] plaintext=textToEncrypt.trim().getBytes();
	        //Get Cipher Instance
	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        
	        //Create SecretKeySpec
	        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
	        
	        //Create IvParameterSpec
	        IvParameterSpec ivSpec = new IvParameterSpec(IV);
	        
	        //Initialize Cipher for ENCRYPT_MODE
	        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
	        
	        //Perform Encryption
	        byte[] cipherText = cipher.doFinal(plaintext);

	        return Base64.getEncoder().encodeToString(cipherText);
	    }
	    
	    public static String decryptAES256(String encyptedString, String secretkey,byte[] IV) throws Exception
	    {
	    	byte[] decodedKey =secretkey.trim().getBytes();

	  	  /* Constructs a secret key from the given byte array */
	  	  SecretKey key = new SecretKeySpec(decodedKey, 0,
	  	    decodedKey.length, "AES");
	    	byte[] cipherTextTemp=encyptedString.trim().getBytes();
	    	byte[] cipherText=Base64.getDecoder().decode(cipherTextTemp);
	        //Get Cipher Instance
	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        
	        //Create SecretKeySpec
	        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
	        
	        //Create IvParameterSpec
	        IvParameterSpec ivSpec = new IvParameterSpec(IV);
	        
	        //Initialize Cipher for DECRYPT_MODE
	        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
	        
	        //Perform Decryption
	        byte[] decryptedText = cipher.doFinal(cipherText);
	        
	        return new String(decryptedText);
	    }
}
