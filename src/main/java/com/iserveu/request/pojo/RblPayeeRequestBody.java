package com.iserveu.request.pojo;

/**
 * POJO class for RBL request body for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class RblPayeeRequestBody {

	private String beneficiaryName;
	private String beneficiaryMobileNumber;
	private String accountNumber;
	private String ifscCode;

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public String getBeneficiaryMobileNumber() {
		return beneficiaryMobileNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public void setBeneficiaryMobileNumber(String beneficiaryMobileNumber) {
		this.beneficiaryMobileNumber = beneficiaryMobileNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

}
