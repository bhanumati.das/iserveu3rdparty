package com.iserveu.request.pojo;


public class WalletOpreationRequest {
	
	private Long id;
	private Long userid;
	private Long apiuserid;
	private Long walletid;
	private Long apiwalletid;
	private Double amount;
	private String type;
	private Long serviceProviderId;
	private Long apiServiceProviderId;
	private String accountnumber;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public Long getApiuserid() {
		return apiuserid;
	}
	public void setApiuserid(Long apiuserid) {
		this.apiuserid = apiuserid;
	}
	public Long getWalletid() {
		return walletid;
	}
	public void setWalletid(Long walletid) {
		this.walletid = walletid;
	}
	public Long getApiwalletid() {
		return apiwalletid;
	}
	public void setApiwalletid(Long apiwalletid) {
		this.apiwalletid = apiwalletid;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getServiceProviderId() {
		return serviceProviderId;
	}
	public void setServiceProviderId(Long serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	public Long getApiServiceProviderId() {
		return apiServiceProviderId;
	}
	public void setApiServiceProviderId(Long apiServiceProviderId) {
		this.apiServiceProviderId = apiServiceProviderId;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	
	
	
	
	

}
