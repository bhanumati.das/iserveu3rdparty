package com.iserveu.request.pojo;
/**
 * pojo to hold request for getting simplcash redirection link 
 * 
 * @author Raj Kishore
 *
 */
public class SimplcashRedirectRequestPojo {
	private String retailer_id;
    private String owner_name;
    private String pincode;
    private String mobile_no;
    private String current_url;
    private String shopLat;
    private String shopLong;
    private String shopName;
    private String addressLine1;
    private String addressLine2;
   // private Long webhookId;
    private String shopLandmark;
    private String isu_code;
    private String transaction_id;//
    private String date_time;//

//	public Long getWebhookId() {
//		return webhookId;
//	}
//	public void setWebhookId(Long webhookId) {
//		this.webhookId = webhookId;
//	}
    
	public String getOwner_name() {
		return owner_name;
	}
	public String getRetailer_id() {
		return retailer_id;
	}
	public void setRetailer_id(String retailer_id) {
		this.retailer_id = retailer_id;
	}
	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getCurrent_url() {
		return current_url;
	}
	public void setCurrent_url(String current_url) {
		this.current_url = current_url;
	}
	public String getShopLat() {
		return shopLat;
	}
	public void setShopLat(String shopLat) {
		this.shopLat = shopLat;
	}
	public String getShopLong() {
		return shopLong;
	}
	public void setShopLong(String shopLong) {
		this.shopLong = shopLong;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getShopLandmark() {
		return shopLandmark;
	}
	public void setShopLandmark(String shopLandmark) {
		this.shopLandmark = shopLandmark;
	}
	public String getIsu_code() {
		return isu_code;
	}
	public void setIsu_code(String isu_code) {
		this.isu_code = isu_code;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	@Override
	public String toString() {
		return "SimplcashRedirectRequestPojo [retailer_id=" + retailer_id + ", owner_name=" + owner_name + ", pincode="
				+ pincode + ", mobile_no=" + mobile_no + ", current_url=" + current_url + ", shopLat=" + shopLat
				+ ", shopLong=" + shopLong + ", shopName=" + shopName + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", shopLandmark=" + shopLandmark + ", isu_code=" + isu_code
				+ ", transaction_id=" + transaction_id + ", date_time=" + date_time + "]";
	}

}
