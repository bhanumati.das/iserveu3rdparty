package com.iserveu.request.pojo;

public class UserPropertiesRequest {
	
	
	private Long userid;
	private String city;
	private String state;
	private int pincode;
	private String role;
	private String admin;
	private Long adminwalletid;
	private String master;
	private Long masterwalletid;
	private String distributor;
	private Long distributorwalletid;
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getAdmin() {
		return admin;
	}
	public void setAdmin(String admin) {
		this.admin = admin;
	}
	public Long getAdminwalletid() {
		return adminwalletid;
	}
	public void setAdminwalletid(Long adminwalletid) {
		this.adminwalletid = adminwalletid;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public Long getMasterwalletid() {
		return masterwalletid;
	}
	public void setMasterwalletid(Long masterwalletid) {
		this.masterwalletid = masterwalletid;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	public Long getDistributorwalletid() {
		return distributorwalletid;
	}
	public void setDistributorwalletid(Long distributorwalletid) {
		this.distributorwalletid = distributorwalletid;
	}
	
	
	

}
