package com.iserveu.request.pojo;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * 
 * @author Raj Kishore
 *
 */
public class RiskcovryEncryptRequestPojo {
	@NotEmpty(message="partner_user_id cannot be empty")
	private String partner_user_id;
	@NotEmpty(message="mobile_no cannot be empty")
	private String mobile_no;
	@NotEmpty(message="email cannnot be empty or null")
	private String email_id;
	@NotEmpty(message="name cannot be empty")
	private String name;
	private String manager_id;
	private String  role; 
	private String  department;
	private String  dob;
	private String gender;
	private String  aadhar_no;

	public String getPartner_user_id() {
		return partner_user_id;
	}
	public void setPartner_user_id(String partner_user_id) {
		this.partner_user_id = partner_user_id;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getManager_id() {
		return manager_id;
	}
	public void setManager_id(String manager_id) {
		this.manager_id = manager_id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAadhar_no() {
		return aadhar_no;
	}
	public void setAadhar_no(String aadhar_no) {
		this.aadhar_no = aadhar_no;
	}
	@Override
	public String toString() {
		return "RiskcovryEncryptRequestPojo [partner_user_id=" + partner_user_id + ", mobile_no=" + mobile_no
				+ ", email_id=" + email_id + ", name=" + name + ", manager_id=" + manager_id + ", role=" + role
				+ ", department=" + department + ", dob=" + dob + ", gender=" + gender + ", aadhar_no=" + aadhar_no
				+ "]";
	}

}
