package com.iserveu.request.pojo;

import javax.validation.constraints.NotBlank;

public class WalletCashoutrequest {
	
	@NotBlank
	private String amount;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
	

}
