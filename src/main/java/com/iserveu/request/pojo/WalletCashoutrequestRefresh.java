package com.iserveu.request.pojo;

 

import javax.validation.constraints.NotBlank;

 

public class WalletCashoutrequestRefresh {
    
    
    @NotBlank(message="Txn id not blank")
    private Long txnId;

 

    public Long getTxnId() {
        return txnId;
    }

 

    public void setTxnId(Long txnId) {
        this.txnId = txnId;
    }

 

}