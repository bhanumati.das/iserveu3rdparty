package com.iserveu.request.pojo;

public class IdToBankRequestPojo {
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "IdToBankRequestPojo [id=" + id + "]";
	}
}
