package com.iserveu.request.pojo;

/**
 * POJO class for RBL account validation request for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class BeneficiaryAccValidationReq {

	private RblRequestHeader Header;
	private RblPayeeRequestBody Body;
	private RblRequestSignature Signature;

	public RblRequestHeader getHeader() {
		return Header;
	}

	public void setHeader(RblRequestHeader header) {
		Header = header;
	}

	public RblPayeeRequestBody getBody() {
		return Body;
	}

	public void setBody(RblPayeeRequestBody body) {
		Body = body;
	}

	public RblRequestSignature getSignature() {
		return Signature;
	}

	public void setSignature(RblRequestSignature signature) {
		Signature = signature;
	}

}
