package com.iserveu.request.pojo;
/**
 * @author Raj Kishore
 * 
 */
import org.hibernate.validator.constraints.NotEmpty;
public class SimplcashWebhookInitiateRequestPojo {

	@NotEmpty(message="timeStamp cannot be empty")
	private String timeStamp;
	
	@NotEmpty(message="transactionId cannot be empty")
	private Long transactionId;
	
	@NotEmpty(message="origin url cannot be empty")
	private String origin_url;
	
	@NotEmpty(message="clientId cannot be empty")
	private String clientId;
	
	@NotEmpty(message="type cannot be empty or null")
	private String type;
	
	@NotEmpty(message="amount cannot be empty")
	private Double amount;
	
	@NotEmpty(message="status cannot be empty")
	private String status;
	
	@NotEmpty(message="partnerId cannot be empty")
	private String retailerId;
	
	private String partnerId;
	
	
	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}


	public String getOrigin_url() {
		return origin_url;
	}

	public void setOrigin_url(String origin_url) {
		this.origin_url = origin_url;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SimplcashWebhookInitiateRequestPojo [timeStamp=" + timeStamp + ", transactionId=" + transactionId
				+ ", origin_url=" + origin_url + ", clientId=" + clientId + ", type=" + type + ", amount=" + amount
				+ ", status=" + status + ", retailerId=" + retailerId + ", partnerId=" + partnerId + "]";
	}

}
