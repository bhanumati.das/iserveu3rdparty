package com.iserveu.request.pojo;

public class UserDataRequest {

	
	private String username;
	
	private Long apiuserid;
	
	private Long apiwallet2id;
	
	private Long apiwalletid;
	
	private String city;
	
	private String state;
	
	private Long userid;
	
	private Long walletid;
	
	private Long wallet2id;
	
	private String pincode;
	
	

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getApiuserid() {
		return apiuserid;
	}

	public void setApiuserid(Long apiuserid) {
		this.apiuserid = apiuserid;
	}

	public Long getApiwallet2id() {
		return apiwallet2id;
	}

	public void setApiwallet2id(Long apiwallet2id) {
		this.apiwallet2id = apiwallet2id;
	}

	public Long getApiwalletid() {
		return apiwalletid;
	}

	public void setApiwalletid(Long apiwalletid) {
		this.apiwalletid = apiwalletid;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getWalletid() {
		return walletid;
	}

	public void setWalletid(Long walletid) {
		this.walletid = walletid;
	}

	public Long getWallet2id() {
		return wallet2id;
	}

	public void setWallet2id(Long wallet2id) {
		this.wallet2id = wallet2id;
	}
	
	
	
	
	
}
