package com.iserveu.request.pojo;
/**
 * model to request bank name for the given IIN Number
 * @author Raj Kishore
 *
 */
public class IINToBankRequestPojo {
	private String iinNumber;


	public String getIinNumber() {
		return iinNumber;
	}


	public void setIinNumber(String iinNumber) {
		this.iinNumber = iinNumber;
	}


	@Override
	public String toString() {
		return "IINToBankRequestPojo [iinNumber=" + iinNumber + "]";
	}


	
}
