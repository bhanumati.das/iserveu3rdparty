package com.iserveu.request.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "Single_Payment_Corp_Req")
public class RBLIMPSRequestPojo {
	 
		@JsonProperty("Single_Payment_Corp_Req")
		private Single_Payment_Corp_Req single_Payment_Corp_Req;

		@JsonProperty("Single_Payment_Corp_Req")
		public Single_Payment_Corp_Req getSingle_Payment_Corp_Req() {
			return single_Payment_Corp_Req;
		}

		@JsonProperty("Single_Payment_Corp_Req")
		public void setSingle_Payment_Corp_Req(Single_Payment_Corp_Req single_Payment_Corp_Req) {
			this.single_Payment_Corp_Req = single_Payment_Corp_Req;
		}

		   
		}
