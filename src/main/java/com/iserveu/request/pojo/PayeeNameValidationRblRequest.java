package com.iserveu.request.pojo;

/**
 * 
 * @author Bhanumati
 *
 */
public class PayeeNameValidationRblRequest {

	private BeneficiaryAccValidationReq beneficiaryAccValidationReq;

	public BeneficiaryAccValidationReq getBeneficiaryAccValidationReq() {
		return beneficiaryAccValidationReq;
	}

	public void setBeneficiaryAccValidationReq(BeneficiaryAccValidationReq beneficiaryAccValidationReq) {
		this.beneficiaryAccValidationReq = beneficiaryAccValidationReq;
	}
}
