package com.iserveu.request.pojo;

/**
 * POJO class for RBL request signature for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class RblRequestSignature {

	private String Signature;

	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

}
