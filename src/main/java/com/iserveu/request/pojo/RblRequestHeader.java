package com.iserveu.request.pojo;

/**
 * POJO class for RBL request header for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class RblRequestHeader {

	private String TranID;
	private String Corp_ID;
	private String Maker_ID;
	private String Checker_ID;
	private String Approver_ID;

	public String getTranID() {
		return TranID;
	}

	public String getCorp_ID() {
		return Corp_ID;
	}

	public String getMaker_ID() {
		return Maker_ID;
	}

	public String getChecker_ID() {
		return Checker_ID;
	}

	public String getApprover_ID() {
		return Approver_ID;
	}

	public void setTranID(String tranID) {
		TranID = tranID;
	}

	public void setCorp_ID(String corp_ID) {
		Corp_ID = corp_ID;
	}

	public void setMaker_ID(String maker_ID) {
		Maker_ID = maker_ID;
	}

	public void setChecker_ID(String checker_ID) {
		Checker_ID = checker_ID;
	}

	public void setApprover_ID(String approver_ID) {
		Approver_ID = approver_ID;
	}
}
