package com.iserveu.request.pojo;

public class IblRequestPojo {
	
	private String clientUniqueId;
	private String customerMobileNo;
	private String beneIfscCode;
	private String beneAccountNo;
	private String beneName;
	private String amount;
	private String customerName;
	private String transactionMode;
	
	public String getClientUniqueId() {
		return clientUniqueId;
	}
	public void setClientUniqueId(String clientUniqueId) {
		this.clientUniqueId = clientUniqueId;
	}
	public String getCustomerMobileNo() {
		return customerMobileNo;
	}
	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}
	public String getBeneIfscCode() {
		return beneIfscCode;
	}
	public void setBeneIfscCode(String beneIfscCode) {
		this.beneIfscCode = beneIfscCode;
	}
	public String getBeneAccountNo() {
		return beneAccountNo;
	}
	public void setBeneAccountNo(String beneAccountNo) {
		this.beneAccountNo = beneAccountNo;
	}
	public String getBeneName() {
		return beneName;
	}
	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getTransactionMode() {
		return transactionMode;
	}
	public void setTransactionMode(String transactionMode) {
		this.transactionMode = transactionMode;
	}
	
	

}
