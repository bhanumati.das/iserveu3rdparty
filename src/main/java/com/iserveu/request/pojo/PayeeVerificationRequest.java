package com.iserveu.request.pojo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Request POJO for payee verification.
 * 
 * @author Bhanumati
 *
 */
/**
 * Request POJO for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class PayeeVerificationRequest {

	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "please send valid charracter in clientUniueId")
	@Size(max = 50, message = "paramA cannot be greator tha 50 charracters")
	@NotBlank(message = "paramA cannot be blank")
	private String paramA;

	@Pattern(regexp = "(0/91)?[2-9][0-9]{9}$", message = "invalid mobile number")
	@Size(min = 10, max = 10, message = "please enter 10 digit mobile number")
	@NotBlank(message = "cutomerMobileNo cannot be bank")
	private String customerMobileNo;

	@NotBlank(message = "beneIfscCode cannot be blank")
	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "invalid ifsc")
	@Size(max = 25, message = "beneIFSC length cannot be creator than 25")
	private String beneIFSCCode;

	@NotBlank(message = "beneAccountNo cannot be blank")
	@Pattern(regexp = "^[0-9]*$", message = "invalid beneBankAccount")
	@Size(max = 30, min = 1, message = "length of account number should be between 1 to 30")
	private String beneAccountNo;

	@Pattern(regexp = "[a-zA-Z][a-zA-Z ]+[a-zA-Z]$", message = "special charracters not allowed in name")
	@NotBlank(message = "beneName cannot be blank")
	@Size(min = 3, max = 84, message = "name cannot be greator than 84 character and smaller that 3 charracters")
	private String beneName;

	private String amount;

	@Pattern(regexp = "[a-zA-Z][a-zA-Z ]+[a-zA-Z]$", message = "special charracters not allowed in name")
	@Size(min = 3, max = 84, message = "name shoud be between 3 to 84 charracters")
	@NotBlank(message = "cutomer name cannot be blank")
	private String customerName;

	private int gateWayNo;

	@Pattern(regexp = "[a-zA-Z][a-zA-Z ]+[a-zA-Z]$", message = "invalid bank name")
	@Size(max = 50, message = "bank name cannot be greator than 50")
	private String beneBankName;

	//@NotBlank(message = "need to pass value in paramB")
	private String paramB;

	//@NotBlank(message = "need to pass value in paramC")
	private String paramC;

	public String getParamA() {
		return paramA;
	}

	public void setParamA(String paramA) {
		this.paramA = paramA;
	}

	public String getParamB() {
		return paramB;
	}

	public void setParamB(String paramB) {
		this.paramB = paramB;
	}

	public String getParamC() {
		return paramC;
	}

	public void setParamC(String paramC) {
		this.paramC = paramC;
	}

	public String getBeneBankName() {
		return beneBankName;
	}

	public void setBeneBankName(String beneBankName) {
		this.beneBankName = beneBankName;
	}

	public String getCustomerMobileNo() {
		return customerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}

	public String getBeneIFSCCode() {
		return beneIFSCCode;
	}

	public void setBeneIFSCCode(String beneIFSCCode) {
		this.beneIFSCCode = beneIFSCCode;
	}

	public String getBeneAccountNo() {
		return beneAccountNo;
	}

	public void setBeneAccountNo(String beneAccountNo) {
		this.beneAccountNo = beneAccountNo;
	}

	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getGateWayNo() {
		return gateWayNo;
	}

	public void setGateWayNo(int gateWayNo) {
		this.gateWayNo = gateWayNo;
	}

}
