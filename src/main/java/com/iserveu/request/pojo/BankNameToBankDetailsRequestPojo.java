package com.iserveu.request.pojo;

public class BankNameToBankDetailsRequestPojo {
	private String bankName;

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Override
	public String toString() {
		return "BankNameToBankDetailsPojo [bankName=" + bankName + "]";
	}
	
}
