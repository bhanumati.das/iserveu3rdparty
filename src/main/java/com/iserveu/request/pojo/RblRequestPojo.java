package com.iserveu.request.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * POJO class for RBL request for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class RblRequestPojo {

	@NotNull(message = "BeneficiaryName should not be Null")
	@NotEmpty(message = "BeneficiaryName should not be Empty")
	@Size(max = 50, message = "BeneficiaryName can't be bigger than 50")
	@Pattern(message = "Invalid BeneficiaryName", regexp = "^[a-zA-Z]+$")
	private String beneficiaryName;

	@NotNull(message = "BeneficiaryMobileNumber should not be Null")
	@NotEmpty(message = "BeneficiaryMobileNumber should not be Empty")
	@Size(max = 10, message = "BeneficiaryMobileNumber can't be bigger than 10")
	@Pattern(message = "Invalid BeneficiaryMobileNumber", regexp = "^[0-9]+$")
	private String beneficiaryMobileNumber;

	@NotNull(message = "ACCOUNT NUMBER should not be Null")
	@NotEmpty(message = "ACCOUNT NUMBER should not be Empty")
	@Size(max = 20, message = "ACCOUNT NUMBER can't be bigger than 20")
	@Pattern(message = "Invalid AccountNumber", regexp = "^[a-zA-Z0-9]+$")
	private String accountNumber;

	@NotNull(message = "IFSC should not be Null")
	@NotEmpty(message = "IFSC should not be Empty")
	@Size(max = 15, message = "IFSC Code can't be bigger than 15")
	@Pattern(message = "Invalid IfscCode", regexp = "^[a-zA-Z0-9]+$")
	private String ifscCode;

	private String tranID;

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public String getBeneficiaryMobileNumber() {
		return beneficiaryMobileNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public void setBeneficiaryMobileNumber(String beneficiaryMobileNumber) {
		this.beneficiaryMobileNumber = beneficiaryMobileNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getTranID() {
		return tranID;
	}

	public void setTranID(String tranID) {
		this.tranID = tranID;
	}

}
