package com.iserveu.request.pojo;

/**
 * POJO class for transaction status.
 * 
 * @author Bhanumati
 *
 */
public class TxnStatusWrapper {

	private String ResponseCode;
	private String MessageString;
	private String DisplayMessage;
	private String RequestID;
	private String ClientUniqueID;
	private String ResponseData;

	public String getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}

	public String getMessageString() {
		return MessageString;
	}

	public void setMessageString(String messageString) {
		MessageString = messageString;
	}

	public String getDisplayMessage() {
		return DisplayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		DisplayMessage = displayMessage;
	}

	public String getRequestID() {
		return RequestID;
	}

	public void setRequestID(String requestID) {
		RequestID = requestID;
	}

	public String getClientUniqueID() {
		return ClientUniqueID;
	}

	public void setClientUniqueID(String clientUniqueID) {
		ClientUniqueID = clientUniqueID;
	}

	public String getResponseData() {
		return ResponseData;
	}

	public void setResponseData(String responseData) {
		ResponseData = responseData;
	}

}
