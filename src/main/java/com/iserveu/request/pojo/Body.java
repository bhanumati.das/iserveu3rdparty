package com.iserveu.request.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Body {
	
	@JsonProperty("Debit_Acct_No")
    private String debit_Acct_No;

	@JsonProperty("Amount")
    private String amount;

	@JsonProperty("Ben_Acct_No")
    private String ben_Acct_No;

	@JsonProperty("Mode_of_Pay")
    private String mode_of_Pay;

	@JsonProperty("Ben_BankName")
    private String ben_BankName;

	@JsonProperty("Debit_Mobile")
    private String debit_Mobile;

	@JsonProperty("Ben_Mobile")
    private String ben_Mobile;

	@JsonProperty("Ben_IFSC")
    private String ben_IFSC;
	
	@JsonProperty("Debit_IFSC")
    private String debit_IFSC;

	@JsonProperty("Remarks")
    private String remarks;

	@JsonProperty("Ben_Name")
    private String ben_Name;

	@JsonProperty("Debit_Acct_Name")
    private String debit_Acct_Name;

	@JsonProperty("Ben_Email")
    private String ben_Email;

	@JsonProperty("Debit_Acct_No")
	public String getDebit_Acct_No() {
		return debit_Acct_No;
	}

	@JsonProperty("Debit_Acct_No")
	public void setDebit_Acct_No(String debit_Acct_No) {
		this.debit_Acct_No = debit_Acct_No;
	}

	@JsonProperty("Amount")
	public String getAmount() {
		return amount;
	}

	@JsonProperty("Amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}

	@JsonProperty("Ben_Acct_No")
	public String getBen_Acct_No() {
		return ben_Acct_No;
	}

	@JsonProperty("Ben_Acct_No")
	public void setBen_Acct_No(String ben_Acct_No) {
		this.ben_Acct_No = ben_Acct_No;
	}

	@JsonProperty("Mode_of_Pay")
	public String getMode_of_Pay() {
		return mode_of_Pay;
	}

	@JsonProperty("Mode_of_Pay")
	public void setMode_of_Pay(String mode_of_Pay) {
		this.mode_of_Pay = mode_of_Pay;
	}

	
	@JsonProperty("Ben_BankName")
	public String getBen_BankName() {
		return ben_BankName;
	}

	@JsonProperty("Ben_BankName")
	public void setBen_BankName(String ben_BankName) {
		this.ben_BankName = ben_BankName;
	}

	@JsonProperty("Debit_Mobile")
	public String getDebit_Mobile() {
		return debit_Mobile;
	}

	@JsonProperty("Debit_Mobile")
	public void setDebit_Mobile(String debit_Mobile) {
		this.debit_Mobile = debit_Mobile;
	}

	
	@JsonProperty("Ben_Mobile")
	public String getBen_Mobile() {
		return ben_Mobile;
	}

	@JsonProperty("Ben_Mobile")
	public void setBen_Mobile(String ben_Mobile) {
		this.ben_Mobile = ben_Mobile;
	}

	@JsonProperty("Ben_IFSC")
	public String getBen_IFSC() {
		return ben_IFSC;
	}

	@JsonProperty("Ben_IFSC")
	public void setBen_IFSC(String ben_IFSC) {
		this.ben_IFSC = ben_IFSC;
	}

	@JsonProperty("Debit_IFSC")
	public String getDebit_IFSC() {
		return debit_IFSC;
	}

	@JsonProperty("Debit_IFSC")
	public void setDebit_IFSC(String debit_IFSC) {
		this.debit_IFSC = debit_IFSC;
	}

	@JsonProperty("Remarks")
	public String getRemarks() {
		return remarks;
	}

	@JsonProperty("Remarks")
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@JsonProperty("Ben_Name")
	public String getBen_Name() {
		return ben_Name;
	}

	@JsonProperty("Ben_Name")
	public void setBen_Name(String ben_Name) {
		this.ben_Name = ben_Name;
	}

	@JsonProperty("Debit_Acct_Name")
	public String getDebit_Acct_Name() {
		return debit_Acct_Name;
	}

	@JsonProperty("Debit_Acct_Name")
	public void setDebit_Acct_Name(String debit_Acct_Name) {
		this.debit_Acct_Name = debit_Acct_Name;
	}

	@JsonProperty("Ben_Email")
	public String getBen_Email() {
		return ben_Email;
	}

	@JsonProperty("Ben_Email")
	public void setBen_Email(String ben_Email) {
		this.ben_Email = ben_Email;
	}

	@Override
	public String toString() {
		return "Body [debit_Acct_No=" + debit_Acct_No + ", amount=" + amount + ", ben_Acct_No=" + ben_Acct_No
				+ ", mode_of_Pay=" + mode_of_Pay + ", ben_BankName=" + ben_BankName + ", debit_Mobile=" + debit_Mobile
				+ ", ben_Mobile=" + ben_Mobile + ", ben_IFSC=" + ben_IFSC + ", debit_IFSC=" + debit_IFSC + ", remarks="
				+ remarks + ", ben_Name=" + ben_Name + ", debit_Acct_Name=" + debit_Acct_Name + ", ben_Email="
				+ ben_Email + "]";
	}

   


}
