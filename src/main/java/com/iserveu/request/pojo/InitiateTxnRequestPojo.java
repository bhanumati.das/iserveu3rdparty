package com.iserveu.request.pojo;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class InitiateTxnRequestPojo {
	
	@Pattern(regexp="[a-zA-Z][a-zA-Z ]+[a-zA-Z]$",message="special charracters not allowed in name")
	@NotEmpty(message="borrowerName cannot be empty")
	@Size(min=4,max=80,message="borrowerName length should be between 4 to 80")
	private String borrowerName;
	
	@NotEmpty(message="borrower mobile number cannot be empty")
	@Size(max=10,min=10,message="mobile number should be of 10 digits")
	private String borrowerMobileNumber;
	
	@NotEmpty(message="jobId cannot be empty")
	private String jobId;
	
	@NotEmpty(message="amount cannot be empty")
	private String amount;
	
	@NotEmpty(message="operation cannot be empty")
	private String operation;
	
	private String sumAssured;
	
	

	public String getSumAssured() {
		return sumAssured;
	}

	public void setSumAssured(String sumAssured) {
		this.sumAssured = sumAssured;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getBorrowerMobileNumber() {
		return borrowerMobileNumber;
	}

	public void setBorrowerMobileNumber(String borrowerMobileNumber) {
		this.borrowerMobileNumber = borrowerMobileNumber;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Override
	public String toString() {
		return "InitiateTxnRequestPojo [borrowerName=" + borrowerName + ", borrowerMobileNumber=" + borrowerMobileNumber
				+ ", jobId=" + jobId + ", amount=" + amount + ", operation=" + operation + ", sumAssured=" + sumAssured
				+ "]";
	}



}
