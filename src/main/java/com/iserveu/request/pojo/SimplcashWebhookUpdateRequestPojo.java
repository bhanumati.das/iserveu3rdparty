package com.iserveu.request.pojo;
/**
 * 
 * @author Raj Kishore
 *
 */

public class SimplcashWebhookUpdateRequestPojo {
	private String transactionId;
	private String status;
	private String timeStamp;

	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	@Override
	public String toString() {
		return "SimplcashWebhookUpdateRequest [transactionId=" + transactionId + ", status=" + status + ", timeStamp="
				+ timeStamp + "]";
	}
}
