package com.iserveu.request.pojo;

import javax.validation.constraints.NotBlank;

public class MatmParamRequestPojo {
	
	@NotBlank(message="txnId cannot be blank")
	private String txnId;
	
	@NotBlank(message="paramA cannot be blank")
	private String paramA;
	
	@NotBlank(message="paramB cannot be blank")
	private String paramB;
	
	@NotBlank(message="paramC cannot be blank")
	private String paramC;
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getParamA() {
		return paramA;
	}
	public void setParamA(String paramA) {
		this.paramA = paramA;
	}
	public String getParamB() {
		return paramB;
	}
	public void setParamB(String paramB) {
		this.paramB = paramB;
	}
	public String getParamC() {
		return paramC;
	}
	public void setParamC(String paramC) {
		this.paramC = paramC;
	}
	@Override
	public String toString() {
		return "MatmParamRequestPojo [txnId=" + txnId + ", paramA=" + paramA + ", paramB=" + paramB + ", paramC="
				+ paramC + "]";
	}

}
