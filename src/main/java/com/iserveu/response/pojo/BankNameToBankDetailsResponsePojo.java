package com.iserveu.response.pojo;

public class BankNameToBankDetailsResponsePojo {
	private String branchName;
	private String ifscCode;
	private boolean status;
	private String bankName;
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	@Override
	public String toString() {
		return "BankNameToBankDetailsResponsePojo [branchName=" + branchName + ", ifscCode=" + ifscCode + ", status="
				+ status + ", bankName=" + bankName + "]";
	}
}
