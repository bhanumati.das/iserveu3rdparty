package com.iserveu.response.pojo;

/**
 * POJO class for Fino response for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class NeftResponseWrapper extends AbstractResponse {

	private String ResponseCode;
	private String MessageString;
	private String DisplayMessage;
	private String RequestID;
	private String ClientUniqueID;
	private String ResponseData;

	// added for IBL
	private String transactionType;
	private String statusDesc;
	private String iBLRefNo;// transaction id
	private int amount;
	private double IBLamount;
	private Long customerRefNo;// client unique id
	private String statusCode;
	private String beneName;
	private String responsedate;
	private String utr;

	public String getUtr() {
		return utr;
	}

	public void setUtr(String utr) {
		this.utr = utr;
	}

	public String getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}

	public String getMessageString() {
		return MessageString;
	}

	public void setMessageString(String messageString) {
		MessageString = messageString;
	}

	public String getDisplayMessage() {
		return DisplayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		DisplayMessage = displayMessage;
	}

	public String getRequestID() {
		return RequestID;
	}

	public void setRequestID(String requestID) {
		RequestID = requestID;
	}

	public String getClientUniqueID() {
		return ClientUniqueID;
	}

	public void setClientUniqueID(String clientUniqueID) {
		ClientUniqueID = clientUniqueID;
	}

	public String getResponseData() {
		return ResponseData;
	}

	public void setResponseData(String responseData) {
		ResponseData = responseData;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getiBLRefNo() {
		return iBLRefNo;
	}

	public void setiBLRefNo(String iBLRefNo) {
		this.iBLRefNo = iBLRefNo;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int i) {
		this.amount = i;
	}

	public double getIBLamount() {
		return IBLamount;
	}

	public void setIBLamount(double iBLamount) {
		IBLamount = iBLamount;
	}

	public Long getCustomerRefNo() {
		return customerRefNo;
	}

	public void setCustomerRefNo(Long customerRefNo) {
		this.customerRefNo = customerRefNo;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getResponsedate() {
		return responsedate;
	}

	public void setResponsedate(String responsedate) {
		this.responsedate = responsedate;
	}

	@Override
	public String toString() {
		return "NeftResponseWrapper [ResponseCode=" + ResponseCode + ", MessageString=" + MessageString
				+ ", DisplayMessage=" + DisplayMessage + ", RequestID=" + RequestID + ", ClientUniqueID="
				+ ClientUniqueID + ", ResponseData=" + ResponseData + ", transactionType=" + transactionType
				+ ", statusDesc=" + statusDesc + ", iBLRefNo=" + iBLRefNo + ", amount=" + amount + ", customerRefNo="
				+ customerRefNo + ", statusCode=" + statusCode + ", beneName=" + beneName + ", responsedate="
				+ responsedate + "]";
	}

}
