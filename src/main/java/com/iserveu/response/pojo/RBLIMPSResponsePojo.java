package com.iserveu.response.pojo;

public class RBLIMPSResponsePojo {
	
	 private Single_Payment_Corp_Resp  Single_Payment_Corp_Resp;

	    public Single_Payment_Corp_Resp  getSingle_Payment_Corp_Resp ()
	    {
	        return Single_Payment_Corp_Resp ;
	    }

	    public void setSingle_Payment_Corp_Resp (Single_Payment_Corp_Resp Single_Payment_Corp_Resp)
	    {
	        this.Single_Payment_Corp_Resp  = Single_Payment_Corp_Resp;
	    }

	    @Override
	    public String toString()
	    {
	        return "RBLIMPSResponsePojo [Single_Payment_Corp_Resp = "+Single_Payment_Corp_Resp +"]";
	    }

}
