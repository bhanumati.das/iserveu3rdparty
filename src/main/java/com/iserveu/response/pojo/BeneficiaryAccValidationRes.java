package com.iserveu.response.pojo;

/**
 * POJO class for RBL account validation response for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class BeneficiaryAccValidationRes {

	private RblPayeeResponseHeader Header;
	private RblPayeeResponseBody Body;
	private RblResponseSignature Signature;

	public RblPayeeResponseHeader getHeader() {
		return Header;
	}

	public void setHeader(RblPayeeResponseHeader header) {
		Header = header;
	}

	public RblPayeeResponseBody getBody() {
		return Body;
	}

	public void setBody(RblPayeeResponseBody body) {
		Body = body;
	}

	public RblResponseSignature getSignature() {
		return Signature;
	}

	public void setSignature(RblResponseSignature signature) {
		Signature = signature;
	}

}
