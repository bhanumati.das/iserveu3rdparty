package com.iserveu.response.pojo;

/**
 * POJO class for RBL response body for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class RblPayeeResponseBody {

	private String beneName;
	private String beneficiaryMobileNumber;
	private String accountNumber;
	private String ifscCode;
	private String status;
	private String remarks;
	private String npciResponsecode;
	private String Error_Cde;
	private String Error_Desc;
	private String bankRefno;

	public String getStatus() {
		return status;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getNpciResponsecode() {
		return npciResponsecode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setNpciResponsecode(String npciResponsecode) {
		this.npciResponsecode = npciResponsecode;
	}

	public String getBeneficiaryMobileNumber() {
		return beneficiaryMobileNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setBeneficiaryMobileNumber(String beneficiaryMobileNumber) {
		this.beneficiaryMobileNumber = beneficiaryMobileNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getError_Cde() {
		return Error_Cde;
	}

	public String getError_Desc() {
		return Error_Desc;
	}

	public void setError_Cde(String error_Cde) {
		Error_Cde = error_Cde;
	}

	public void setError_Desc(String error_Desc) {
		Error_Desc = error_Desc;
	}

	public String getBeneName() {
		return beneName;
	}

	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}

	public String getBankRefno() {
		return bankRefno;
	}

	public void setBankRefno(String bankRefno) {
		this.bankRefno = bankRefno;
	}

	
	

}
