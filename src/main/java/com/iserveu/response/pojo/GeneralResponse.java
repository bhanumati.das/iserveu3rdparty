package com.iserveu.response.pojo;

public class GeneralResponse {
	private String status;
	private String statusDesc;
	private Object response;

	public GeneralResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GeneralResponse(String status, String statusDesc, Object response) {
		super();
		this.status = status;
		this.statusDesc = statusDesc;
		this.response = response;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	@Override
	public String toString() {
		return "GeneralResponse [status=" + status + ", statusDesc=" + statusDesc + ", response=" + response + "]";
	}
	
}
