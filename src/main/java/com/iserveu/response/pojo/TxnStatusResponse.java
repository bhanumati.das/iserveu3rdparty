package com.iserveu.response.pojo;

/**
 * POJO class for transaction status response.
 * 
 * @author Bhanumati
 *
 */
public class TxnStatusResponse extends AbstractResponse {

	private String responseCode;
	private String messageString;
	private String displayMessage;
	private String requestID;
	private String clientUniqueID;
	private String responseData;
	private String message;
	private String status;
	private long statusCode;

	public long getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(long statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getMessageString() {
		return messageString;
	}

	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getClientUniqueID() {
		return clientUniqueID;
	}

	public void setClientUniqueID(String clientUniqueID) {
		this.clientUniqueID = clientUniqueID;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String ActCode;
	private String txnID;
	private String AmountRequested;
	private String ChargesDeducted;
	private String TotalAmount;
	private String BeneName;
	private String Rfu1;
	private String Rfu2;
	private String Rfu3;
	private String TransactionDatetime;
	private String TxnDescription;

	public String getActCode() {
		return ActCode;
	}

	public void setActCode(String actCode) {
		ActCode = actCode;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

	public String getAmountRequested() {
		return AmountRequested;
	}

	public void setAmountRequested(String amountRequested) {
		AmountRequested = amountRequested;
	}

	public String getChargesDeducted() {
		return ChargesDeducted;
	}

	public void setChargesDeducted(String chargesDeducted) {
		ChargesDeducted = chargesDeducted;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getBeneName() {
		return BeneName;
	}

	public void setBeneName(String beneName) {
		BeneName = beneName;
	}

	public String getRfu1() {
		return Rfu1;
	}

	public void setRfu1(String rfu1) {
		Rfu1 = rfu1;
	}

	public String getRfu2() {
		return Rfu2;
	}

	public void setRfu2(String rfu2) {
		Rfu2 = rfu2;
	}

	public String getRfu3() {
		return Rfu3;
	}

	public void setRfu3(String rfu3) {
		Rfu3 = rfu3;
	}

	public String getTransactionDatetime() {
		return TransactionDatetime;
	}

	public void setTransactionDatetime(String transactionDatetime) {
		TransactionDatetime = transactionDatetime;
	}

	public String getTxnDescription() {
		return TxnDescription;
	}

	public void setTxnDescription(String txnDescription) {
		TxnDescription = txnDescription;
	}

	@Override
	public String toString() {
		return "TxnStatusResponse [responseCode=" + responseCode + ", messageString=" + messageString
				+ ", displayMessage=" + displayMessage + ", requestID=" + requestID + ", clientUniqueID="
				+ clientUniqueID + ", responseData=" + responseData + ", message=" + message + ", status=" + status
				+ ", statusCode=" + statusCode + ", ActCode=" + ActCode + ", TxnID=" + txnID + ", AmountRequested="
				+ AmountRequested + ", ChargesDeducted=" + ChargesDeducted + ", TotalAmount=" + TotalAmount
				+ ", BeneName=" + BeneName + ", Rfu1=" + Rfu1 + ", Rfu2=" + Rfu2 + ", Rfu3=" + Rfu3
				+ ", TransactionDatetime=" + TransactionDatetime + ", TxnDescription=" + TxnDescription
				+ ", getStatusCode()=" + getStatusCode() + ", getStatus()=" + getStatus() + ", getResponseCode()="
				+ getResponseCode() + ", getMessageString()=" + getMessageString() + ", getDisplayMessage()="
				+ getDisplayMessage() + ", getRequestID()=" + getRequestID() + ", getClientUniqueID()="
				+ getClientUniqueID() + ", getResponseData()=" + getResponseData() + ", getMessage()=" + getMessage()
				+ ", getActCode()=" + getActCode() + ", getTxnID()=" + getTxnID() + ", getAmountRequested()="
				+ getAmountRequested() + ", getChargesDeducted()=" + getChargesDeducted() + ", getTotalAmount()="
				+ getTotalAmount() + ", getBeneName()=" + getBeneName() + ", getRfu1()=" + getRfu1() + ", getRfu2()="
				+ getRfu2() + ", getRfu3()=" + getRfu3() + ", getTransactionDatetime()=" + getTransactionDatetime()
				+ ", getTxnDescription()=" + getTxnDescription() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}
