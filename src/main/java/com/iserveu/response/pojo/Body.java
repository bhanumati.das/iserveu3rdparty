package com.iserveu.response.pojo;

public class Body {
	
	private String RRN;

	private String Amount;

	private String Ben_Acct_No;

	private String Txn_Time;

	private String RefNo;

	private String BenIFSC;

	private String channelpartnerrefno;

	public String getRRN ()
	{
	return RRN;
	}

	public void setRRN (String RRN)
	{
	this.RRN = RRN;
	}

	public String getAmount ()
	{
	return Amount;
	}

	public void setAmount (String Amount)
	{
	this.Amount = Amount;
	}

	public String getBen_Acct_No ()
	{
	return Ben_Acct_No;
	}

	public void setBen_Acct_No (String Ben_Acct_No)
	{
	this.Ben_Acct_No = Ben_Acct_No;
	}

	public String getTxn_Time ()
	{
	return Txn_Time;
	}

	public void setTxn_Time (String Txn_Time)
	{
	this.Txn_Time = Txn_Time;
	}

	public String getRefNo ()
	{
	return RefNo;
	}

	public void setRefNo (String RefNo)
	{
	this.RefNo = RefNo;
	}

	public String getBenIFSC ()
	{
	return BenIFSC;
	}

	public void setBenIFSC (String BenIFSC)
	{
	this.BenIFSC = BenIFSC;
	}

	public String getChannelpartnerrefno ()
	{
	return channelpartnerrefno;
	}

	public void setChannelpartnerrefno (String channelpartnerrefno)
	{
	this.channelpartnerrefno = channelpartnerrefno;
	}

	@Override
	public String toString()
	{
	return "Body [RRN = "+RRN+", Amount = "+Amount+", Ben_Acct_No = "+Ben_Acct_No+", Txn_Time = "+Txn_Time+", RefNo = "+RefNo+", BenIFSC = "+BenIFSC+", channelpartnerrefno = "+channelpartnerrefno+"]";
	}


}
