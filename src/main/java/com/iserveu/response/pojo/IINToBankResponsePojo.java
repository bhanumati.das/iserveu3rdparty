package com.iserveu.response.pojo;
/**
 * response model to hold the requested bank details for the given IIN number
 * @author Raj kishore
 *
 */
public class IINToBankResponsePojo {
	private String bankName;
	private String iinNumber;
	private Long id;

	

	public String getIinNumber() {
		return iinNumber;
	}

	public void setIinNumber(String iinNumber) {
		this.iinNumber = iinNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Override
	public String toString() {
		return "IINToBankResponsePojo [bankName=" + bankName + "]";
	}
	
}
