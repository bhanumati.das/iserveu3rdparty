package com.iserveu.response.pojo;

/**
 * 
 * @author Bhanumati
 *
 */
public class TrimResponseData {

	private String ActCode;
	private String TxnID;
	private String AmountRequested;
	private String ChargesDeducted;
	private String TotalAmount;
	private String BeneName;
	private String Rfu1;
	private String Rfu2;
	private String Rfu3;
	private String TransactionDatetime;
	private String TxnDescription;

	public String getActCode() {
		return ActCode;
	}

	public void setActCode(String actCode) {
		ActCode = actCode;
	}

	public String getTxnID() {
		return TxnID;
	}

	public void setTxnID(String txnID) {
		TxnID = txnID;
	}

	public String getAmountRequested() {
		return AmountRequested;
	}

	public void setAmountRequested(String amountRequested) {
		AmountRequested = amountRequested;
	}

	public String getChargesDeducted() {
		return ChargesDeducted;
	}

	public void setChargesDeducted(String chargesDeducted) {
		ChargesDeducted = chargesDeducted;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getBeneName() {
		return BeneName;
	}

	public void setBeneName(String beneName) {
		BeneName = beneName;
	}

	public String getRfu1() {
		return Rfu1;
	}

	public void setRfu1(String rfu1) {
		Rfu1 = rfu1;
	}

	public String getRfu2() {
		return Rfu2;
	}

	public void setRfu2(String rfu2) {
		Rfu2 = rfu2;
	}

	public String getRfu3() {
		return Rfu3;
	}

	public void setRfu3(String rfu3) {
		Rfu3 = rfu3;
	}

	public String getTransactionDatetime() {
		return TransactionDatetime;
	}

	public void setTransactionDatetime(String transactionDatetime) {
		TransactionDatetime = transactionDatetime;
	}

	public String getTxnDescription() {
		return TxnDescription;
	}

	public void setTxnDescription(String txnDescription) {
		TxnDescription = txnDescription;
	}

}
