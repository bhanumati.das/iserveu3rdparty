package com.iserveu.response.pojo;

public class Signature {
	
	private String Signature;

    public String getSignature ()
    {
        return Signature;
    }

    public void setSignature (String Signature)
    {
        this.Signature = Signature;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Signature = "+Signature+"]";
    }
}
