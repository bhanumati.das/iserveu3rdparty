package com.iserveu.response.pojo;

/**
 * POJO class for RBL response signature for payee verification.
 * 
 * @author Bhanumati
 *
 */
public class RblResponseSignature {
	
	private String Signature;

	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

}
