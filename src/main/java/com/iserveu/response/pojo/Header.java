package com.iserveu.response.pojo;

public class Header {


	    private String Status;

	    private String TranID;

	    private String Checker_ID;

	    private String Error_Desc;

	    private String Maker_ID;

	    private String Corp_ID;

	    private String Resp_cde;

	    private String Approver_ID;

	    public String getStatus ()
	    {
	        return Status;
	    }

	    public void setStatus (String Status)
	    {
	        this.Status = Status;
	    }

	    public String getTranID ()
	    {
	        return TranID;
	    }

	    public void setTranID (String TranID)
	    {
	        this.TranID = TranID;
	    }

	    public String getChecker_ID ()
	    {
	        return Checker_ID;
	    }

	    public void setChecker_ID (String Checker_ID)
	    {
	        this.Checker_ID = Checker_ID;
	    }

	    public String getError_Desc ()
	    {
	        return Error_Desc;
	    }

	    public void setError_Desc (String Error_Desc)
	    {
	        this.Error_Desc = Error_Desc;
	    }

	    public String getMaker_ID ()
	    {
	        return Maker_ID;
	    }

	    public void setMaker_ID (String Maker_ID)
	    {
	        this.Maker_ID = Maker_ID;
	    }

	    public String getCorp_ID ()
	    {
	        return Corp_ID;
	    }

	    public void setCorp_ID (String Corp_ID)
	    {
	        this.Corp_ID = Corp_ID;
	    }

	    public String getResp_cde ()
	    {
	        return Resp_cde;
	    }

	    public void setResp_cde (String Resp_cde)
	    {
	        this.Resp_cde = Resp_cde;
	    }

	    public String getApprover_ID ()
	    {
	        return Approver_ID;
	    }

	    public void setApprover_ID (String Approver_ID)
	    {
	        this.Approver_ID = Approver_ID;
	    }

	    @Override
	    public String toString()
	    {
	        return "Header [Status = "+Status+", TranID = "+TranID+", Checker_ID = "+Checker_ID+", Error_Desc = "+Error_Desc+", Maker_ID = "+Maker_ID+", Corp_ID = "+Corp_ID+", Resp_cde = "+Resp_cde+", Approver_ID = "+Approver_ID+"]";
	    }
}
