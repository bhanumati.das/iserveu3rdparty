package com.iserveu.response.pojo;

import java.util.Map;
/**
 * 
 * @author Raj kishore
 *
 */
public class GeneralApiResponse {
	private String status;
	private String statusDesc;
	private Map<String,Object> response;
	
	public GeneralApiResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GeneralApiResponse(String status, String statusDesc, Map<String, Object> response) {
		super();
		this.status = status;
		this.statusDesc = statusDesc;
		this.response = response;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public Map<String, Object> getResponse() {
		return response;
	}
	public void setResponse(Map<String, Object> response) {
		this.response = response;
	}
	@Override
	public String toString() {
		return "GeneralResponse [status=" + status + ", statusDesc=" + statusDesc + ", response=" + response + "]";
	}
}
