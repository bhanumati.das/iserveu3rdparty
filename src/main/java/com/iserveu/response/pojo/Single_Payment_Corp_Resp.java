package com.iserveu.response.pojo;

public class Single_Payment_Corp_Resp {


    private Body Body;

    private Header Header;

    private Signature Signature;

    public Body getBody ()
    {
        return Body;
    }

    public void setBody (Body Body)
    {
        this.Body = Body;
    }

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public Signature getSignature ()
    {
        return Signature;
    }

    public void setSignature (Signature Signature)
    {
        this.Signature = Signature;
    }

    @Override
    public String toString()
    {
        return "Single_Payment_Corp_Resp [Body = "+Body+", Header = "+Header+", Signature = "+Signature+"]";
    }
}
