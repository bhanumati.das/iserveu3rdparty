package com.iserveu.response.pojo;

/**
 * 
 * @author Bhanumati
 *
 */
public class PayeenamevalidationRblResponse {

	private BeneficiaryAccValidationRes beneficiaryAccValidationRes;

	@Override
	public String toString() {
		return "RBLResponse [BeneficiaryAccValidationRes = " + beneficiaryAccValidationRes + "]";
	}

	public BeneficiaryAccValidationRes getBeneficiaryAccValidationRes() {
		return beneficiaryAccValidationRes;
	}

	public void setBeneficiaryAccValidationRes(BeneficiaryAccValidationRes beneficiaryAccValidationRes) {
		this.beneficiaryAccValidationRes = beneficiaryAccValidationRes;
	}

}
