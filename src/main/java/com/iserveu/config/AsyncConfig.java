package com.iserveu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfig extends AsyncConfigurerSupport {
	/*
	 * 
	 * (non-Javadoc)
	 * @see org.springframework.scheduling.annotation.AsyncConfigurerSupport#getAsyncExecutor()
	 * 
	 * !!!!!!!!!!!!!!!To be used with precaution!!!!!!!!!!!!!!!!
	 * 
	 * ---------Here are the parameters for the import configuration-----------
	 * These values can change with the time. The maximum number of transaction per min was < 800 during this configuration
	 * If the actual flow varies, a new configuration should be done
	 * executor.setCorePoolSize(300); //Will create new worker thread if the number of workers is < 300
	 * executor.setMaxPoolSize(3000); //A new worker thread will be added if the queue is full and poolsize <3000
	 * executor.setQueueCapacity(500);//Contains up to 500 elements if the number of threads > 300
	 * 
	 * ---------Configurations for core app-----------	
	 * executor.setCorePoolSize(10);	
	 * executor.setMaxPoolSize(50);	
	 * executor.setQueueCapacity(100);
	 * 
	 * ---------Configuration for BigQuery service-------
	 * executor.setCorePoolSize(100);	
	 * executor.setMaxPoolSize(1000);	
	 * executor.setQueueCapacity(400);
	 */
	
	@Bean(name = "bigQueryThreadPoolTaskExecutor")
	@Override
	public ThreadPoolTaskExecutor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);				
		executor.setMaxPoolSize(50);				
		executor.setQueueCapacity(100);				
		executor.setThreadNamePrefix("BigQueryPool-");
		executor.initialize();
		return executor;
	} 
	
	
}