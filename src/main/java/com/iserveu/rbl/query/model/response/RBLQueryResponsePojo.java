package com.iserveu.rbl.query.model.response;

public class RBLQueryResponsePojo {
	
	private Get_Single_Payment_Status_Corp_Res get_Single_Payment_Status_Corp_Res;

	public Get_Single_Payment_Status_Corp_Res getGet_Single_Payment_Status_Corp_Res() {
		return get_Single_Payment_Status_Corp_Res;
	}

	public void setGet_Single_Payment_Status_Corp_Res(
			Get_Single_Payment_Status_Corp_Res get_Single_Payment_Status_Corp_Res) {
		this.get_Single_Payment_Status_Corp_Res = get_Single_Payment_Status_Corp_Res;
	}

	@Override
	public String toString() {
		return "RBLQueryResponsePojo [get_Single_Payment_Status_Corp_Res=" + get_Single_Payment_Status_Corp_Res + "]";
	}

}
