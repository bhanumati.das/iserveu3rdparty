package com.iserveu.rbl.query.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Body {
	
	@JsonProperty("OrgTransactionID")
    private String orgTransactionID;

	
	@JsonProperty("RefNo")
    private String refNo;


	public String getOrgTransactionID() {
		return orgTransactionID;
	}


	public void setOrgTransactionID(String orgTransactionID) {
		this.orgTransactionID = orgTransactionID;
	}


	public String getRefNo() {
		return refNo;
	}


	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}


	@Override
	public String toString() {
		return "Body [orgTransactionID=" + orgTransactionID + ", refNo=" + refNo + "]";
	}

	
	
}
