package com.iserveu.rbl.query.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Get_Single_Payment_Status_Corp_Res {

	
	 @JsonProperty("Body")
	 private Body body;
	 @JsonProperty("Header")
	 private Header header;
	 @JsonProperty("Signature")
	 private Signature signature;

	 public Body getBody() {
	  return body;
	 }

	 public void setBody(Body body) {
	  this.body = body;
	 }

	 public Header getHeader() {
	  return header;
	 }

	 public void setHeader(Header header) {
	  this.header = header;
	 }

	 public Signature getSignature() {
	  return signature;
	 }

	 public void setSignature(Signature signature) {
	  this.signature = signature;
	 }
	
}
