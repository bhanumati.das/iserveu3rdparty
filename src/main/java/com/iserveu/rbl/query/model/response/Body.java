package com.iserveu.rbl.query.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Body {
	
	
	@JsonProperty("ORGTRANSACTIONID")
    private String orgtransactionid;
	
	@JsonProperty("REFNO")
    private String refno;
	
	@JsonProperty("RRN")
    private String rrn;
	
	@JsonProperty("AMOUNT")
    private String amount;
	
	@JsonProperty("PAYMENTSTATUS")
    private String paymentstatus;
	
	@JsonProperty("REMITTERNAME")
    private String remittername;
	
	@JsonProperty("REMITTERMBLNO")
    private String remittermblno;
	
	@JsonProperty("BENEFICIARYNAME") 
    private String beneficiaryname;
	
	@JsonProperty("BANK")
    private String bank;
	
	@JsonProperty("IFSCCODE")
    private String ifsccode;
	
	@JsonProperty("BEN_ACCT_NO")
    private String ben_acct_no;
	
	@JsonProperty("TXNTIME")
    private String txntime;
	
	@JsonProperty("STATUSDESC")
    private String statusdesc;
	
	@JsonProperty("TXNSTATUS")
    private String txnstatus;

	public String getStatusdesc() {
		return statusdesc;
	}

	public void setStatusdesc(String statusdesc) {
		this.statusdesc = statusdesc;
	}

	public String getTxnstatus() {
		return txnstatus;
	}

	public void setTxnstatus(String txnstatus) {
		this.txnstatus = txnstatus;
	}

	public String getOrgtransactionid() {
		return orgtransactionid;
	}

	public void setOrgtransactionid(String orgtransactionid) {
		this.orgtransactionid = orgtransactionid;
	}

	public String getRefno() {
		return refno;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public String getRemittername() {
		return remittername;
	}

	public void setRemittername(String remittername) {
		this.remittername = remittername;
	}

	public String getRemittermblno() {
		return remittermblno;
	}

	public void setRemittermblno(String remittermblno) {
		this.remittermblno = remittermblno;
	}

	public String getBeneficiaryname() {
		return beneficiaryname;
	}

	public void setBeneficiaryname(String beneficiaryname) {
		this.beneficiaryname = beneficiaryname;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getIfsccode() {
		return ifsccode;
	}

	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}

	public String getBen_acct_no() {
		return ben_acct_no;
	}

	public void setBen_acct_no(String ben_acct_no) {
		this.ben_acct_no = ben_acct_no;
	}

	public String getTxntime() {
		return txntime;
	}

	public void setTxntime(String txntime) {
		this.txntime = txntime;
	}

	@Override
	public String toString() {
		return "Body [orgtransactionid=" + orgtransactionid + ", refno=" + refno + ", rrn=" + rrn + ", amount=" + amount
				+ ", paymentstatus=" + paymentstatus + ", remittername=" + remittername + ", remittermblno="
				+ remittermblno + ", beneficiaryname=" + beneficiaryname + ", bank=" + bank + ", ifsccode=" + ifsccode
				+ ", ben_acct_no=" + ben_acct_no + ", txntime=" + txntime + ", statusdesc=" + statusdesc
				+ ", txnstatus=" + txnstatus + "]";
	}
	
	
		
    

	
}
