package com.iserveu.rbl.query.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Signature {
	
	@JsonProperty("Signature")
    private String signature;

	@JsonProperty("Signature")
	public String getSignature() {
		return signature;
	}

	@JsonProperty("Signature")
	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "Signature [signature=" + signature + "]";
	}

}
