package com.iserveu.rbl.query.model.request;

public class RBLQueryRequestPojo {
	
	private Get_Single_Payment_Status_Corp_Req get_Single_Payment_Status_Corp_Req;

	public Get_Single_Payment_Status_Corp_Req getGet_Single_Payment_Status_Corp_Req() {
		return get_Single_Payment_Status_Corp_Req;
	}

	public void setGet_Single_Payment_Status_Corp_Req(
			Get_Single_Payment_Status_Corp_Req get_Single_Payment_Status_Corp_Req) {
		this.get_Single_Payment_Status_Corp_Req = get_Single_Payment_Status_Corp_Req;
	}

	@Override
	public String toString() {
		return "RBLQueryRequestPojo [get_Single_Payment_Status_Corp_Req=" + get_Single_Payment_Status_Corp_Req + "]";
	} 

}
