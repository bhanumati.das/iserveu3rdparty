package com.iserveu.rbl.query.model.request;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserFeaturePojo {
	@NotBlank(message="userName cannot be null")
	private String userName;
	
	@Size(min=1,message="send atleast one transaction Id")
	private List<Integer> featureIds;
	
	@NotNull(message="activeDeactive cannot be null")
	private Boolean activeDeactive;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Boolean getActiveDeactive() {
		return activeDeactive;
	}
	public void setActiveDeactive(Boolean activeDeactive) {
		this.activeDeactive = activeDeactive;
	}
	public List<Integer> getFeatureIds() {
		return featureIds;
	}
	public void setFeatureIds(List<Integer> featureIds) {
		this.featureIds = featureIds;
	}
}
