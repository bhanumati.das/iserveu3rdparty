package com.iserveu.rbl.query.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Get_Single_Payment_Status_Corp_Req {

	@JsonProperty("Body")
    private Body body;

	@JsonProperty("Header")
    private Header header;

	@JsonProperty("Signature")
    private Signature signature;

	@JsonProperty("Body")
	public Body getBody() {
		return body;
	}

	@JsonProperty("Body")
	public void setBody(Body body) {
		this.body = body;
	}

	@JsonProperty("Header")
	public Header getHeader() {
		return header;
	}

	@JsonProperty("Header")
	public void setHeader(Header header) {
		this.header = header;
	}

	@JsonProperty("Signature")
	public Signature getSignature() {
		return signature;
	}

	@JsonProperty("Signature")
	public void setSignature(Signature signature) {
		this.signature = signature;
	}

	
}
