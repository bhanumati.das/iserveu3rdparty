package com.iserveu.rbl.query.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Header {
	
	@JsonProperty("TranID")
    private String tranID;

	@JsonProperty("Checker_ID")
    private String checker_ID;

	@JsonProperty("Maker_ID")
    private String maker_ID;

	@JsonProperty("Corp_ID")
    private String corp_ID;

	@JsonProperty("Approver_ID")
    private String approver_ID;

	public String getTranID() {
		return tranID;
	}

	public void setTranID(String tranID) {
		this.tranID = tranID;
	}

	public String getChecker_ID() {
		return checker_ID;
	}

	public void setChecker_ID(String checker_ID) {
		this.checker_ID = checker_ID;
	}

	public String getMaker_ID() {
		return maker_ID;
	}

	public void setMaker_ID(String maker_ID) {
		this.maker_ID = maker_ID;
	}

	public String getCorp_ID() {
		return corp_ID;
	}

	public void setCorp_ID(String corp_ID) {
		this.corp_ID = corp_ID;
	}

	public String getApprover_ID() {
		return approver_ID;
	}

	public void setApprover_ID(String approver_ID) {
		this.approver_ID = approver_ID;
	}

	@Override
	public String toString() {
		return "Header [tranID=" + tranID + ", checker_ID=" + checker_ID + ", maker_ID=" + maker_ID + ", corp_ID="
				+ corp_ID + ", approver_ID=" + approver_ID + "]";
	}
	
	

	

}
