package com.iserveu.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iserveu.entity.BankIINDetailsEntity;

public interface BankIINDetailsRepository extends JpaRepository<BankIINDetailsEntity,Long> {
	
	BankIINDetailsEntity findByIINNumber(Long IINNUmber);

}
