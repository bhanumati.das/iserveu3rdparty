package com.iserveu.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iserveu.entity.SimplcashWebhook;
/**
 * repository to do DML operation on simplcash webhook
 * @author Raj Kishore
 *
 */
public interface SimplcashWebhookRepository extends JpaRepository<SimplcashWebhook,String>{
	
}
