package com.iserveu.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.iserveu.entity.UserApiMappingEntity;

@Repository
public interface UserApiMappingRepository extends CassandraRepository<UserApiMappingEntity,String> {

}