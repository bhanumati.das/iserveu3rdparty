package com.iserveu.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;

import com.iserveu.entity.ServiceProviderDetailsEntity;

public interface ServiceProviderJpaRepository extends CassandraRepository<ServiceProviderDetailsEntity, Long> {
	
		//@Query("SELECT * FROM service_provider WHERE userid=?1 and min <=?2 and max >=?3 ALLOW FILTERING")
		//ServiceProvider findByid(Long userId);
    	@Query("select * from service_provider_details where service_provider_type=:sptype and  min <=:amount and max >=:amount ALLOW FILTERING")
    	ServiceProviderDetailsEntity findByAmountRange(@Param("amount")Double amount,@Param("sptype")String sptype);
		//@Query("select * from service_provider where userid=:userId and min <=:amount and max >=:amount ALLOW FILTERING")
		//ServiceProvider findByUserIdAndAmountRange(@Param("userId")Long userId,@Param("amount")Double amount);
}
