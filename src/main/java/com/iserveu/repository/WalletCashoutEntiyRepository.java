package com.iserveu.repository;

import java.util.Optional;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.iserveu.entity.WalletCashoutTransaction;

public interface WalletCashoutEntiyRepository extends  CassandraRepository<WalletCashoutTransaction, Long>  {

	Optional<WalletCashoutTransaction> findById(Long clientUniqueID);

}
