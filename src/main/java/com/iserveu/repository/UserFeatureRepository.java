package com.iserveu.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.iserveu.entity.UserFeatureEntity;
import com.iserveu.entity.UserFeatureEntityKey;

@Repository
public interface UserFeatureRepository extends CassandraRepository<UserFeatureEntity, UserFeatureEntityKey>{


	UserFeatureEntity findByUserFeatureKeyUserIdAndUserFeatureKeyFeatureId(Long userId, int i);
	
	List<UserFeatureEntity> findByUserFeatureKeyUserIdAndUserFeatureKeyFeatureIdIn(Long userId, List<Integer> featureIds);

 

}