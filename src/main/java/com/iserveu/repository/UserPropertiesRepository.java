package com.iserveu.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.iserveu.entity.UserPropertiesEntity;

@Repository
public interface UserPropertiesRepository extends CassandraRepository<UserPropertiesEntity, Long> {

}
