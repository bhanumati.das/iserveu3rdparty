package com.iserveu.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iserveu.entity.PayeeVerification;

public interface PayeeVerificationRepo extends JpaRepository<PayeeVerification,Long>{

}
