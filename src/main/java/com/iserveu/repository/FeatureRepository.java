package com.iserveu.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.iserveu.entity.FeatureEntity;

@Repository
public interface FeatureRepository extends CassandraRepository<FeatureEntity, Integer> {

	List<Integer> findIdByFeatureIdIn(List<Integer> featureIds);	

}