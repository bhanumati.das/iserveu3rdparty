package com.iserveu.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iserveu.entity.UserTranslateEntity;

public interface UserTranslateRespository extends JpaRepository<UserTranslateEntity, Long> {

	List<UserTranslateEntity> findByParam1AndParam2(String param1, String param12);

}
