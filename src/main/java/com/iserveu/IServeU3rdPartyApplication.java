package com.iserveu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class IServeU3rdPartyApplication {

	public static void main(String[] args) {
		SpringApplication.run(IServeU3rdPartyApplication.class, args);
	}

}
